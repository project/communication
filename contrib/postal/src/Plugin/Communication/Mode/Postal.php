<?php

namespace Drupal\communication_postal\Plugin\Communication\Mode;

use Drupal\communication\ParticipantRole;
use Drupal\communication\Plugin\Communication\Mode\TextContentModeBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\entity\BundleFieldDefinition;

/**
 * Class Postal
 *
 * @CommunicationMode( *
 *   id = "post",
 *   label = "Post",
 * )
 *
 * @package Drupal\communication_postal\Plugin\Communication\Mode
 *
 * @todo: Permit attachments by improving pdf tools.
 */
class Postal extends TextContentModeBase {

  /**
   * {@inheritdoc}
   */
  protected function hasAttachments() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function textContentFieldDefinitions() {
    $text_fields = [];

    // If PDF tools module exists then we can generate a letter from html.
    if (\Drupal::moduleHandler()->moduleExists('pdf_tools')) {
      $text_fields['body_html'] = BundleFieldDefinition::create('text_long')
        ->setLabel(new TranslatableMarkup('Body'))
        ->setProvider('communication')
        ->setRevisionable(TRUE)
        ->setDisplayOptions('view', [
          'type' => 'text_default',
          'label' => 'hidden',
        ])
        ->setDisplayConfigurable('view', TRUE)
        ->setDisplayOptions('form', [
          'type' => 'text_textarea',
        ])
        ->setDisplayConfigurable('form', TRUE);
    }

    return $text_fields;
  }

  /**
   * {@inheritdoc}
   */
  public function communicationFieldDefinitions(array $base_field_definitions) {
    $fields = parent::communicationFieldDefinitions($base_field_definitions);

    $fields['pdf'] = BundleFieldDefinition::create('file')
      ->setLabel(new TranslatableMarkup('Communication PDF'))
      ->setSettings([
        'uri_scheme' => 'private',
        'file_directory' => 'postal_mail',
        'file_extensions' => 'pdf',
      ])
      ->setProvider('communication_postal')
      ->setDisplayOptions('view', [
        'type' => 'file_default',
        'label' => 'above',
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'file_generic',
      ])
      ->setDisplayConfigurable('form', TRUE);

    // If PDF tools module exists then we can generate a letter from html.
    if (\Drupal::moduleHandler()->moduleExists('pdf_tools')) {
      $fields['pdf_style'] = BundleFieldDefinition::create('entity_reference')
        ->setLabel(new TranslatableMarkup('Style'))
        ->setDescription(new TranslatableMarkup('PDF Generation Style'))
        ->setSetting('target_type', 'pdf_style')
        ->setProvider('communication_postal')
        ->setDisplayOptions('form', [
          'type' => 'options_select',
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);
    }

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function participantRoles() {
    return [
      'from' => ParticipantRole::create('from', 'postal_mail', new TranslatableMarkup('Sender')),
      'return' => ParticipantRole::create('return', 'postal_mail', new TranslatableMarkup('Return Address')),
      'recipient' => ParticipantRole::create('recipient', 'postal_mail', new TranslatableMarkup('Recipient')),
    ];
  }

  /**
   * Get the default target participant.
   *
   * @return string
   */
  public function defaultTargetParticipant() {
    return 'recipient';
  }

  /**
   * Get the default source participant.
   *
   * @return string
   */
  public function defaultSourceParticipant() {
    return 'from';
  }
}
