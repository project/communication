<?php

namespace Drupal\communication\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;
use Drupal\user\EntityOwnerInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Class CommunicationEvent
 *
 * Communication events store things that happen to communications, for example:
 *   - sent
 *   - forward
 *   - received
 *
 * Which events are supported are provided by the communication mode.
 *
 * @ContentEntityType(
 *   id = "communication_event",
 *   label = @Translation("Communication Event"),
 *   base_table = "communication_event",
 *   handlers = {
 *     "storage" = "Drupal\communication\Entity\CommunicationEventStorage",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\communication\Entity\CommunicationEventAccessControlHandler",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "bundle" = "type",
 *     "owner" = "user",
 *   },
 *   admin_permission = "administer communications"
 * )
 *
 * @package Drupal\communication\Entity
 */
class CommunicationEvent extends ContentEntityBase implements EntityOwnerInterface {
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += self::ownerBaseFieldDefinitions($entity_type);

    $fields['communication'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Communication'))
      ->setSetting('target_type', 'communication')
      ->setCardinality(1)
      ->setDisplayConfigurable('view', TRUE);

    $fields['type'] = BaseFieldDefinition::create('list_string')
      ->setLabel(new TranslatableMarkup('Event'))
      ->setSetting('allowed_values_function', [static::class, 'eventTypeOptions'])
      ->setCardinality(1)
      ->setDisplayConfigurable('view', TRUE);

    $fields['date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(new TranslatableMarkup('Date & Time'))
      ->setDescription(new TranslatableMarkup('When the event happened'))
      ->setSetting('datetime_type', DateTimeItem::DATETIME_TYPE_DATETIME)
      ->setDefaultValueCallback(static::class.'::defaultEventDateValue')
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['notes'] = BaseFieldDefinition::create('text_long')
      ->setLabel(new TranslatableMarkup('Notes'))
      ->setDescription(new TranslatableMarkup('Notes associated with this event'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * Get the event type options.
   *
   * @param \Drupal\Core\Field\FieldStorageDefinitionInterface $definition
   * @param \Drupal\Core\Entity\FieldableEntityInterface|NULL $entity
   * @param bool $cacheable
   *
   * @return array
   */
  public static function eventTypeOptions(FieldStorageDefinitionInterface $definition, FieldableEntityInterface $entity = NULL, &$cacheable = FALSE) {
    if ($entity && !$entity->get('communication')->isEmpty()) {
      /** @var \Drupal\communication\Entity\Communication $communication */
      $communication =  $entity->get('communication')->entity;
      $mode = $communication->getMode();

      $options = [];
      foreach ($mode->supportedEvents() as $type => $info) {
        $options[$type] = $info['label'];
      }

      return $options;
    }

    /** @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info */
    $bundle_info = \Drupal::service('entity_type.bundle.info');
    $options = [];
    foreach ($bundle_info->getBundleInfo('communication_event') as $bundle => $info) {
      $options[$bundle] = $info['label'];
    }
    return $options;
  }

  /**
   * Get the current time in the right format.
   *
   * @return false|string
   */
  public static function defaultEventDateValue() {
    return gmdate(DateTimeItem::DATETIME_STORAGE_FORMAT);
  }

  /**
   * Get the communication entity.
   *
   * @return \Drupal\communication\Entity\Communication
   */
  public function getCommunication() {
    return $this->communication->entity;
  }
}
