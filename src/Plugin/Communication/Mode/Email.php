<?php

namespace Drupal\communication\Plugin\Communication\Mode;

use Drupal\communication\ParticipantRole;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\entity\BundleFieldDefinition;

/**
 * Class Email
 *
 * @CommunicationMode(
 *   id = "email",
 *   label = "E-mail",
 *   forms = {
 *     "configure" = "Drupal\communication\PluginForm\ModeConfigureForm"
 *   }
 * )
 *
 * @package Drupal\communication\Plugin\Communication\Mode
 */
class Email extends TextContentModeBase {

  /**
   * {@inheritdoc}
   */
  public function textContentFieldDefinitions() {
    $text_fields = [];

    $text_fields['body_html'] = BundleFieldDefinition::create('text_long')
      ->setLabel(new TranslatableMarkup('Formatted Body'))
      ->setProvider('communication')
      ->setRevisionable(TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'hidden',
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
      ])
      ->setDisplayConfigurable('form', TRUE);
    $text_fields['body_plain'] = BundleFieldDefinition::create('string_long')
      ->setLabel(new TranslatableMarkup('Plain Body'))
      ->setProvider('communication')
      ->setRevisionable(TRUE)
      ->setDisplayOptions('view', [
        'type' => 'string',
        'label' => 'above',
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
      ])
      ->setDisplayConfigurable('form', TRUE);

    return $text_fields;
  }

  /**
   * {@inheritdoc}
   */
  public function participantRoles() {
    return [
      'from' => ParticipantRole::create('from', 'email', new TranslatableMarkup('From')),
      'to' => ParticipantRole::create('to', 'email', new TranslatableMarkup('Recipient'), BaseFieldDefinition::CARDINALITY_UNLIMITED),
      'cc' => ParticipantRole::create('cc', 'email',  new TranslatableMarkup('Carbon-Copy Recipient'), BaseFieldDefinition::CARDINALITY_UNLIMITED),
      'bcc' => ParticipantRole::create(
        'bcc',
        'email',
        new TranslatableMarkup('Blind Carbon-Copy Recipient'),
        BaseFieldDefinition::CARDINALITY_UNLIMITED
      ),
      'reply_to' => ParticipantRole::create(
        'reply_to',
        'email',
        new TranslatableMarkup('Reply to')
      )
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultTargetParticipant() {
    return 'to';
  }

  /**
   * {@inheritdoc}
   */
  public function defaultSourceParticipant() {
    return 'from';
  }
}
