<?php

namespace Drupal\communication_user\Plugin\TypedDataFilter;

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\user\UserInterface;

/**
 * Filter to make a cancel url for the user.
 *
 * @DataFilter(
 *   id = "cancel_user_url",
 *   label = @Translation("Provides the cancel url for this user."),
 * )
 *
 * @package Drupal\communication_user\Plugin\TypedDataFilter
 */
class CancelUrl extends OneTimeLoginUrl {

  /**
   * {@inheritdoc}
   */
  public function filter(
    DataDefinitionInterface $definition,
    $value,
    array $arguments,
    BubbleableMetadata $bubbleable_metadata = NULL
  ) {
    assert($value instanceof UserInterface);
    return user_cancel_url($value);
  }

}
