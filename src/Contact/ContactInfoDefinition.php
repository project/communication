<?php

namespace Drupal\communication\Contact;

use Drupal\Core\TypedData\DataDefinition;

class ContactInfoDefinition extends DataDefinition implements ContactInfoDefinitionInterface {

  /**
   * @var array
   */
  protected $constraints = [];

  /**
   * {@inheritdoc}
   */
  public function addUsageConstraint($name, array $options = []) {
    $this->constraints[$name] = $options;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getUsageConstraint($name) {
    return $this->constraints[$name];
  }

  /**
   * {@inheritdoc}
   */
  public function getUsageConstraints() {
    return $this->constraints;
  }

  /**
   * {@inheritdoc}
   */
  public function setUsageConstraints(array $constraints = []) {
    $this->constraints = $constraints;
    return $this;
  }
}
