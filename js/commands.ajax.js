(function ($, Drupal) {
  Drupal.AjaxCommands.prototype.set_form_value = function(ajax, response) {
    var $form;
    if (response.form_selector) {
      $form = $('form' + response.form_selector);
    }
    else if (response.form_build_id) {
      $form = $('input[name="form_build_id"][value="'+response.form_build_id+'"]').closest('form');
    }
    else {
      $form = ajax.$form;
    }

    $element = $form.find('input[name="'+response.element_name+'"]');
    $element.val(response.data);
    if ($element.hasClass('ui-autocomplete-input')) {
      $element.trigger('autocompleteclose');
    }
    else {
      $element.trigger('change');
    }
  };
  Drupal.AjaxCommands.prototype.set_read_only = function (ajax, response) {
    var $form;
    if (response.form_selector) {
      $form = $('form' + response.form_selector);
    }
    else if (response.form_build_id) {
      $form = $('input[name="form_build_id"][value="'+response.form_build_id+'"]').closest('form');
    }
    else {
      $form = ajax.$form;
    }

    if (response.readonly) {
      $form.find('input[name="' + response.element_name + '"]').attr('readonly', 'readonly');
    }
    else {
      $form.find('input[name="' + response.element_name + '"]').removeAttr('readonly');
    }
  };
})(jQuery, Drupal);
