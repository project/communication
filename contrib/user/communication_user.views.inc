<?php

/**
 * Implements hook_views_data_alter().
 */
function communication_user_views_data_alter(&$data) {
  /** @var \Drupal\Core\Entity\EntityFieldManagerInterface $field_manager */
  $field_manager = \Drupal::service('entity_field.manager');
  $user_definition = \Drupal::entityTypeManager()->getDefinition('user');
  $table = $user_definition->getDataTable();

  if ($data[$table]) {
    $field_definitions = $field_manager->getBaseFieldDefinitions('user');
    foreach (['communication_contact_emails', 'communication_contact_telephones', 'communication_contact_addresses'] as $field_name) {
      $definition = $field_definitions[$field_name];
      $data[$table][$field_name] = [
        'title' => $definition->getLabel(),
        'help' => $definition->getDescription(),
        'field' => [
          'id' => 'field',
        ],
        'entity field' => $field_name,
      ];
    }
  }
}
