<?php
/**
 * Created by PhpStorm.
 * User: Rob
 * Date: 05/04/2019
 * Time: 15:08
 */

namespace Drupal\communication\Plugin\Field;

use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * Class ComputedParticipantsReferenceFieldItemList
 *
 * @package Drupal\communication\Plugin\Field
 */
class ComputedParticipantsReferenceFieldItemList extends EntityReferenceFieldItemList {
  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    if (!$this->getEntity()->id()) {
      return;
    }

    $role = $this->getSetting('participant_role');
    $query = \Drupal::entityTypeManager()->getStorage('communication_participant')->getQuery();
    $query->accessCheck(FALSE);
    $query->condition('communication', $this->getEntity()->id());
    if ($role) {
      $query->condition('role', $role);
    }

    $delta = 0;
    foreach ($query->execute() as $id) {
      $this->list[$delta] = $this->createItem($delta, $id);
      $delta++;
    }
  }
}
