<?php

namespace Drupal\communication\Form;

use Drupal\communication\CommunicationModePluginManager;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormFactoryInterface;
use Drupal\Core\Plugin\PluginWithFormsInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ModeConfigForm allow modes to have settings. Maybe display useful
 * information about supported operations here.
 *
 * @package Drupal\communication\Form
 */
class ModeConfigForm extends ConfigFormBase {

  /**
   * The communication mode plugin id.
   *
   * @var string
   */
  protected $modePluginId = '';

  /**
   * The communication mode plugin.
   *
   * @var \Drupal\communication\Plugin\Communication\Mode\ModeInterface
   */
  protected $mode;

  /**
   * The communication mode manager.
   *
   * @var \Drupal\communication\CommunicationModePluginManager
   */
  protected $modeManager;

  /**
   * The plugin form factory.
   *
   * @var \Drupal\Core\Plugin\PluginFormFactoryInterface
   */
  protected $pluginFormFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.communication.mode'),
      $container->get('plugin_form.factory')
    );
  }

  public function __construct(ConfigFactoryInterface $config_factory, CommunicationModePluginManager $mode_manager, PluginFormFactoryInterface  $plugin_form_factory) {
    parent::__construct($config_factory);

    $this->modeManager = $mode_manager;
    $this->pluginFormFactory = $plugin_form_factory;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    if (!empty($this->modePluginId)) {
      return ['communication.mode.'.$this->modePluginId];
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mode_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $bundle = '') {
    if (empty($this->mode)) {
      $this->modePluginId = $bundle;
      $this->mode = $this->modeManager->createInstance($bundle);
    }

    try {
      if ($this->mode instanceof PluginWithFormsInterface) {
        $plugin_form = $this->pluginFormFactory->createInstance(
          $this->mode,
          'configure'
        );
        $form = $plugin_form->buildConfigurationForm($form, $form_state);
      }
    }
    catch (InvalidPluginDefinitionException $exception) {
      // Just ignore this error.
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    try {
      if ($this->mode instanceof PluginWithFormsInterface) {
        $plugin_form = $this->pluginFormFactory->createInstance(
          $this->mode,
          'configure'
        );
        $plugin_form->validateConfigurationForm($form, $form_state);
      }
    }
    catch (InvalidPluginDefinitionException $exception) {
      // Just ignore this error.
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    try {
      if ($this->mode instanceof PluginWithFormsInterface) {
        $plugin_form = $this->pluginFormFactory->createInstance(
          $this->mode,
          'configure'
        );
        $plugin_form->submitConfigurationForm($form, $form_state);
      }
    }
    catch (InvalidPluginDefinitionException $exception) {
      // Just ignore this error.
    }

    if ($this->mode instanceof ConfigurableInterface) {
      $config = $this->config('communication.mode.' . $this->mode->getPluginId());
      $config->setData($this->mode->getConfiguration());
      $config->save();
    }

    parent::submitForm($form, $form_state);
  }
}
