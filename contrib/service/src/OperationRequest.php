<?php

namespace Drupal\communication_service;

use Drupal\communication\Entity\Communication;

class OperationRequest {

  /**
   * @var \Drupal\communication\Entity\Communication[]
   */
  protected $communications = [];

  /**
   * @var array
   */
  protected $options = [];

  /**
   * OperationRequest constructor.
   *
   * @param \Drupal\communication\Entity\Communication[]|\Drupal\communication\Entity\Communication $communications
   * @param array $options
   */
  public function __construct($communications, array $options = []) {
    $this->options = $options;

    if ($communications instanceof Communication) {
      $this->communications[0] = $communications;
    }
    else if (is_array($communications)) {
      $this->communications = $communications;
    }
    else {
      throw new \UnexpectedValueException();
    }
  }

  /**
   * Get communications.
   *
   * @return \Drupal\communication\Entity\Communication[]
   */
  public function getCommunications() {
    return $this->communications;
  }

  /**
   * Get the communication
   *
   * @param int $index
   *
   * @return \Drupal\communication\Entity\Communication|null
   */
  public function getCommunication($index = 0) {
    return isset($this->communications[$index]) ? $this->communications[$index] : NULL;
  }

  /**
   * Set the communication
   *
   * @param \Drupal\communication\Entity\Communication $communication
   * @param int $index
   *
   * @return static
   */
  public function setCommunication(Communication $communication, $index = 0) {
    $this->communications[$index] = $communication;

    return $this;
  }

  /**
   * Get options.
   *
   * @return array
   */
  public function getOptions() {
    return $this->options;
  }

  /**
   * Set options.
   *
   * @param array $options
   *
   * @return static
   */
  public function setOptions(array $options = []) {
    $this->options = $options;

    return $this;
  }

}
