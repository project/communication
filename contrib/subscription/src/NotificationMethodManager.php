<?php
namespace Drupal\communication_subscription;

use Drupal\communication_subscription\Annotation\NotificationMethod;
use Drupal\communication_subscription\Plugin\Communication\NotificationMethod\NotificationMethodInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Class NotificationMethodManager
 *
 * @package Drupal\communication_subscription
 */
class NotificationMethodManager extends DefaultPluginManager {

  /**
   * Constructs a new NotificationMethodManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Communication/NotificationMethod',
      $namespaces,
      $module_handler,
      NotificationMethodInterface::class,
      NotificationMethod::class
    );

    $this->alterInfo('communication_notification_method_info');
    $this->setCacheBackend($cache_backend, 'communication_notification_method');
  }

}
