<?php

namespace Drupal\communication;

use Drupal\communication\Contact\ContactInterface;
use Drupal\communication\Entity\Communication;
use Drupal\communication\Entity\CommunicationParticipant;
use Drupal\Core\Entity\EntityInterface;

interface CommunicationBuilderInterface {

  /**
   * Build a communication for a given target and source.
   *
   * @param \Drupal\communication\string $mode
   * @param \Drupal\Core\Entity\EntityInterface|\Drupal\Core\Entity\EntityInterface[] $target
   * @param \Drupal\Core\Entity\EntityInterface $source
   *
   * @param \Drupal\communication\Entity\Communication|null $original
   *
   * @return \Drupal\communication\Entity\Communication
   *
   */
  public function buildCommunication(string $mode, $target, EntityInterface $source = NULL, Communication $original = NULL);

  /**
   * Build participant from a contact entity.
   *
   * This method will select the best contact info from the contact and make a
   * com
   *
   * @param string $type
   *   The type of participant.
   * @param string $role
   *   The role of the participant.
   * @param \Drupal\Core\Entity\EntityInterface $contact
   *   The contact entity.
   *
   * @return \Drupal\communication\Entity\CommunicationParticipant
   */
  public function buildParticipant(string $type, string $role, EntityInterface $contact): CommunicationParticipant;
}
