<?php

namespace Drupal\communication\Form;

use Drupal\communication\Entity\Communication;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

trait CommunicationOperationsFormTrait {

  /**
   * Get the renderer
   *
   * @return \Drupal\Core\Render\RendererInterface
   */
  protected function renderer() {
    if (isset($this->renderer)) {
      return $this->renderer;
    }

    return \Drupal::service('renderer');
  }

  /**
   * Get the ajax form wrapper
   *
   * @return string
   */
  protected function ajaxFormWrapper() {
    return 'communication-operations-form';
  }

  /**
   * Apply the ajax form wrapper to the form array
   *
   * @param array $form
   */
  protected function applyAjaxFormWrapper(array &$form) {
    // @todo: Dynamic id to allow for multiple operation forms on one page.
    $form['#prefix'] = '<div id="'.$this->ajaxFormWrapper().'">';
    $form['#suffix'] = '</div>';
  }

  /**
   * @return array
   */
  protected function ajaxDefaults() {
    return [
      'callback' => [$this, 'formAjax'],
      'wrapper' => $this->ajaxFormWrapper(),
      'method' => 'replace',
    ];
  }

  /**
   * Get the default options.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array|mixed
   */
  protected function defaultOptions(FormStateInterface $form_state) {
    if (isset($this->options['configuration'])) {
      return $this->options['configuration'];
    }

    return [];
  }

  /**
   * Get the communication.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\communication\Entity\Communication
   */
  protected function getCommunication(FormStateInterface $form_state): Communication {
    return $this->entity;
  }

  /**
   * Form ajax.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array|\Drupal\Core\Ajax\AjaxResponse
   */
  public function formAjax(array $form, FormStateInterface $form_state) {
    if ($form_state->isRebuilding() || $form_state->hasAnyErrors()) {
      $element = $form_state->getTriggeringElement();
      $array_parents = $element['#array_parents'];
      array_pop($array_parents);
      array_pop($array_parents);
      $form_chunk = NestedArray::getValue($form, $array_parents);

      if ($form_state->hasAnyErrors()) {
        $form_chunk['status_messages'] = [
          '#type' => 'status_messages',
          '#weight' => -1000,
        ];
        $form_chunk['#sorted'] = FALSE;
      }

      if (!$form_state->get('ajax_commands')) {
        return $form_chunk;
      }
      else {
        $response = new AjaxResponse();
        $response->addCommand(new ReplaceCommand('#' . $this->ajaxFormWrapper(), $this->renderer->render($form_chunk)));
        foreach ($form_state->get('ajax_commands') as $command) {
          $response->addCommand($command);
        }

        return $response;
      }
    }

    $url = Url::fromRoute('entity.communication.canonical', [
      'communication' => $this->getCommunication($form_state)->id(),
    ])->toString();
    if ($redirect = $form_state->getRedirect()) {
      if ($redirect instanceof RedirectResponse) {
        $url = $redirect->getTargetUrl();
      }
      else if ($redirect instanceof Url) {
        $url = $redirect->toString();
      }
    }

    if ($destination = \Drupal::request()->query->get('destination')) {
      $url = $destination;
    }

    $response = new AjaxResponse();
    $response->addCommand(new RedirectCommand($url));
    return $response;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  protected function buildOperationForm(array $form, FormStateInterface  $form_state) {
    $operation_id = $form_state->get('operation');
    /** @var \Drupal\communication\Plugin\Communication\Operation\OperationInterface $operation */
    $operation = $this->operationManager->createInstance($operation_id);
    return $operation->form($form, $form_state, $this->getCommunication($form_state));
  }

  protected function operationFormActions(array $form, FormStateInterface $form_state) {
    $actions = [];
    $actions['submit'] = [
      '#type' => 'submit',
      '#value' => new TranslatableMarkup('Continue'),
      '#submit' => [
        '::formSubmitOperationDo',
      ],
      '#validate' => [
        '::formValidateOperationDo',
      ],
      '#ajax' => $this->ajaxDefaults(),
    ];
    $actions['cancel'] = [
      '#type' => 'submit',
      '#value' => new TranslatableMarkup('Cancel'),
      '#submit' => [
        '::formSubmitOperationCancel',
      ],
      '#validate' => [],
      '#ajax' => $this->ajaxDefaults(),
    ];

    return $actions;
  }

  /**
   * Validate the operation selection.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function formValidateOperationSelect(array $form, FormStateInterface $form_state) {
    $element = $form_state->getTriggeringElement();
    $operation_id = $element['#operation'];
    /** @var \Drupal\communication\Plugin\Communication\Operation\OperationInterface $plugin */
    $plugin = $this->operationManager->createInstance($operation_id);

    if (!$plugin->applicable($this->getCommunication($form_state), $this->defaultOptions($form_state))) {
      $form_state->setError($element, new TranslatableMarkup(
        '@operation is not applicable to the communication.',
        [
          '@operation' => $plugin->getPluginDefinition()['label'],
        ]
      ));
    }

    // If this plugin does not have a form, then we need to validate that the
    // operation can be run.
    if (!$plugin->hasForm($this->getCommunication($form_state), $this->defaultOptions($form_state))) {
      $reasons = [];
      if (!$plugin->validate($this->getCommunication($form_state), $this->defaultOptions($form_state), $reasons)) {
        foreach ($reasons as $reason) {
          $form_state->setError($element, $reason);
        }
      }
    }
  }

  /**
   * Submit when an operation is selected. If the selected operation has no form
   * then run the operation.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function formSubmitOperationSelect(array $form, FormStateInterface $form_state) {
    $element = $form_state->getTriggeringElement();
    $operation_id = $element['#operation'];
    /** @var \Drupal\communication\Plugin\Communication\Operation\OperationInterface $plugin */
    $plugin = $this->operationManager->createInstance($operation_id);

    if ($plugin->hasForm($this->getCommunication($form_state), $this->defaultOptions($form_state))) {
      $form_state->set('operation', $operation_id);
      $form_state->setRebuild(TRUE);
    }
    else {
      $plugin->run($this->getCommunication($form_state), $this->defaultOptions($form_state));
      $plugin->formPostOperation($form, $form_state, $this->getCommunication($form_state));

      $this->formSubmitPostOperationDo($form, $form_state);
    }
  }

  /**
   * Validate when an operation form has been submitted.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function formValidateOperationDo(array $form, FormStateInterface $form_state) {
    $communication = clone $this->getCommunication($form_state);

    /** @var \Drupal\communication\Plugin\Communication\Operation\OperationInterface $plugin */
    $plugin = $this->operationManager->createInstance($form_state->get('operation'));
    $plugin->formValidate($form, $form_state, $communication);

    $initial_options = $form_state->get('operation_options') ?: $this->defaultOptions();
    $form_state->set('operation_options', $initial_options);
    $plugin->formSubmit($form, $form_state, $communication);
    $reasons = [];
    if (!$plugin->validate($communication, $form_state->get('operation_options'), $reasons)) {
      foreach ($reasons as $reason) {
        $form_state->setError($form_state->getTriggeringElement(), $reason);
      }
    }
    $form_state->set('operation_options', $initial_options);
  }

  /**
   * Do the operation.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function formSubmitOperationDo(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\communication\Plugin\Communication\Operation\OperationInterface $plugin */
    $plugin = $this->operationManager->createInstance($form_state->get('operation'));
    $initial_options = $form_state->get('operation_options') ?: $this->defaultOptions();
    $form_state->set('operation_options', $initial_options);
    $plugin->formSubmit($form, $form_state, $this->getCommunication($form_state));

    $plugin->run($this->getCommunication($form_state), $form_state->get('operation_options'));

    $plugin->formPostOperation($form, $form_state, $this->getCommunication($form_state));

    $this->formSubmitPostOperationDo($form, $form_state);
  }

  /**
   * Clean up after doing the operation.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function formSubmitPostOperationDo(array $form, FormStateInterface $form_state) {
    $form_state->set('operation', NULL);
    $form_state->set('operation_options', $this->defaultOptions($form_state));

    if (!$form_state->getRedirect() && !\Drupal::request()->query->has('destination')) {
      $form_state->setRebuild(TRUE);
    }
  }

  /**
   * Cancel the operation selected.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function formSubmitOperationCancel(array $form, FormStateInterface $form_state) {
    $form_state->set('operation', NULL);
    $form_state->set('operation_options', []);
    $form_state->setRebuild(TRUE);
  }
}
