<?php
/**
 * Created by PhpStorm.
 * User: Rob
 * Date: 05/04/2019
 * Time: 13:28
 */

namespace Drupal\communication\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a CommunicationMode plugin annotation object.
 *
 * @Annotation
 */
class CommunicationParticipantRole extends Plugin {

  public $participant_type;

  public $id;

  public $label;

  public $plural_label;

  public $cardinality;

}
