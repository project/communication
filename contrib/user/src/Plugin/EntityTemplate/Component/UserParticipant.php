<?php

namespace Drupal\communication_user\Plugin\EntityTemplate\Component;

use Drupal\communication\CommunicationBuilderInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginWithFormsInterface;
use Drupal\Core\Plugin\PluginWithFormsTrait;
use Drupal\Core\TypedData\Exception\MissingDataException;
use Drupal\entity_template\Plugin\EntityTemplate\Component\ComponentBase;
use Drupal\entity_template\Plugin\EntityTemplate\Component\TemplateContextAwareComponentInterface;
use Drupal\entity_template\Plugin\EntityTemplate\Component\TemplateContextAwareComponentTrait;
use Drupal\entity_template\TemplateResult;
use Drupal\typed_data\DataFetcherInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class UserParticipant.
 *
 * @EntityTemplateComponent(
 *   id = "communication_user.participant",
 *   label = "Participant from User",
 *   category = "Communication Contact",
 *   forms = {
 *     "configure" = "\Drupal\communication_user\PluginForm\UserParticipantConfigureForm",
 *   },
 *   deriver = "\Drupal\communication\Plugin\Derivative\CommunicationParticipantRoleDeriver",
 * )
 *
 * @package Drupal\communication_user\Plugin\EntityTemplate\Component
 */
class UserParticipant extends ComponentBase implements TemplateContextAwareComponentInterface, PluginWithFormsInterface, ContainerFactoryPluginInterface {
  use PluginWithFormsTrait;
  use TemplateContextAwareComponentTrait;

  /**
   * The data fetcher service.
   *
   * @var \Drupal\typed_data\DataFetcherInterface
   */
  protected $dataFetcher;

  /**
   * The communication builder service.
   *
   * @var \Drupal\communication\CommunicationBuilderInterface
   */
  protected $communicationBuilder;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('typed_data.data_fetcher'),
      $container->get('communication.builder')
    );
  }

  /**
   * UserParticipant constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\typed_data\DataFetcherInterface $data_fetcher
   * @param \Drupal\communication\CommunicationBuilderInterface $communication_builder
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, DataFetcherInterface $data_fetcher, CommunicationBuilderInterface $communication_builder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->dataFetcher = $data_fetcher;
    $this->communicationBuilder = $communication_builder;
  }

  /**
   * {@inheritdoc}
   */
  public function apply(EntityInterface $entity, TemplateResult $result) {
    $selector = $this->configuration['selector'];
    [$context, $path] = explode('.', $selector.'.', 2);

    if (empty($path)) {
      $contact = $this->template->getContextValue($context);
    }
    else {
      $path = substr($path, 0, -1);

      try {
        $value = $this->dataFetcher->fetchDataByPropertyPath(
          $this->template->getContext($context)->getContextData(),
          $path
        );

        if ($value && $value->getValue()) {
          $contact = $value->getValue();
        }
      }
      catch (MissingDataException $exception) {
        $result->addMessage($exception->getMessage());
      }
    }

    if (empty($contact)) {
      return;
    }

    $type = $this->getPluginDefinition()['participant_type'];
    $role = $this->getPluginDefinition()['role'];
    $field_name = $this->getPluginDefinition()['field_name'];
    $participant = $this->communicationBuilder->buildParticipant($type, $role, $contact);
    $entity->{$field_name}[] = $participant;
  }
}
