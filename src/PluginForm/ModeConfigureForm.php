<?php

namespace Drupal\communication\PluginForm;

use Drupal\communication\OperationPluginManager;
use Drupal\communication\OperationVariantPluginManager;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ModeConfigureForm
 *
 * @package Drupal\communication\PluginForm
 */
class ModeConfigureForm extends PluginFormBase implements ContainerInjectionInterface {
  use DependencySerializationTrait;
  use StringTranslationTrait;

  /**
   * Communication mode plugin.
   *
   * @var \Drupal\communication\Plugin\Communication\Mode\ModeBase
   */
  protected $plugin;

  /**
   * Operation variant manager.
   *
   * @var \Drupal\communication\OperationVariantPluginManager
   */
  protected $operationVariantManager;

  /**
   * Operation manager service.
   *
   * @var \Drupal\communication\OperationPluginManager
   */
  protected $operationManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.communication.operation'),
      $container->get('plugin.manager.communication.operation_variant')
    );
  }

  /**
   * ModeConfigureForm constructor.
   *
   * @param \Drupal\communication\OperationPluginManager $operation_manager
   * @param \Drupal\communication\OperationVariantPluginManager $operation_variant_manager
   */
  public function __construct(OperationPluginManager $operation_manager, OperationVariantPluginManager $operation_variant_manager) {
    $this->operationManager = $operation_manager;
    $this->operationVariantManager = $operation_variant_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $config = $this->plugin->getConfiguration();

    $form['enabled_op_variants'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ];

    foreach ($this->operationManager->getDefinitions() as $op => $operation) {
      if (
        !empty($operation['modes']) &&
        !in_array($this->plugin->getPluginId(), $operation['modes'])
      ) {
        continue;
      }
      if (
        !empty($operation['exclude_modes']) &&
        in_array($this->plugin->getPluginId(), $operation['exclude_modes'])
      ) {
        continue;
      }

      $options = [];
      foreach (
        $this->operationVariantManager->getDefinitionsForOperation($op) as $name => $variant
      ) {
        if (in_array($this->plugin->getPluginId(), $variant['modes'])) {
          $options[$name] = $variant['label'];
        }
      }

      $form['enabled_op_variants'][$op] = [
        '#type' => 'checkboxes',
        '#title' => $this->t(
          '%op_label Methods',
          [ '%op_label' => $operation['label'], ]
        ),
        '#description' => $this->t(
          'Select which %op_label methods should be enabled for this mode',
          [ '%op_label' => $operation['label'], ]
        ),
        '#options' => $options,
        '#default_value' => !empty($config['enabled_op_variants'][$op]) ?
          $config['enabled_op_variants'][$op] : [],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $config = $this->plugin->getConfiguration();

    $config['enabled_op_variants'] = $form_state->getValue('enabled_op_variants');
    foreach ($config['enabled_op_variants'] as $op => $variants) {
      $config['enabled_op_variants'][$op] = array_filter($variants);
    }

    $this->plugin->setConfiguration($config);
  }
}