<?php

namespace Drupal\communication;

use Drupal\communication\Entity\Communication;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\HttpFoundation\Request;

class CommunicationOperationAccess implements AccessInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * CommunicationOperationAccess constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Get the serializer.
   *
   * @return \Symfony\Component\Serializer\SerializerInterface|NULL
   */
  protected function serializer() {
    if (\Drupal::hasService('serializer')) {
      return \Drupal::service('serializer');
    }

    return NULL;
  }

  /**
   * Check the access
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   * @param \Drupal\communication\Entity\Communication|NULL $communication
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   */
  public function access(Request $request, RouteMatchInterface $route_match, Communication $communication = NULL) {
    if ($communication) {
      $options = [];
      $communications = [$communication];
    }
    else if ($serializer = $this->serializer()) {
      $content = $serializer->decode($request->getContent(), $request->query->get('_format'));

      if (isset($content['communication'])) {
        $communications = [
          $this->entityTypeManager->getStorage('communication')
            ->load($content['communication'])
        ];
      }
      else if (isset($content['communications'])) {
        $communications = $this->entityTypeManager->getStorage('communication')
          ->loadMultiple($content['communications']);
      }

      $options = isset($content['options']) ? $content['options'] : [];
    }

    $operation = $route_match->getRouteObject()->getRequirement('_communication_operation_access');

    /** @var \Drupal\Core\Access\AccessResultInterface $access */
    $access = NULL;
    foreach ($communications as $communication) {
      $result = $communication->operationAccess($operation, $options, NULL, TRUE);

      if ($access) {
        $access = $access->andIf($result);
      }
      else {
        $access = $result;
      }
    }

    return $access ?: AccessResult::neutral();
  }
}
