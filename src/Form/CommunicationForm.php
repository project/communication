<?php

namespace Drupal\communication\Form;

use Drupal\communication\OperationPluginManager;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\inline_entity_form\ElementSubmit;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CommunicationForm extends ContentEntityForm {
  use CommunicationOperationsFormTrait;

  /**
   * @var \Drupal\communication\OperationPluginManager
   */
  protected $operationManager;

  /**
   * @var \Drupal\communication\Entity\Communication
   */
  protected $entity;

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.communication.operation'),
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time')
    );
  }

  public function __construct(
    OperationPluginManager $operation_manager,
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL,
    TimeInterface $time = NULL
  ) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);

    $this->operationManager = $operation_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $this->applyAjaxFormWrapper($form);
    if (!$form_state->get('operation')) {
      return parent::form($form, $form_state);
    }
    else {
      return $this->buildOperationForm($form, $form_state);
    }
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    if ($form_state->get('operation')) {
      $actions = $this->operationFormActions($form, $form_state);
    }
    else {
      $actions = parent::actions($form, $form_state);

      $ajax_defaults = $this->ajaxDefaults();
      foreach ($this->operationManager->availableOperations($this->entity) as $id => $plugin) {
        $actions['op_' . $id] = [
          '#type' => 'submit',
          '#name' => 'op_' . $id,
          '#value' => $plugin->getPluginDefinition()['label'],
          '#submit' => [
            '::submitForm',
            '::save',
            '::formSubmitOperationSelect',
          ],
          '#validate' => [
            '::validateForm',
            '::formValidateOperationSelect',
          ],
          '#ajax' => $ajax_defaults,
          '#operation' => $id,
        ];
        ElementSubmit::addCallback($actions['op_'.$id], $form);
      }
    }

    return $actions;
  }

}
