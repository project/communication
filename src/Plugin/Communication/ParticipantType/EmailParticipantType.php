<?php

namespace Drupal\communication\Plugin\Communication\ParticipantType;

use Drupal\communication\Contact\ContactInfoDefinition;
use Drupal\communication\Entity\CommunicationParticipant;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\entity\BundleFieldDefinition;

/**
 * Class EmailParticipantType
 *
 * @CommunicationParticipantType(
 *   id = "email",
 *   label = "E-mail",
 * )
 *
 * @package Drupal\communication\Plugin\CommunicationParticipantType
 */
class EmailParticipantType extends ParticipantTypeBase {

  /**
   * Get communication participant fields required for this type.
   *
   * @param \Drupal\Core\Field\BaseFieldDefinition[] $base_field_definitions
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   */
  public function communicationParticipantFieldDefinitions(array $base_field_definitions) {
    $fields = parent::communicationParticipantFieldDefinitions($base_field_definitions);

    $fields['email'] = BundleFieldDefinition::create('email')
      ->setLabel(new TranslatableMarkup('Email'))
      ->setDescription(new TranslatableMarkup('The email of this participant.'))
      ->setRequired(TRUE)
      ->setProvider('communication')
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'email_default',
      ])
      ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }

  /**
   * Get the participant label.
   *
   * @param \Drupal\communication\Entity\CommunicationParticipant $participant
   *
   * @return string|\Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public function participantLabel(CommunicationParticipant $participant) {
    if ($participant->name->isEmpty()) {
      return $participant->email->value ?: new TranslatableMarkup('INCOMPLETE');
    }
    else {
      return new TranslatableMarkup("@name <@email>;", [
        '@name' => $participant->name->value,
        '@email' => $participant->email->value ?: new TranslatableMarkup('INCOMPLETE'),
      ]);
    }
  }

  /**
   * The required contact info definition.
   *
   * @return \Drupal\communication\Contact\ContactInfoDefinitionInterface
   */
  public function requiredContactInfoDefinition() {
    return ContactInfoDefinition::create('email');
  }
}
