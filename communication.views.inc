<?php

use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Implements hook_views_data_alter().
 */
function communication_views_data_alter(&$data) {
  $data['communication_participant']['table']['join']['communication'] = [
    'table' => 'communication_participant',
    'left_field' => 'id',
    'field' => 'communication',
  ];

  /** @var \Drupal\communication\CommunicationModePluginManager $mode_manager */
  $mode_manager = \Drupal::service('plugin.manager.communication.mode');
  foreach ($mode_manager->getDefinitions() as $mode => $definition) {
    /** @var \Drupal\communication\Plugin\Communication\Mode\ModeInterface $plugin */
    $plugin = $mode_manager->createInstance($mode);

    foreach ($plugin->participantRoles() as $role_name => $role_def) {
      $cardinality = isset($role_def['cardinality']) ? $role_def['cardinality'] : 1;
      $plural_name = isset($role_def['plural_name']) ? $role_def['plural_name'] : $role_name.'s';
      $plural_label = isset($role_def['plural_label']) ? $role_def['plural_label'] : $role_def['label'].'s';

      $field_name = $cardinality === 1 ? $role_name : $plural_name;

      $data['communication'][$field_name] = [
        'title' => $cardinality === 1 ? $role_def['label'] : $plural_label,
        'field' => [
          'id' => 'field',
        ],
        'entity field' => $field_name,
      ];
    }

    foreach ($plugin->supportedEvents() as $event_type => $info) {
      $field_name = 'last_'.$event_type.'_event';
      if (!empty($data['communication'][$field_name])) {
        continue;
      }

      $data['communication'][$field_name] = [
        'title' => new TranslatableMarkup('Latest @type Event', ['@type' => $info['label']]),
        'relationship' => [
          'help' => new TranslatableMarkup('Bridge to the most recent @type event', ['@type' => $info['label']]),
          'id' => 'communication_latest_event',
          'event type' => $event_type,
          'base' => 'communication_event',
          'base field' => 'communication',
          'relationship table' => 'communication',
          'relationship field' => 'id',
          'entity type' => 'communication event',
          'extra' => [
            [
              'field' => 'type',
              'value' => $event_type,
            ],
          ],
        ],
      ];
    }
  }
}
