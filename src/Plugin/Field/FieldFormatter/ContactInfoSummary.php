<?php

namespace Drupal\communication\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Class ContactInfoSummary
 *
 * Formatter plugin for contact info summary.
 *
 * @FieldFormatter(
 *   id = "contact_info_summary",
 *   label = @Translation("Contact Info Summary List"),
 *   field_types = {
 *     "contact_info",
 *   }
 * )
 *
 * @package Drupal\communication\Plugin\Field\FieldFormatter
 */
class ContactInfoSummary extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'info_label_display' => 'none',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements['info_label_display'] = [
      '#type' => 'select',
      '#title' => new TranslatableMarkup('Contact Info Label Display'),
      '#options' => [
        'none' => new TranslatableMarkup('None'),
        'brackets_post' => new TranslatableMarkup('in brackets after the Summary'),
        'bold_pre' => new TranslatableMarkup('in bold before the Summary'),
      ],
      '#default_value' => $this->getSetting('info_label_display'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $string = $item->summary;

      switch ($this->getSetting('info_label_display')) {
        case 'brackets_post':
          $string .= " ({$item->label})";
          break;
        case 'bold_pre':
          $string = "<strong>{$item->label}</strong> {$string}";
          break;
        default:
      }

      // The text value has no text format assigned to it, so the user input
      // should equal the output, including newlines.
      $elements[$delta] = [
        '#markup' => $string,
      ];
    }

    return $elements;
  }
}
