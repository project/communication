<?php

namespace Drupal\communication\Plugin\Block;

use Drupal\communication\Form\CommunicationOperationsForm;
use Drupal\communication\OperationPluginManager;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CommunicationOperationsFormBlock
 *
 * @Block(
 *   id = "communication_operations_form",
 *   admin_label = @Translation("Communication Operations From"),
 *   category = @Translation("Communication"),
 *   context = {
 *     "communication" = @ContextDefinition("entity:communication", label = @Translation("Communication"))
 *   }
 * )
 *
 * @package Drupal\communication\Plugin\Block
 */
class CommunicationOperationsFormBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * @var \Drupal\communication\OperationPluginManager
   */
  protected $operationManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder'),
      $container->get('plugin.manager.communication.operation')
    );
  }

  /**
   * CommunicationOperationsFormBlock constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $form_builder, OperationPluginManager $operation_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $form_builder;
    $this->operationManager = $operation_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return $this->formBuilder->getForm(
      CommunicationOperationsForm::class,
      $this->getContextValue('communication'),
      $this->getConfiguration()['operations']
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $default_config = parent::defaultConfiguration();
    $default_config['operations'] = [
      'exclude' => [],
      'configuration' => [],
    ];
    return $default_config;
  }

  /**
   * [@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['operations'] = [
      '#type' => 'container',
    ];

    $operation_defs = $this->operationManager->getDefinitions();
    $form['operations']['exclude'] = [
      '#type' => 'checkboxes',
      '#title' => new TranslatableMarkup('Excluded Operations'),
      '#description' => new TranslatableMarkup('Any selected operations will <strong>not</strong> be shown as options to the user.'),
      '#options' => [],
      '#default_value' => $this->getConfiguration()['operations']['exclude'],
    ];
    foreach ($operation_defs as $plugin_id => $def) {
      $form['operations']['exclude']['#options'][$plugin_id] = $def['label'];
    }

    return parent::buildConfigurationForm($form, $form_state);
  }
}
