<?php

namespace Drupal\communication_user\Plugin\ContactInfoSource;

use Drupal\communication\Contact\ContactInfo;
use Drupal\communication\Contact\ContactInfoDefinition;
use Drupal\communication\Contact\ContactInfoDefinitionInterface;
use Drupal\communication\Plugin\ContactInfoSource\ContactInfoSourceBase;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\TypedData\DataDefinitionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ProfileFieldSource
 *
 * @ContactInfoSource(
 *   id = "profile_field",
 *   label = @Translation("Profile Field"),
 *   entity_type_id = "user",
 * )
 *
 * @package Drupal\communication_user\Plugin\ContactInfoSource
 */
class ProfileFieldSource extends ContactInfoSourceBase {

  /**
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * ProfileFieldSource constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager);

    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function collectInfo(EntityInterface $entity, ContactInfoDefinitionInterface $definition, array $options = []) {
    $profile_storage = $this->entityTypeManager->getStorage('profile');
    $ids = $profile_storage->getQuery()
      ->condition('uid', $entity->id())
      ->condition('is_default', 1)
      ->execute();

    $info = [];
    /** @var \Drupal\profile\Entity\Profile $profile */
    foreach ($profile_storage->loadMultiple($ids) as $profile) {
      $bundle = $profile->bundle();

      /** @var \Drupal\Core\Field\FieldDefinitionInterface $field_definition */
      foreach ($profile->getFieldDefinitions() as $field_definition) {
        $sub_key = "{$bundle}.{$field_definition->getName()}";
        if (
          ($field_definition->getType() == 'email' && $definition->getDataType() == 'email') ||
          ($field_definition->getType() == 'telephone' && $definition->getDataType() == 'telephone') ||
          ($field_definition->getType() == 'address' && $definition->getDataType() == 'address')
        ) {
          $info[$sub_key] = new ContactInfo($definition, $entity, $this->getPluginId(), $sub_key);
          if ($profile->{$field_definition->getName()}->isEmpty()) {
            $info[$sub_key]->setIncomplete();
          }
        }
      }
    }

    return $info;
  }

  /**
   * @return mixed
   */
  public function getInfoValue(EntityInterface $entity, $key, $name, DataDefinitionInterface $definition) {
    list($bundle, $field_name) = explode('.', $key, 2);

    $profile_storage = $this->entityTypeManager->getStorage('profile');
    $ids = $profile_storage->getQuery()
      ->condition('uid', $entity->id())
      ->condition('is_default', 1)
      ->condition('type', $bundle)
      ->execute();

    if ($ids) {
      $profile = $profile_storage->load(reset($ids));
      if ($name == "email" || $name == 'number') {
        return $profile->{$field_name}->value;
      }
      else if ($name == "address") {
        return $profile->{$field_name}->getItem(0)->toArray();
      }
      else if ($name == "name") {
        return $entity->getDisplayName();
      }
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function supportsWriteBackInfoValue(EntityInterface $entity, $key, $name, DataDefinitionInterface $definition) {
    return ($name != "name");
  }

  /**
   * {@inheritdoc}
   */
  public function writeBackInfoValues(EntityInterface $entity, $key, DataDefinitionInterface $definition, $values) {
    list($bundle, $field_name) = explode('.', $key, 2);

    $profile_storage = $this->entityTypeManager->getStorage('profile');
    $ids = $profile_storage->getQuery()
      ->condition('uid', $entity->id())
      ->condition('is_default', 1)
      ->condition('type', $bundle)
      ->execute();

    if ($ids) {
      $profile = $profile_storage->load(reset($ids));
      $profile->{$field_name} = reset($values);
      $profile->save();
    }

    return $key;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel(EntityInterface $entity, $key) {
    list($bundle, $field_name) = explode('.', $key, 2);

    $profile_type = $this->entityTypeManager->getStorage('profile_type')->load($bundle);
    $definitions = $this->entityFieldManager->getFieldDefinitions('profile', $bundle);

    $definition = $definitions[$field_name];
    return $profile_type->label().' '.$definition->getLabel();
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary(EntityInterface $entity, $key) {
    list($bundle, $field_name) = explode('.', $key, 2);

    /** @var \Drupal\profile\ProfileStorageInterface $profile_storage */
    $profile_storage = $this->entityTypeManager->getStorage('profile');
    if ($profile = $profile_storage->loadDefaultByUser($entity, $bundle)) {
      $build = $profile->get($field_name)->view([
        'label' => 'hidden',
      ]);
      return \Drupal::service('renderer')->render($build);
    }

    return '';
  }

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param $key
   *
   * @return \Drupal\communication\Contact\ContactInfoDefinitionInterface
   */
  public function getInfoDefinition(EntityInterface $entity, $key) {
    list($bundle, $field_name) = explode('.', $key, 2);
    $field_definitions = $this->entityFieldManager->getFieldDefinitions('profile', $bundle);

    return ContactInfoDefinition::create($field_definitions[$field_name]->getDataType());
  }
}
