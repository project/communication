<?php

namespace Drupal\communication\Plugin\Communication\OperationVariant;

use Drupal\communication\Entity\CommunicationInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SendEmailDrupalMailsystemOperationVariant
 *
 * @CommunicationOperationVariant(
 *   id = "send_email_mailsystem",
 *   label = @Translation("Drupal"),
 *   modes = {"email"},
 *   operation = "send",
 *   exclude_modes = {},
 * )
 *
 * @package Drupal\communication\Plugin\Communication\OperationVariant
 */
class SendEmailDrupalMailsystemOperationVariant extends OperationVariantBase implements ContainerFactoryPluginInterface {

  /**
   * Mail manager service
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * The language manager
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.mail'),
      $container->get('language_manager')
    );
  }

  /**
   * SendEmailDrupalMailsystemOperationVariant constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   */
  public function __construct(
    array $configuration, $plugin_id, $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    MailManagerInterface $mail_manager,
    LanguageManagerInterface $language_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager);
    $this->mailManager = $mail_manager;
    $this->languageManager = $language_manager;
  }

  /**
   * Run this operation.
   *
   * @param \Drupal\communication\Entity\CommunicationInterface $communication
   * @param array $options
   *
   * @return CommunicationInterface
   *   The communication with the operation performed on it.
   */
  public function run(CommunicationInterface $communication, array $options = []) {
    // Build Recipients list
    $to = [];
    $first_participant = NULL;
    foreach ($communication->getParticipants('to') as $participant) {
      /** @var \Drupal\communication\Entity\CommunicationParticipant $participant */
      if (!$first_participant) {
        $first_participant = $participant;
      }
      $name = $participant->name->value;
      $address = $participant->email->value;

      $to[] = !empty($name) ? "{$name} <{$address}>" : $address;
    }

    $reply_to = NULL;
    if ($reply = $communication->getParticipants('reply_to')) {
      $reply = reset($reply);
      $name = $reply->name->value;
      $address = $reply->email->value;

      $reply_to = !empty($name) ? "{$name} <{$address}>" : $address;
    }

    $this->mailManager->mail(
      'communication',
      'email',
      implode(', ', $to),
      $this->languageManager->getDefaultLanguage()->getId(),
      [
        'communication' => $communication,
        'options' => $options,
      ],
      $reply_to
    );

    $communication->status = 'sent';
    $communication->save();
    $communication->logEvent('sent');

    return $communication;
  }

  /**
   * Validate whether this operation can be run.
   *
   * @param \Drupal\communication\Entity\CommunicationInterface $communication
   * @param array $options
   * @param array $reasons
   *
   * @return boolean
   *   True if the operation can be run. False if it cannot.
   */
  public function validate(CommunicationInterface $communication, array $options = [], array &$reasons = []) {
    // @todo: Implement properly.
    return TRUE;
  }
}
