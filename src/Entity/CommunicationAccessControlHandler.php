<?php

namespace Drupal\communication\Entity;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

class CommunicationAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    if (stripos($operation, 'op__') === 0) {
      list(,$op) = explode('__', $operation, 2);
      return $this->checkOperationAccess($entity, $op, $account);
    }

    return parent::checkAccess($entity, $operation, $account)->orIf(
      AccessResult::allowedIfHasPermission($account, "{$operation} any communication")
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return parent::checkCreateAccess($account, $context, $entity_bundle)
      ->orIf(AccessResult::allowedIfHasPermission($account, "create communication"));
  }

  /**
   * Check the access to perform an operation.
   *
   * @param \Drupal\communication\Entity\Communication $communication
   * @param $operation
   * @param array $options
   * @param \Drupal\Core\Session\AccountInterface|NULL $account
   */
  public function operationAccess(Communication $communication, $operation, array $options = [], AccountInterface $account = NULL, $return_as_object = FALSE) {
    $account = $this->prepareUser($account);

    // @todo: Consider caching?!
    // @todo: Consider hook?!

    $access = $this->checkOperationAccess($communication, $operation, $account, $options);

    return $return_as_object ? $access : $access->isAllowed();
  }

  /**
   * Check whether the operation is allowed to happen.
   *
   * @param \Drupal\communication\Entity\Communication $communication
   * @param $operation
   * @param \Drupal\Core\Session\AccountInterface $account
   * @param array $options
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   */
  protected function checkOperationAccess(Communication $communication, $operation, AccountInterface $account, array $options = []) {
    return AccessResult::allowed();
  }

}
