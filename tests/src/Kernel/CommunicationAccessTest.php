<?php

namespace Drupal\Tests\communication\Kernel;

use Drupal\communication\Entity\Communication;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Tests for Communication access.
 *
 * @group communication
 */
class CommunicationAccessTest extends KernelTestBase {
  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'serialization', 'system', 'datetime', 'user', 'options',
    'field', 'file', 'text', 'communication',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('system', ['sequences']);
    $this->installEntitySchema('communication');
    $this->installEntitySchema('communication_event');
    $this->installEntitySchema('user');
  }

  /**
   * Test the create access.
   */
  public function testCreateAccess() {
    // User 1 can do anything, so should be able to perform any operation.
    $admin_user = $this->createUser();

    // This user has the administer permission, so should be able to perform any operation.
    $user_with_admin_permission = $this->createUser(['administer communications']);

    // This user has no permissions, so should not be able to perform any operation.
    $user_with_no_perms = $this->createUser();

    // This user has the view any permission, so should be able to view the
    // communication.
    $user_with_view_any_perm = $this->createUser(['view any communication']);

    // This user has the create permission, so should be able to create a communication.
    $user_with_create_perm = $this->createUser(['create communication']);

    // This user has the delete any permission, so should be able to delete a
    // communication.
    $user_with_delete_any_perm = $this->createUser(['delete any communication']);

    // This user has the update any permission, so should be able to update a
    // communication.
    $user_with_update_any_perm = $this->createUser(['update any communication']);

    $communication = Communication::create([
      'mode' => 'email',
    ]);

    $this->assertTrue($communication->access('create', $user_with_create_perm), 'User with the create permission can create a communication');
    $this->assertTrue($communication->access('create', $admin_user), 'User 1 can create a communication');
    $this->assertTrue($communication->access('create', $user_with_admin_permission), 'User with the admin permission can create a communication');
    $this->assertFalse($communication->access('create', $user_with_no_perms), 'User with no permissions cannot create a communication');
    $this->assertFalse($communication->access('create', $user_with_view_any_perm), 'User with the view any permission cannot create a communication');
    $this->assertFalse($communication->access('create', $user_with_delete_any_perm), 'User with the delete any permission cannot create a communication');
    $this->assertFalse($communication->access('create', $user_with_update_any_perm), 'User with the update any permission cannot create a communication');
  }

  /**
   * Test the view access.
   */
  public function testViewAccess() {
    // User 1 can do anything, so should be able to perform any operation.
    $admin_user = $this->createUser();

    // This user has the administer permission, so should be able to perform any operation.
    $user_with_admin_permission = $this->createUser(['administer communications']);

    // This user has no permissions, so should not be able to perform any operation.
    $user_with_no_perms = $this->createUser();

    // This user has the view any permission, so should be able to view the
    // communication.
    $user_with_view_any_perm = $this->createUser(['view any communication']);

    // This user has the create permission, so should be able to create a communication.
    $user_with_create_perm = $this->createUser(['create communication']);

    // This user has the delete any permission, so should be able to delete a
    // communication.
    $user_with_delete_any_perm = $this->createUser(['delete any communication']);

    // This user has the update any permission, so should be able to update a
    // communication.
    $user_with_update_any_perm = $this->createUser(['update any communication']);

    $communication = Communication::create([
      'mode' => 'email',
    ]);

    $this->assertFalse($communication->access('view', $user_with_create_perm), 'User with the create permission cannot view the communication');
    $this->assertTrue($communication->access('view', $admin_user), 'User 1 can view the communication');
    $this->assertTrue($communication->access('view', $user_with_admin_permission), 'Users with the administer communications permission can view the communication.');
    $this->assertFalse($communication->access('view', $user_with_no_perms), 'Users with no permissions cannot view the communication.');
    $this->assertTrue($communication->access('view', $user_with_view_any_perm), 'Users with the view any permission can view the communication.');
    $this->assertFalse($communication->access('view', $user_with_delete_any_perm), 'Users with delete any permission cannot view the communication.');
    $this->assertFalse($communication->access('view', $user_with_update_any_perm), 'Users with update any permission cannot view the communication.');
  }

  /**
   * Test the delete access.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testDeleteAccess() {
    // User 1 can do anything, so should be able to perform any operation.
    $admin_user = $this->createUser();

    // This user has the administer permission, so should be able to perform any operation.
    $user_with_admin_permission = $this->createUser(['administer communications']);

    // This user has no permissions, so should not be able to perform any operation.
    $user_with_no_perms = $this->createUser();

    // This user has the view any permission, so should be able to view the
    // communication.
    $user_with_view_any_perm = $this->createUser(['view any communication']);

    // This user has the create permission, so should be able to create a communication.
    $user_with_create_perm = $this->createUser(['create communication']);

    // This user has the delete any permission, so should be able to delete a
    // communication.
    $user_with_delete_any_perm = $this->createUser(['delete any communication']);

    // This user has the update any permission, so should be able to update a
    // communication.
    $user_with_update_any_perm = $this->createUser(['update any communication']);

    $communication = Communication::create([
      'mode' => 'email',
    ]);

    // We expect all access checks to return false since checkAccess() with 'delete' operation returns forbidden if the entity is new.
    $this->assertFalse($communication->access('delete', $user_with_create_perm), 'User with the create permission cannot delete the communication');
    $this->assertFalse($communication->access('delete', $admin_user), 'User 1 can delete the communication');
    $this->assertFalse($communication->access('delete', $user_with_admin_permission), 'Users with the administer communications permission can delete the communication.');
    $this->assertFalse($communication->access('delete', $user_with_no_perms), 'Users with no permissions cannot delete the communication.');
    $this->assertFalse($communication->access('delete', $user_with_view_any_perm), 'Users with the view any permission cannot delete the communication.');
    $this->assertFalse($communication->access('delete', $user_with_delete_any_perm), 'Users with delete any permission can delete the communication.');
    $this->assertFalse($communication->access('delete', $user_with_update_any_perm), 'Users with update any permission cannot delete the communication.');

    $communication->save();

    // After saving the entity, we expect the permissions to work as expected.
    $this->assertFalse($communication->access('delete', $user_with_create_perm), 'User with the create permission cannot delete the communication');
    $this->assertTrue($communication->access('delete', $admin_user), 'User 1 can delete the communication');
    $this->assertTrue($communication->access('delete', $user_with_admin_permission), 'Users with the administer communications permission can delete the communication.');
    $this->assertFalse($communication->access('delete', $user_with_no_perms), 'Users with no permissions cannot delete the communication.');
    $this->assertFalse($communication->access('delete', $user_with_view_any_perm), 'Users with the view any permission cannot delete the communication.');
    $this->assertTrue($communication->access('delete', $user_with_delete_any_perm), 'Users with delete any permission can delete the communication.');
    $this->assertFalse($communication->access('delete', $user_with_update_any_perm), 'Users with update any permission cannot delete the communication.');
  }

  /**
   * Test the update access.
   */
  public function testUpdateAccess() {
    // User 1 can do anything, so should be able to perform any operation.
    $admin_user = $this->createUser();

    // This user has the administer permission, so should be able to perform any operation.
    $user_with_admin_permission = $this->createUser(['administer communications']);

    // This user has no permissions, so should not be able to perform any operation.
    $user_with_no_perms = $this->createUser();

    // This user has the view any permission, so should be able to view the
    // communication.
    $user_with_view_any_perm = $this->createUser(['view any communication']);

    // This user has the create permission, so should be able to create a communication.
    $user_with_create_perm = $this->createUser(['create communication']);

    // This user has the delete any permission, so should be able to delete a
    // communication.
    $user_with_delete_any_perm = $this->createUser(['delete any communication']);

    // This user has the update any permission, so should be able to update a
    // communication.
    $user_with_update_any_perm = $this->createUser(['update any communication']);

    $communication = Communication::create([
      'mode' => 'email',
    ]);

    $this->assertFalse($communication->access('update', $user_with_create_perm), 'User with the create permission cannot update the communication');
    $this->assertTrue($communication->access('update', $admin_user), 'User 1 can update the communication');
    $this->assertTrue($communication->access('update', $user_with_admin_permission), 'Users with the administer communications permission can update the communication.');
    $this->assertFalse($communication->access('update', $user_with_no_perms), 'Users with no permissions cannot update the communication.');
    $this->assertFalse($communication->access('update', $user_with_view_any_perm), 'Users with the view any permission cannot update the communication.');
    $this->assertFalse($communication->access('update', $user_with_delete_any_perm), 'Users with delete any permission cannot update the communication.');
    $this->assertTrue($communication->access('update', $user_with_update_any_perm), 'Users with update any permission can update the communication.');
  }
}
