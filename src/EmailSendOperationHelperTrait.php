<?php

namespace Drupal\communication;

use Drupal\communication\Entity\CommunicationInterface;
use Drupal\communication\Entity\CommunicationParticipant;
use Drupal\Component\Utility\Mail;

/**
 * Trait EmailSendOperationHelperTrait
 *
 * Helpful functions for email send operations.
 *
 * @package Drupal\communication
 */
trait EmailSendOperationHelperTrait {

  /**
   * Reduce a participant to a string.
   *
   * @param \Drupal\communication\Entity\CommunicationParticipant $participant
   *
   * @return string
   */
  protected function reduceParticipant(CommunicationParticipant $participant) {
    $name = $participant->name->value;
    $address = $participant->email->value;

    return !empty($name) ? Mail::formatDisplayName($name) . " <{$address}>" : $address;
  }

  /**
   * Reduce a role to a single line.
   *
   * @param \Drupal\communication\Entity\CommunicationInterface $communication
   * @param $role
   *
   * @return null|string
   */
  protected function reduceRoleParticipants(CommunicationInterface $communication, $role) {
    $output = [];
    foreach ($communication->getParticipants($role) as $participant) {
      $output[] = $this->reduceParticipant($participant);
    }

    return !empty($output) ? implode(', ', $output) : NULL;
  }
}
