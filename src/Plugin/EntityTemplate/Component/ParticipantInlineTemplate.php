<?php

namespace Drupal\communication\Plugin\EntityTemplate\Component;

use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_template\Plugin\EntityTemplate\Component\InlineTemplate;
use Drupal\entity_template\TemplateResult;

/**
 * Class ParticipantInlineTemplate
 *
 * @EntityTemplateComponent(
 *   id = "communication.participant_template",
 *   label = "Inline Participant Template",
 *   category = "Inline Template",
 *   forms = {
 *     "configure" = "\Drupal\entity_template_ui\PluginForm\InlineTemplateConfigureForm",
 *     "add" = "\Drupal\entity_template_ui\PluginForm\InlineTemplateAddForm",
 *   },
 *   ui_class = "\Drupal\entity_template_ui\Plugin\EntityTemplate\Template\InlineTemplateUI",
 *   deriver = "\Drupal\communication\Plugin\Derivative\CommunicationParticipantRoleDeriver",
 * )
 *
 * @package Drupal\communication\Plugin\EntityTemplate\Component
 */
class ParticipantInlineTemplate extends InlineTemplate {

  /**
   * {@inheritdoc}
   */
  protected function getReturnBundle() {
    return $this->pluginDefinition['participant_type'];
  }

  /**
   * {@inheritdoc}
   */
  public function getReturnType() {
    return "entity:communication_participant:".$this->getReturnBundle();
  }

  /**
   * @return \Drupal\Core\Entity\EntityInterface
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function createEntity() {
    $storage = $this->entityTypeManager()->getStorage("communication_participant");

    $initial_values = [
      'type' => $this->getReturnBundle(),
      'role' => $this->pluginDefinition['role'],
    ];
    return $storage->create($initial_values);
  }

  /**
   * Apply this component to the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param \Drupal\entity_template\TemplateResult $result
   */
  public function apply(EntityInterface $entity, TemplateResult $result) {
    /** @var \Drupal\Core\Entity\FieldableEntityInterface $entity */
    $template_result = $this->execute();
    if ($generated = $template_result->getEntity()) {
      $generated->communication = $entity;

      if ($this->getFieldDefinition()->getFieldStorageDefinition()->getCardinality() === 1) {
        $entity->{$this->getFieldName()} = $generated;
      }
      else {
        $entity->{$this->getFieldName()}[] = $generated;
      }
    }
  }


}
