<?php

namespace Drupal\communication\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

interface CommunicationInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * Get the pariticipants.
   *
   * @param string $role_name
   *   (optional)
   *
   * @return \Drupal\communication\Entity\CommunicationParticipant[]
   */
  public function getParticipants($role_name = NULL);

  /**
   * Get a participant.
   *
   * If the role has multiple cardinality return the first.
   *
   * @param string $role_name
   *
   * @return \Drupal\communication\Entity\CommunicationParticipant
   */
  public function getParticipant($role_name);

  /**
   * Get the mode plugin.
   *
   * @return \Drupal\communication\Plugin\Communication\Mode\ModeInterface;
   */
  public function getMode();
}
