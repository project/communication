<?php

namespace Drupal\communication\Plugin\Communication\OperationVariant;

use Drupal\communication\Entity\CommunicationInterface;
use Drupal\communication\Plugin\Communication\Operation\OperationInterface;

interface OperationVariantInterface extends OperationInterface {
}
