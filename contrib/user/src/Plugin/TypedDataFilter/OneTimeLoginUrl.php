<?php

namespace Drupal\communication_user\Plugin\TypedDataFilter;

use Drupal\Core\Entity\TypedData\EntityDataDefinitionInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\typed_data\DataFilterBase;
use Drupal\user\UserInterface;

/**
 * Filter to make a one time login url.
 *
 * @DataFilter(
 *   id = "one_time_login_url",
 *   label = @Translation("Provides the one time login url for this user."),
 * )
 *
 * @package Drupal\communication_user\Plugin\TypedDataFilter
 */
class OneTimeLoginUrl extends DataFilterBase {

  /**
   * {@inheritdoc}
   */
  public function filter(
    DataDefinitionInterface $definition,
    $value,
    array $arguments,
    BubbleableMetadata $bubbleable_metadata = NULL
  ) {
    assert($value instanceof UserInterface);
    return user_pass_reset_url($value);
  }

  /**
   * {@inheritdoc}
   */
  public function canFilter(DataDefinitionInterface $definition) {
    return ($definition instanceof EntityDataDefinitionInterface) && $definition->getEntityTypeId() === 'user';
  }

  /**
   * {@inheritdoc}
   */
  public function filtersTo(DataDefinitionInterface $definition, array $arguments) {
    return DataDefinition::create('uri');
  }
}
