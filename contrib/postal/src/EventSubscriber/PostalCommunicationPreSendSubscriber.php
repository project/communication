<?php

namespace Drupal\communication_postal\EventSubscriber;

use Symfony\Component\Mime\MimeTypesInterface;
use Drupal\communication\Event\CommunicationEvents;
use Drupal\communication\Event\CommunicationOperationEvent;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\pdf_tools\PDFGeneratorInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class PostalCommunicationPreSendSubscriber implements EventSubscriberInterface {

  /**
   * @var \Drupal\pdf_tools\PDFGeneratorInterface
   */
  protected $pdfGenerator;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Symfony\Component\Mime\MimeTypesInterface
   */
  protected $mimeTypeGuesser;

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $fileStorage;

  /**
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * PostalCommunicationPreSendSubscriber constructor.
   *
   * @param \Drupal\pdf_tools\PDFGeneratorInterface $pdf_generator
   */
  public function __construct(
    PDFGeneratorInterface $pdf_generator,
    EntityTypeManagerInterface $entity_type_manager,
    MimeTypesInterface $mime_type_guesser,
    FileSystemInterface $file_system
  ) {
    $this->pdfGenerator = $pdf_generator;
    $this->entityTypeManager = $entity_type_manager;
    $this->mimeTypeGuesser = $mime_type_guesser;
    $this->fileStorage = $this->entityTypeManager->getStorage('file');
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      CommunicationEvents::PRE_OPERATION => 'preCommunicationSendPDFGenerate',
    ];
  }

  /**
   * Generate a pdf prior to sending.
   *
   * @param \Drupal\communication\Event\CommunicationOperationEvent $event
   */
  public function preCommunicationSendPDFGenerate(CommunicationOperationEvent $event) {
    if ($event->getOperation() !== 'send' || $event->getCommunication()->bundle() !== 'post') {
      return;
    }

    // Don't generate a pdf if we already have one.
    if (!$event->getCommunication()->pdf->isEmpty()) {
      return;
    }

    $pdf_options = $event->getOption('pdf_generation', []);
    if (
      !empty($pdf_options['pdf_style'])
      && !$event->getCommunication()->pdf_style->isEmpty()
    ) {
      $pdf_options['pdf_style'] = $event->getCommunication()->pdf_style->entity;
    }
    if (empty($pdf_options['__destination'])) {
      /** @var \Drupal\Core\Field\FieldDefinitionInterface $field_def */
      $field_def = $event->getCommunication()->pdf->getFieldDefinition();
      $pdf_options['__destination'] =
        $field_def->getSetting('uri_scheme', 'private') . '://'
        . $field_def->getSetting('file_directory', 'postal_mail')
        . '/communication-' . $event->getCommunication()->id() . '.pdf';
    }

    $uri = $this->pdfGenerator->entityToPDF($event->getCommunication(), 'send', $pdf_options);

    /** @var \Drupal\file\Entity\File $file */
    $file = $this->fileStorage->create([
      'uri' => $uri,
      'size' => filesize($uri),
      'uid' => \Drupal::currentUser()->id(),
      'status' => 1,
      'filename' => basename($uri),
      'filemime' => $this->mimeTypeGuesser->guess($uri),
    ]);

    $this->fileSystem->chmod($file->getFileUri());
    $file->save();

    $event->getCommunication()->pdf = $file;
  }
}
