<?php

namespace Drupal\communication\Entity;

use Drupal\communication\Contact\ContactInfo;
use Drupal\communication\Contact\ContactInfoEntitySyncableInterface;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Class CommunicationParticipant
 *
 * @ContentEntityType(
 *   id = "communication_participant",
 *   label = @Translation("Communication Participant"),
 *   base_table = "communication_participant",
 *   revision_table = "communication_participant_revision",
 *   handlers = {
 *     "storage" = "Drupal\communication\Entity\CommunicationParticipantStorage",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\communication\Entity\CommunicationParticipantAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\communication\Form\CommunicationParticipantForm",
 *     },
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "uuid" = "uuid",
 *     "bundle" = "type",
 *   },
 *   admin_permission = "administer communications",
 *   field_ui_base_route = "communication.participant_type_configure"
 * )
 *
 * @package Drupal\communication\Entity
 */
class CommunicationParticipant extends ContentEntityBase implements ContactInfoEntitySyncableInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['communication'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Communication'))
      ->setSetting('target_type', 'communication')
      ->setCardinality(1)
      ->setDisplayConfigurable('view', TRUE);

    $fields['role'] = BaseFieldDefinition::create('list_string')
      ->setLabel(new TranslatableMarkup('Role'))
      ->setSetting('allowed_values_function', [static::class, 'roleOptions'])
      ->setCardinality(1)
      ->setDisplayConfigurable('view', TRUE);

    // It is possible to enable this module without any contact middleware enabled.
    if (\Drupal::hasService('communication.contact_middleware')) {
      $fields['contact'] = BaseFieldDefinition::create('contact_reference')
        ->setLabel(new TranslatableMarkup('Contact'))
        ->setCardinality(1)
        ->setDisplayConfigurable('view', TRUE)
        ->setDisplayOptions('form', [
          'type' => 'contact_reference_autocomplete',
        ])
        ->setDisplayConfigurable('form', TRUE);
    }

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Name'))
      ->setCardinality(1)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions) {
    $fields = parent::bundleFieldDefinitions($entity_type, $bundle, $base_field_definitions);

    /** @var \Drupal\communication\CommunicationParticipantTypePluginManager $participant_type_manager */
    $participant_type_manager = \Drupal::service('plugin.manager.communication.participant_type');
    try {
      /** @var \Drupal\communication\Plugin\Communication\ParticipantType\ParticipantTypeInterface $plugin */
      $plugin = $participant_type_manager->createInstance($bundle);
      $fields += $plugin->communicationParticipantFieldDefinitions($base_field_definitions);
    }
    catch (PluginException $e) {}

    return $fields;
  }

  /**
   * Get the role options.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $definition
   * @param \Drupal\Core\Entity\EntityInterface|NULL $entity
   * @param bool $cacheable
   *
   * @return array
   */
  public static function roleOptions(FieldDefinitionInterface $definition, EntityInterface $entity = NULL, &$cacheable = TRUE) {
    $cacheable = FALSE;
    $mode_manager = \Drupal::service('plugin.manager.communication.mode');

    $role_options = [];
    if (!empty($entity->communication->entity) && $entity->communication->entity->bundle()) {
      $mode = $entity->communication->entity->bundle();
      foreach ($mode_manager->createInstance($mode)->participantRoles() as $role_name => $role_info) {
        $role_options[$role_name] = $role_info['label'];
      }
    }
    else {
     foreach ($mode_manager->getDefinitions() as $mode_name => $mode_info) {
       foreach ($mode_manager->createInstance($mode_name)->participantRoles() as $role_name => $role_info) {
         $role_options[$role_name] = $role_info['label'];
       }
     }
    }

    return $role_options;
  }

  /**
   * Get the participant type plugin.
   *
   * @return \Drupal\communication\Plugin\Communication\ParticipantType\ParticipantTypeInterface
   */
  public function getType() {
    return \Drupal::service('plugin.manager.communication.participant_type')->createInstance($this->bundle());
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->getType()->participantLabel($this);
  }

  /**
   * Get the contact info definition needed to sync with this entity.
   *
   * @return \Drupal\communication\Contact\ContactInfoDefinitionInterface
   */
  public function requiredContactInfoDefinition() {
    return $this->getType()->requiredContactInfoDefinition();
  }

  /**
   * {@inheritdoc}
   */
  public function onChange($name) {
    parent::onChange($name);

    if ($name == 'contact' && ($this->get('contact')->key)) {
      /** @var \Drupal\communication\Contact\ContactInfoInterface $contact_info */
      $contact_info = $this->get('contact')->info;
      \Drupal::service('communication.contact_middleware')->populateParticipant($this, $contact_info);
    }
  }

}
