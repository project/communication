<?php

namespace Drupal\communication_user\Plugin\TypedDataFilter;

use Drupal\Core\Entity\TypedData\EntityDataDefinitionInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\typed_data\DataFilterBase;
use Drupal\user\UserInterface;

/**
 * Filter to make a regtistration confirmation link.
 *
 * This needs the user_registrationpassword module to be enabled, without that
 * module it will fallback to the core drupal one time login link.
 *
 * @DataFilter(
 *   id = "registration_password_confirm_url",
 *   label = @Translation("Provides the one time login url for this user."),
 * )
 *
 * @package Drupal\communication_user\Plugin\TypedDataFilter
 */
class UserRegistrationPasswordConfirmationUrl extends DataFilterBase {

  /**
   * {@inheritdoc}
   */
  public function filter(
    DataDefinitionInterface $definition,
    $value,
    array $arguments,
    BubbleableMetadata $bubbleable_metadata = NULL
  ) {
    assert($value instanceof UserInterface);
    return is_callable('user_registrationpassword_confirmation_url') ? user_registrationpassword_confirmation_url($value) : user_pass_reset_url($value);
  }

  /**
   * {@inheritdoc}
   */
  public function canFilter(DataDefinitionInterface $definition) {
    return ($definition instanceof EntityDataDefinitionInterface) && $definition->getEntityTypeId() === 'user';
  }

  /**
   * {@inheritdoc}
   */
  public function filtersTo(DataDefinitionInterface $definition, array $arguments) {
    return DataDefinition::create('uri');
  }
}
