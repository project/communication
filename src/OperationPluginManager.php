<?php

namespace Drupal\communication;

use Drupal\communication\Entity\Communication;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

class OperationPluginManager extends DefaultPluginManager {

  /**
   * Constructs a OperationPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Communication/Operation',
      $namespaces,
      $module_handler, 'Drupal\communication\Plugin\Communication\Operation\OperationInterface',
      'Drupal\communication\Annotation\CommunicationOperation'
    );
    $this->alterInfo('communication_operation');
    $this->setCacheBackend($cache_backend, 'communication_operation');
  }

  /**
   * Get all the available operations for a communication.
   *
   * @param \Drupal\communication\Entity\Communication $communication
   * @param array $options
   *
   * @return \Drupal\communication\Plugin\Communication\Operation\OperationInterface[]
   */
  public function availableOperations(Communication $communication, array $options = []) {
    $options += [
      'exclude' => [],
      'configuration' => [],
    ];
    $available_operations = [];

    foreach ($this->getDefinitions() as $plugin_id => $plugin_definition) {
      if (in_array($plugin_id, $options['exclude'])) {
        continue;
      }

      /** @var \Drupal\communication\Plugin\Communication\Operation\OperationInterface $plugin */
      $plugin = $this->createInstance($plugin_id);
      if ($plugin->applicable($communication, $options['configuration'])) {
        $available_operations[$plugin_id] = $plugin;
      }
    }

    return $available_operations;
  }

}
