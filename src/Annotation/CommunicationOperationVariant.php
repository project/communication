<?php

namespace Drupal\communication\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a CommunicationOperationVariant plugin annotation object.
 *
 * Plugin Namespace: Plugin\Communication\OperationVariant.
 *
 * @see \Drupal\communication\Plugin\Communication\OperationVariant\OperationVariantInterface
 * @see \Drupal\communication\Plugin\Communication\OperationVariant\OperationVariantBase
 *
 * @ingroup plugin_api
 *
 * @Annotation
 */
class CommunicationOperationVariant extends Plugin {

  /**
   * The id of this mode.
   *
   * @var string
   */
  public $id;

  /**
   * The label of this mode.
   *
   * @var string
   */
  public $label;

  /**
   * The operation this is a variant of.
   *
   * @var string
   */
  public $operation;

  /**
   * The modes this operation applies to.
   *
   * @var array
   */
  public $modes = [];

  /**
   * The modes this operation cannot apply to.
   *
   * @var array
   */
  public $exclude_modes = [];
}
