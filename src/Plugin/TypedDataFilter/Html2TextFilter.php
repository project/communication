<?php

namespace Drupal\communication\Plugin\TypedDataFilter;


use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\Core\TypedData\Type\StringInterface;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\typed_data\DataFilterBase;
use Html2Text\Html2Text;

/**
 * A data filter for formatting fields.
 *
 * @DataFilter(
 *   id = "html2text",
 *   label = @Translation("Converts a html into text suitable for an email."),
 * )
 */
class Html2TextFilter extends DataFilterBase {

  /**
   * {@inheritdoc}
   */
  public function filter(
    DataDefinitionInterface $definition,
    $value,
    array $arguments,
    BubbleableMetadata $bubbleable_metadata = NULL
  ) {
    if ($value instanceof TypedDataInterface) {
      $value = $value->getValue();
    }

    return (new Html2Text($value))->getText();
  }

  /**
   * {@inheritdoc}
   */
  public function canFilter(DataDefinitionInterface $definition) {
    return is_subclass_of($definition->getClass(), StringInterface::class);
  }

  /**
   * {@inheritdoc}
   */
  public function filtersTo(DataDefinitionInterface $definition, array $arguments) {
    return DataDefinition::create('string');
  }

}
