<?php

namespace Drupal\communication\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;

/**
 * Class ParticipantItem
 *
 * Extend EntityReferenceItem so that normalization and denormalization can
 * behave differently.
 *
 * @package Drupal\communication\Plugin\Field\FieldType
 */
class ParticipantItem extends EntityReferenceItem {
}
