<?php

namespace Drupal\communication_service\Plugin\Communication\NotificationMethod;

use Drupal\communication\Entity\CommunicationEvent;
use Drupal\communication_subscription\Entity\Subscription;
use Drupal\communication_subscription\Plugin\Communication\NotificationMethod\NotificationMethodBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\entity\BundleFieldDefinition;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class Webhook
 *
 * @NotificationMethod(
 *   id = "webhook",
 *   label = @Translation("Web Hook"),
 * )
 *
 * @package Drupal\communication_service\Plugin\CommunicationNotificationMethod
 */
class Webhook extends NotificationMethodBase implements ContainerFactoryPluginInterface {

  /**
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * @var \Symfony\Component\Serializer\SerializerInterface
   */
  protected $serializer;

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return \Drupal\communication_service\Plugin\CommunicationNotificationMethod\Webhook|\Drupal\Core\Plugin\ContainerFactoryPluginInterface
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('http_client'),
      $container->get('serializer')
    );
  }

  /**
   * Webhook constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \GuzzleHttp\ClientInterface $http_client
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ClientInterface $http_client, SerializerInterface $serializer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->httpClient = $http_client;
    $this->serializer = $serializer;
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    $fields['uri'] = BundleFieldDefinition::create('uri')
      ->setRequired(TRUE)
      ->setLabel(new TranslatableMarkup('Notification URI'))
      ->setDescription(new TranslatableMarkup('The URI the notification will be sent to. {event} will be replaced with the event type and {communication} will be replaced with the id of the communication.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'uri',
      ]);

    $fields['secret'] = BundleFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('WebHook Secret'))
      ->setDescription(new TranslatableMarkup('A secret string passed with the request to authenticate the webhook.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ]);

    $fields['format'] = BundleFieldDefinition::create('list_string')
      ->setLabel(new TranslatableMarkup('Format'))
      ->setSetting('allowed_values', [
        'json' => 'JSON',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'options_select',
      ]);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function notify(CommunicationEvent $event, Subscription $subscription) {
    $url = $subscription->url->value;
    $url = str_replace(
      ['{communication}', '{event}'],
      [$event->communication->target_id, $event->bundle()],
      $url
    );

    $options = [
      'body' => $this->serializer->serialize($event, $subscription->format->value ?: 'json'),
    ];
    if ($subscription->secret->value) {
      $options['headers']['Authorization'] = 'Bearer '.$subscription->secret->value;
    }

    $this->httpClient->request(
      'POST',
      $url,
      $options
    );
  }
}
