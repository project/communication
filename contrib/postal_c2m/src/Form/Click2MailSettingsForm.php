<?php

namespace Drupal\communication_postal_c2m\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Tests\text\Functional\TextRequiredSummaryUpdateTest;

class Click2MailSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'communication_postal_c2m.credentials',
      'communication_postal_c2m.settings'
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'click2mail_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => 'Enabled',
      '#default_value' => !empty($this->config('communication_postal_c2m.settings')->get('enabled')),
    ];

    $form['credentials'] = [
      '#type' => 'details',
      '#title' => new TranslatableMarkup('Credentials'),
      '#open' => TRUE,
    ];
    $form['credentials']['username'] = [
      '#type' => 'textfield',
      '#title' => new TranslatableMarkup('Username'),
      '#default_value' => $this->config('communication_postal_c2m.credentials')->get('username'),
    ];
    $form['credentials']['password'] = [
      '#type' => 'password',
      '#title' => new TranslatableMarkup('Password'),
      '#description' => new TranslatableMarkup('The click2mail password.'),
    ];
    if ($this->config('communication_postal_c2m.credentials')->get('password')) {
      $form['credentials']['password']['#description'] = new TranslatableMarkup('The current password is *********. Leave black to keep using this password.');
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Store enabled
    $config = $this->config('communication_postal_c2m.settings');
    $config->set('enabled', $form_state->getValue('enabled'));
    $config->save();

    // Store credentials updates.
    $config = $this->config('communication_postal_c2m.credentials');
    if (empty(trim($form_state->getValue('username')))) {
      $config->set('username', '');
      $config->set('password', '');
    }
    else {
      $config->set('username', $form_state->getValue('username'));

      if (!empty(trim($form_state->getValue('password')))) {
        $config->set('password', trim($form_state->getValue('password')));
      }
    }
    $config->save();

    parent::submitForm($form, $form_state);
  }
}
