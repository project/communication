<?php

namespace Drupal\communication\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a CommunicationMode plugin annotation object.
 *
 * Plugin Namespace: Plugin\CommunicationParticipantType.
 *
 * @see \Drupal\communication\Plugin\CommunicationParticipantType\CommunicationParticipantTypeInterface
 * @see \Drupal\communication\Plugin\CommunicationParticipantType\CommunicationParticipantTypeBase
 *
 * @ingroup plugin_api
 *
 * @Annotation
 */
class CommunicationParticipantType extends Plugin {

  /**
   * The id of this mode.
   *
   * @var string
   */
  public $id;

  /**
   * The label of this mode.
   *
   * @var string
   */
  public $label;

}
