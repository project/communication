<?php

namespace Drupal\communication;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

class CommunicationParticipantTypePluginManager extends DefaultPluginManager {

  /**
   * Constructs a CommunicationParticipantTypePluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Communication/ParticipantType',
      $namespaces,
      $module_handler,
      'Drupal\communication\Plugin\Communication\ParticipantType\ParticipantTypeInterface',
      'Drupal\communication\Annotation\CommunicationParticipantType'
    );
    $this->alterInfo('communication_participant_type');
    $this->setCacheBackend($cache_backend, 'communication_participant_type');
  }
}
