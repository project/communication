<?php

namespace Drupal\communication\Plugin\Communication\Operation;

/**
 * Class SendOperation
 *
 * @CommunicationOperation(
 *   id = "send",
 *   label = @Translation("Send"),
 * )
 *
 * @package Drupal\communication\Plugin\Communication\Operation
 */
class SendOperation extends OperationWithVariantsBase {

}
