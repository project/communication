<?php

namespace Drupal\communication\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a CommunicationMode plugin annotation object.
 *
 * Plugin Namespace: Plugin\CommunicationMode.
 *
 * @see \Drupal\communication\Plugin\Communication\Mode\ModeInterface
 * @see \Drupal\communication\Plugin\Communication\Mode\CommunicationModeBase
 *
 * @ingroup plugin_api
 *
 * @Annotation
 */
class CommunicationMode extends Plugin {

  /**
   * The id of this mode.
   *
   * @var string
   */
  public $id;

  /**
   * The label of this mode.
   *
   * @var string
   */
  public $label;

  /**
   * Information about participants on communications of this mode.
   *
   * @var array
   */
  public $participants = [];
}
