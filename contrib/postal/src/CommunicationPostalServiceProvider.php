<?php

namespace Drupal\communication_postal;

use Drupal\communication_postal\EventSubscriber\PostalCommunicationPreSendSubscriber;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class CommunicationPostalServiceProvider
 *
 * @package Drupal\communication_postal
 */
class CommunicationPostalServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    // We cannot use the module handler as the container is not yet compiled.
    // @see \Drupal\Core\DrupalKernel::compileContainer()
    $modules = $container->getParameter('container.modules');

    // If the pdf_tools module exists, we can generate pdfs for the letters
    // on the fly, register an event subscriber to take care of this.
    if (isset($modules['pdf_tools'])) {
      $container->register('communication_postal.pre_send.subscriber',PostalCommunicationPreSendSubscriber::class)
        ->setArguments([
          new Reference('pdf_tools.generator'),
          new Reference('entity_type.manager'),
          new Reference('file.mime_type.guesser'),
          new Reference('file_system'),
        ])
        ->addTag('event_subscriber');
    }
  }

}
