<?php

namespace Drupal\communication\Plugin\DataType;

use Drupal\Core\TypedData\TypedData;

/**
 * Data type for contact info data.
 *
 * @DataType(
 *   id = "contact_info",
 *   label = @Translation("Contact Info"),
 * )
 */
class ContactInfoData extends TypedData {

  /**
   * @var \Drupal\communication\Contact\ContactInfoInterface
   */
  protected $value;

}
