<?php

namespace Drupal\communication\Plugin\Communication\OperationVariant;

use Drupal\communication\Entity\CommunicationInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class OperationVariantBase extends PluginBase implements OperationVariantInterface, ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * OperationVariantBase constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Get communication event storage.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function eventStorage() {
    return $this->entityTypeManager->getStorage('communication_event');
  }

  /**
   * {@inheritdoc}
   */
  public function applicable(CommunicationInterface $communication, array $options = []) {
    if (!in_array($communication->mode->value, $this->pluginDefinition['modes']) && !empty($this->pluginDefinition['modes'])) {
      return FALSE;
    }

    if (in_array($communication->mode->value, $this->pluginDefinition['exclude_modes'])) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function hasForm(CommunicationInterface $communication, array $options = []) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state, CommunicationInterface $communication) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function formValidate(array $form, FormStateInterface $form_state, CommunicationInterface $communication) {
  }

  /**
   * {@inheritdoc}
   */
  public function formSubmit(array $form, FormStateInterface $form_state, CommunicationInterface $communication) {
  }

  /**
   * {@inheritdoc}
   */
  public function formPostOperation(array $form, FormStateInterface $form_state, CommunicationInterface $communication) {
  }
}
