<?php

namespace Drupal\communication\Annotation;

use Drupal\Component\Annotation\Plugin;

class ContactInfoUsageValidator extends Plugin {

  /**
   * The id of this mode.
   *
   * @var string
   */
  public $id;

  /**
   * The label of this mode.
   *
   * @var string
   */
  public $label;

}
