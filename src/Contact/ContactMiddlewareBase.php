<?php

namespace Drupal\communication\Contact;

use Drupal\communication\ContactInfoSourceManager;
use Drupal\communication\Entity\CommunicationParticipant;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class ContactMiddlewareBase implements ContactMiddlewareInterface {

  /**
   * @var \Drupal\communication\ContactInfoSourceManager
   */
  protected $contactInfoSourceManager;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * @var array
   */
  protected $_collectionCache;

  /**
   * ContactMiddlewareBase constructor.
   *
   * @param \Drupal\communication\ContactInfoSourceManager $contact_info_source_manager
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(ContactInfoSourceManager $contact_info_source_manager, EntityTypeManagerInterface $entity_type_manager, CacheBackendInterface $cache) {
    $this->contactInfoSourceManager = $contact_info_source_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->cache = $cache;
  }

  /**
   * Get the entity type id this middleware handles.
   *
   * @return string
   */
  abstract public function contactEntityTypeId();

  /**
   * Get the selection handler for this middleware.
   *
   * @return string
   */
  public function contactSelectionHandler() {
    return 'default';
  }

  /**
   * @param array $values
   *
   * @return mixed|void
   */
  public function createContact(array $values = []) {
    $entity_type = $this->entityTypeManager->getDefinition($this->contactEntityTypeId());
    if (!empty($values['bundle'])) {
      if ($entity_type->hasKey('bundle')) {
        $values[$entity_type->getKey('bundle')] = $values['bundle'];
      }

      unset($values['bundle']);
    }

    return $this->entityTypeManager->getStorage($entity_type->id())->create($values);
  }

  /**
   * Test whether this plugin definition is applicable to this middleware.
   *
   * @return boolean
   */
  protected function applicableSourceDefinition($definition, $entity_type_id = NULL) {
    if (!$entity_type_id) {
      $entity_type_id = $this->contactEntityTypeId();
    }

    return $definition['entity_type_id'] == 'any' || $definition['entity_type_id'] == $entity_type_id;
  }

  /**
   * @param \Drupal\communication\Entity\CommunicationParticipant $participant
   *
   * @return \Drupal\communication\Contact\ContactInterface|void
   */
  public function findContactForParticipant(CommunicationParticipant $participant) {
    $suggestions = [];

    foreach ($this->contactInfoSourceManager()->getDefinitions() as $plugin_id => $definition) {
      $plugin = $this->contactInfoSourceManager->createInstance($plugin_id);
      $suggestions = $plugin->findContacts($participant);
    }

    if (count($suggestions) > 1 || !count($suggestions)) {
      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function populateParticipant(CommunicationParticipant $participant, ContactInfoInterface $contact_info) {
    $participant_fields = $participant->getFieldDefinitions();

    foreach ($participant_fields as $name => $definition) {
      if (in_array($name, ['contact', 'type', 'role', 'communication', 'uuid', 'vid', 'id', 'revision_default'])) {
        continue;
      }

      $data = $contact_info->getInfoValue($name, $definition);
      if (is_array($data)) {
        foreach ($data as $prop => $value) {
          $participant->{$name}->{$prop} = $value;
        }
      }
      else if ($data instanceof FieldItemListInterface) {
        $participant->{$name} = $data->get(0)->toArray();
      }
      else if ($data instanceof FieldItemInterface) {
        $participant->{$name} = $data->toArray();
      }
      else {
        $participant->{$name} = $data;
      }
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function collectContactInfo(EntityInterface $entity, ContactInfoDefinitionInterface $definition, array $options = []) {
    if (!isset($this->_collectionCache[$entity->id()][$definition->getDataType()]) || !empty($options['reset'])) {
      //$cache_key = 'collection:'.$entity->id().':'.$definition->getDataType();
      //if (empty($options['reset']) && ($collection = $this->cache->get($cache_key))) {
      //  $this->_collectionCache[$entity->id()][$definition->getDataType()] = $collection->data;
      //}
      //else {
        /** @var \Drupal\communication\Contact\ContactInfoInterface[] $contact_infos */
        $contact_infos = [];

        foreach ($this->contactInfoSourceManager->getDefinitions() as $plugin_id => $plugin_definition) {
          if (!$this->applicableSourceDefinition($plugin_definition, $entity->getEntityTypeId())) {
            continue;
          }

          /** @var \Drupal\communication\Plugin\ContactInfoSource\ContactInfoSourceInterface $plugin */
          $plugin = $this->contactInfoSourceManager->createInstance($plugin_id);
          $collected_info = $plugin->collectInfo($entity, $definition, $options);

          if (is_array($collected_info)) {
            foreach ($collected_info as $sub_key => $contact_info) {
              $contact_infos[$plugin_id . '.' . $sub_key] = $contact_info;
            }
          }
          else {
            if ($collected_info instanceof ContactInfoInterface) {
              $contact_infos[$plugin_id] = $collected_info;
            }
          }
        }

        //$tags = ['contact:'.$entity->id()];
        //$max_age = Cache::PERMANENT;
        //foreach ($contact_infos as $contact_info) {
        //  $tags = Cache::mergeTags($tags, $contact_info->getCacheTags());
        //  $max_age = Cache::mergeMaxAges($max_age, $contact_info->getCacheMaxAge());
        //}
        //$this->cache->set($cache_key, array_keys($contact_infos), $max_age, $tags);
        $this->_collectionCache[$entity->id()][$definition->getDataType()] = $contact_infos;
      //}
    }

    return $this->_collectionCache[$entity->id()][$definition->getDataType()];
  }

  /**
   * Get the contact info source manager.
   *
   * @return \Drupal\communication\ContactInfoSourceManager
   */
  protected function contactInfoSourceManager() {
    if (empty($this->contactInfoSourceManager)) {
      $this->contactInfoSourceManager = \Drupal::service('plugin.manager.communication.contact_info_source');
    }

    return $this->contactInfoSourceManager;
  }

  /**
   * @param $id
   *
   * @return \Drupal\Core\Entity\EntityInterface|void
   */
  public function loadContact($id) {
    return $this->entityTypeManager->getStorage($this->contactEntityTypeId())->load($id);
  }
}
