<?php

namespace Drupal\communication\Contact;

use Drupal\communication\Plugin\ContactInfoSource\ContactInfoSourceMetadataInterface;
use Drupal\Core\Cache\CacheableDependencyTrait;
use Drupal\Core\Cache\RefinableCacheableDependencyTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinitionInterface;

class ContactInfo implements ContactInfoInterface {
  use RefinableCacheableDependencyTrait;

  protected $entity;

  protected $pluginId;

  protected $subKey;

  protected $values;

  /**
   * @var \Drupal\communication\Contact\ContactInfoDefinitionInterface
   */
  protected $definition;

  protected $complete = TRUE;

  /**
   * @var \Drupal\communication\Plugin\ContactInfoSource\ContactInfoSourceInterface
   */
  protected $_plugin;

  /**
   * ContactInfo constructor.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param $plugin_id
   * @param string $key
   * @param string $value
   */
  public function __construct(ContactInfoDefinitionInterface $definition, EntityInterface $entity, $plugin_id, $sub_key = '') {
    $this->definition = $definition;
    $this->entity = $entity;
    $this->pluginId = $plugin_id;
    $this->subKey = $sub_key;
  }

  /**
   * @param $entity
   * @param $key
   */
  public static function create(EntityInterface $entity, $key) {
    if (!stripos($key, '.')) {
      $key .= '.';
    }

    list($plugin_id, $sub_key) = explode('.', $key, 2);
    /** @var \Drupal\communication\Plugin\ContactInfoSource\ContactInfoSourceInterface $plugin */
    $plugin = \Drupal::service('plugin.manager.communication.contact_info_source')->createInstance($plugin_id);

    return new static(
      $plugin->getInfoDefinition($entity, $sub_key),
      $entity,
      $plugin_id,
      $sub_key
    );
  }

  /**
   * {@inheritdoc}
   */
  public function key() {
    if ($this->subKey) {
      return "{$this->pluginId}.{$this->subKey}";
    }
    else {
      return $this->pluginId;
    }
  }

  /**
   * Get the plugin.
   *
   * @return \Drupal\communication\Plugin\ContactInfoSource\ContactInfoSourceInterface
   */
  protected function getPlugin() {
    if (empty($this->_plugin)) {
      $this->_plugin = \Drupal::service('plugin.manager.communication.contact_info_source')->createInstance($this->pluginId);
    }

    return $this->_plugin;
  }

  /**
   * @param $name
   * @param $definition
   *
   * @return mixed
   */
  public function getInfoValue($name, DataDefinitionInterface $definition) {
    if (empty($this->values[$name])) {
      $this->values[$name] = $this->getPlugin()->getInfoValue($this->entity, $this->subKey, $name, $definition);
    }

    return $this->values[$name];
  }

  /**
   * {@inheritdoc}
   */
  public function supportsWriteBackInfoValue($name, DataDefinitionInterface $definition) {
    return $this->getPlugin()->supportsWriteBackInfoValue($this->entity, $this->subKey, $name, $definition);
  }

  /**
   * {@inheritdoc}
   */
  public function writeBackInfoValues(DataDefinitionInterface $definition, $values) {
    $updated_key = $this->getPlugin()->writeBackInfoValues($this->entity, $this->subKey, $definition, $values);
    $this->subKey = $updated_key;
    return $this->subKey;
  }

  /**
   * Get a human readable label for this piece of contact information to distinguish
   * it within the context of the contact.
   *
   * @return string
   */
  public function label() {
    return $this->getPlugin()->getLabel($this->entity, $this->subKey);
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    return $this->getPlugin()->getSummary($this->entity, $this->subKey);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinition() {
    if (!$this->definition) {
      $this->definition = $this->getPlugin()->getInfoDefinition($this->entity, $this->subKey);
    }

    return $this->definition;
  }

  /**
   * [@inheritdoc}
   */
  public function setDefinition(ContactInfoDefinitionInterface $definition) {
    $this->definition = $definition;
  }

  /**
   * {@inheritdoc}
   */
  public function setIncomplete() {
    $this->complete = FALSE;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setComplete() {
    $this->complete = TRUE;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isComplete() {
    return $this->complete;
  }

  /**
   * {@inheritdoc}
   */
  public function metadataForm(array $form, FormStateInterface $form_state) {
    $plugin = $this->getPlugin();
    if (!($plugin instanceof ContactInfoSourceMetadataInterface)) {
      return [];
    }

    return $plugin->metadataForm($form, $form_state, $this->entity, $this->subKey);
  }
}
