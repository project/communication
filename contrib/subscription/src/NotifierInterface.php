<?php
/**
 * Created by PhpStorm.
 * User: Rob
 * Date: 07/02/2020
 * Time: 11:13
 */

namespace Drupal\communication_subscription;

use Drupal\communication\Entity\CommunicationEvent;

/**
 * Interface NotifierInterface
 *
 * @package Drupal\communication_subscription
 */
interface NotifierInterface {

  /**
   * Notify all subscriptions of the event.
   *
   * @param \Drupal\communication\Entity\CommunicationEvent $event
   */
  public function notify(CommunicationEvent $event);

}
