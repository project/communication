<?php

namespace Drupal\communication\Plugin\Communication\ParticipantType;

use Drupal\communication\Entity\CommunicationParticipant;

interface ParticipantTypeInterface {

  /**
   * Get communication participant fields required for this type.
   *
   * @param \Drupal\Core\Field\BaseFieldDefinition[] $base_field_definitions
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   */
  public function communicationParticipantFieldDefinitions(array $base_field_definitions);

  /**
   * Get the participant label.
   *
   * @param \Drupal\communication\Entity\CommunicationParticipant $participant
   *
   * @return string|\Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public function participantLabel(CommunicationParticipant $participant);

  /**
   * The required contact info definition.
   *
   * @return \Drupal\communication\Contact\ContactInfoDefinitionInterface
   */
  public function requiredContactInfoDefinition();

}
