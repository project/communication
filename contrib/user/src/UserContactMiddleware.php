<?php

namespace Drupal\communication_user;

use Drupal\communication\Contact\ContactMiddlewareBase;

class UserContactMiddleware extends ContactMiddlewareBase {

  /**
   * {@inheritdoc}
   */
  public function contactEntityTypeId() {
    return 'user';
  }
}
