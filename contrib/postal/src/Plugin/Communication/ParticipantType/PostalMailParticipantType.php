<?php

namespace Drupal\communication_postal\Plugin\Communication\ParticipantType;

use Drupal\communication\Contact\ContactInfoDefinition;
use Drupal\communication\Entity\CommunicationParticipant;
use Drupal\communication\Plugin\Communication\ParticipantType\ParticipantTypeBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\entity\BundleFieldDefinition;

/**
 * Class PostalMailParticipantType
 *
 * @CommunicationParticipantType(
 *   id = "postal_mail",
 *   label = "Postal Address",
 * )
 *
 * @package Drupal\communication_postal\Plugin\Communication\ParticipantType
 */
class PostalMailParticipantType extends ParticipantTypeBase {

  /**
   * {@inheritdoc}
   */
  public function communicationParticipantFieldDefinitions(array $base_field_definitions) {
    $fields = [];
    $fields['address'] = BundleFieldDefinition::create('address')
      ->setLabel(new TranslatableMarkup('Address'))
      ->setRequired(TRUE)
      ->setProvider('communication_postal')
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'address_default',
      ])
      ->setDisplayConfigurable('form', TRUE);
    return $fields;
  }

  /**
   * [@inheritdoc}
   */
  public function participantLabel(CommunicationParticipant $participant) {
    $label = '';
    if ($participant->address->given_name || $participant->address->family_name) {
      $label .= implode(' ', array_filter([$participant->address->given_name, $participant->address->family_name]));

      if ($participant->address->organization) {
        $label .= ' at ';
      }
    }
    if ($participant->address->organization) {
      $label .= $participant->address->organization;
    }

    if (empty($label)) {
      $label = $participant->address->address_line1;
    }

    return $label;
  }

  /**
   * {@inheritdoc}
   */
  public function requiredContactInfoDefinition() {
    return ContactInfoDefinition::create('address');
  }
}
