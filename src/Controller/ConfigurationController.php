<?php

namespace Drupal\communication\Controller;

use Drupal\communication\CommunicationModePluginManager;
use Drupal\communication\CommunicationParticipantTypePluginManager;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConfigurationController
 *
 * @package Drupal\communication\Controller
 */
class ConfigurationController extends ControllerBase {

  /**
   * @var \Drupal\communication\CommunicationModePluginManager
   */
  protected $modeManager;

  /**
   * @var \Drupal\communication\CommunicationParticipantTypePluginManager
   */
  protected $participantTypeManager;

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.communication.mode'),
      $container->get('plugin.manager.communication.participant_type'),
      $container->get('module_handler')
    );
  }

  /**
   * ConfigurationController constructor.
   *
   * @param \Drupal\communication\CommunicationModePluginManager $mode_manager
   */
  public function __construct(CommunicationModePluginManager $mode_manager, CommunicationParticipantTypePluginManager $participant_type_manager, ModuleHandlerInterface $module_handler) {
    $this->modeManager = $mode_manager;
    $this->participantTypeManager = $participant_type_manager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * Build the modes overview page.
   *
   * @return array
   */
  public function modeOverview() {
    $build = [];

    $build['table'] = [
      '#type' => 'table',
      '#title' => new TranslatableMarkup('Communication Modes'),
      '#header' => [
        'mode' => new TranslatableMarkup('Mode'),
        'machine_name' => new TranslatableMarkup('Machine-Name'),
        'operations' => new TranslatableMarkup('Operations'),
      ],
      '#rows' => [],
      '#empty' => new TranslatableMarkup('No communication modes are installed.'),
    ];

    foreach ($this->modeManager->getDefinitions() as $plugin_id => $definition) {
      $operations = [];
      $operations['configure'] = [
        'title' => t('Configure'),
        'weight' => -10,
        'url' => Url::fromRoute('communication.mode_configure', [
          'bundle' => $plugin_id,
        ]),
      ];

      if ($this->moduleHandler->moduleExists('field_ui')) {
        $operations['manage-fields'] = [
          'title' => t('Manage fields'),
          'weight' => 15,
          'url' => Url::fromRoute("entity.communication.field_ui_fields", [
            'bundle' => $plugin_id,
          ]),
        ];
        $operations['manage-form-display'] = [
          'title' => t('Manage form display'),
          'weight' => 20,
          'url' => Url::fromRoute("entity.entity_form_display.communication.default", [
            'bundle' => $plugin_id,
          ]),
        ];
        $operations['manage-view-display'] = [
          'title' => t('Manage view display'),
          'weight' => 20,
          'url' => Url::fromRoute("entity.entity_view_display.communication.default", [
            'bundle' => $plugin_id,
          ]),
        ];
      }

      $build['table']['#rows'][] = [
        'mode' => ['data' => $definition['label']],
        'machine_name' => ['data' => $plugin_id],
        'operations' => [
          'data' => [
            '#type' => 'operations',
            '#links' => $operations,
          ],
        ],
      ];
    }

    return $build;
  }

  /**
   * Build the pariticipant type manager
   */
  public function participantTypeOverview() {
    $build = [];

    $build['table'] = [
      '#type' => 'table',
      '#title' => new TranslatableMarkup('Communication Participants'),
      '#header' => [
        'type' => new TranslatableMarkup('Type'),
        'machine_name' => new TranslatableMarkup('Machine-Name'),
        'operations' => new TranslatableMarkup('Operations'),
      ],
      '#rows' => [],
      '#empty' => new TranslatableMarkup('No participant types are installed.'),
    ];

    foreach ($this->participantTypeManager->getDefinitions() as $plugin_id => $definition) {
      $operations = [];
      $operations['configure'] = [
        'title' => t('Configure'),
        'weight' => -10,
        'url' => Url::fromRoute('communication.participant_type_configure', [
          'bundle' => $plugin_id,
        ]),
      ];

      if ($this->moduleHandler->moduleExists('field_ui')) {
        $operations['manage-fields'] = [
          'title' => t('Manage fields'),
          'weight' => 15,
          'url' => Url::fromRoute("entity.communication_participant.field_ui_fields", [
            'bundle' => $plugin_id,
          ]),
        ];
        $operations['manage-form-display'] = [
          'title' => t('Manage form display'),
          'weight' => 20,
          'url' => Url::fromRoute("entity.entity_form_display.communication_participant.default", [
            'bundle' => $plugin_id,
          ]),
        ];
        $operations['manage-view-display'] = [
          'title' => t('Manage view display'),
          'weight' => 20,
          'url' => Url::fromRoute("entity.entity_view_display.communication_participant.default", [
            'bundle' => $plugin_id,
          ]),
        ];
      }

      $build['table']['#rows'][] = [
        'type' => ['data' => $definition['label']],
        'machine_name' => ['data' => $plugin_id],
        'operations' => [
          'data' => [
            '#type' => 'operations',
            '#links' => $operations,
          ],
        ],
      ];
    }

    return $build;
  }

  /**
   * Get the mode title.
   *
   * @param string $bundle
   *
   * @return string
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function modeTitle(string $bundle) {
    $definition = $this->modeManager->getDefinition($bundle);
    return $definition['label'];
  }
}
