<?php

namespace Drupal\communication\Event;

/**
 * Class CommunicationEvents
 *
 * @package Drupal\communication\Event
 */
final class CommunicationEvents {

  /**
   * Constants for communication events.
   */
  const PRE_OPERATION = 'pre_operation';

}
