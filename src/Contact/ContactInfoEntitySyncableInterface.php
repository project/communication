<?php

namespace Drupal\communication\Contact;

interface ContactInfoEntitySyncableInterface {

  /**
   * Get the contact info definition needed to sync with this entity.
   *
   * @return \Drupal\communication\Contact\ContactInfoDefinitionInterface
   */
  public function requiredContactInfoDefinition();

}
