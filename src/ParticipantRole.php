<?php

namespace Drupal\communication;

/**
 * Class ParticipantRole
 *
 * Definition of a participant role.
 *
 * @package Drupal\communication
 */
class ParticipantRole implements \ArrayAccess {

  protected $cardinality = 1;

  protected $name;

  protected $label;

  protected $pluralName;

  protected $pluralLabel;

  protected $description;

  protected $type;

  /**
   * @param $name
   * @param $type
   * @param $label
   * @param int $cardinality
   * @param null $plural_name
   * @param null $plural_label
   * @param null $description
   *
   * @return \Drupal\communication\ParticipantRole
   */
  public static function create($name, $type, $label, $cardinality = 1, $plural_name = NULL, $plural_label = NULL, $description = NULL) {
    return new static(
      $name,
      $type,
      $label,
      $cardinality,
      $plural_name,
      $plural_label,
      $description
    );
  }

  public function __construct($name, $type, $label, $cardinality = 1, $plural_name = NULL, $plural_label = NULL, $description = NULL) {
    $this->name = $name;
    $this->type = $type;
    $this->label = $label;
    $this->cardinality = $cardinality;
    $this->pluralName = $plural_name;
    $this->pluralLabel = $plural_label;
    $this->description = $description;
  }

  public function getCardinality() {
    return $this->cardinality;
  }

  public function getLabel() {
    return $this->label;
  }

  public function getPluralLabel() {
    return $this->pluralLabel ?: $this->getLabel().'s';
  }

  public function getName() {
    return $this->name;
  }

  public function getPluralName() {
    return $this->pluralName ?: $this->getName().'s';
  }

  public function getType() {
    return $this->type;
  }

  /**
   * Get the participant type plugin from the role.
   *
   * @return \Drupal\communication\Plugin\Communication\ParticipantType\ParticipantTypeInterface
   */
  public function getTypePlugin() {
    return \Drupal::service('plugin.manager.communication.participant_type')
      ->createInstance($this->getType());
  }

  public function getFieldName() {
    return $this->getCardinality() == 1 ? $this->getName() : $this->getPluralName();
  }

  public function getFieldLabel() {
    return $this->getCardinality() == 1 ? $this->getLabel() : $this->getPluralLabel();
  }

  public function getDescription() {
    return $this->description ?: NULL;
  }

  public function setDescription($description = '') {
    $this->description = $description;

    return $this;
  }

  public function setCardinality($cardinality = 1) {
    $this->cardinality = $cardinality;
    return $this;
  }

  public function setName($name) {
    $this->name = $name;
    return $this;
  }

  public function setPluralName($name) {
    $this->pluralName = $name;
    return $this;
  }

  public function setLabel($label) {
    $this->label = $label;
    return $this;
  }

  public function setPluralLabel($label) {
    $this->pluralLabel = $label;
    return $this;
  }

  public function setType($type) {
    $this->type = $type;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function offsetExists($offset) : bool {
    return in_array($offset, ['name', 'plural_name','label', 'plural_label', 'description', 'cardinality', 'type']);
  }

  /**
   * {@inheritdoc}
   */
  public function offsetGet($offset)  : mixed {
    switch ($offset) {
      case 'name':
        return $this->getName();
        break;
      case 'plural_name':
        return $this->getPluralName();
        break;
      case 'label':
        return $this->getLabel();
        break;
      case 'plural_label':
        return $this->getPluralLabel();
        break;
      case 'cardinality':
        return $this->getCardinality();
        break;
      case 'description':
        return $this->getDescription();
        break;
      case 'type':
        return $this->getType();
        break;
      default:
        return NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function offsetSet($offset, $value) : void {
    switch ($offset) {
      case 'name':
        $this->setName($value);
        break;
      case 'plural_name':
        $this->setPluralName($value);
        break;
      case 'label':
        $this->setLabel($value);
        break;
      case 'plural_label':
        $this->setPluralLabel($value);
        break;
      case 'cardinality':
        $this->setCardinality($value);
        break;
      case 'description':
        $this->setDescription($value);
        break;
      case 'type':
        $this->setType($value);
        break;
      default:
        throw new \Exception('Unavailable property '.$offset);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function offsetUnset($offset) : void {
    // Do Nothing.
  }
}
