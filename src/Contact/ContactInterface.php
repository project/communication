<?php

namespace Drupal\communication\Contact;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\TypedData\DataDefinitionInterface;

interface ContactInterface {

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param string $key
   *
   * @return mixed
   */
  public static function create(EntityInterface $entity, $key = '');

  /**
   * Get the entity this contact interface wraps.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   */
  public function getEntity();

  /**
   * Get the participant data from this contact.
   *
   * @param $name
   * @param \Drupal\Core\TypedData\DataDefinition $definition
   *
   * @return mixed
   */
  public function getParticipantData($name, DataDefinitionInterface $definition);

}
