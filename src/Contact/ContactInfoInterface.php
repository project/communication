<?php

namespace Drupal\communication\Contact;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinitionInterface;

interface ContactInfoInterface extends CacheableDependencyInterface, RefinableCacheableDependencyInterface {

  /**
   * @param $name
   * @param $definition
   *
   * @return mixed
   */
  public function getInfoValue($name, DataDefinitionInterface $definition);

  /**
   * Get the label for this contact info.
   *
   * @return string
   */
  public function label();

  /**
   * Get a string/html summary of this contact info.
   *
   * @return string
   */
  public function summary();

  /**
   * Get the key that identifies this piece of contact info.
   *
   * @return string
   */
  public function key();

  /**
   * Find out whether changes to this piece of contact info can be written back
   * to the source.
   *
   * @param $name
   * @param \Drupal\Core\TypedData\DataDefinitionInterface $definition
   *
   * @return boolean
   */
  public function supportsWriteBackInfoValue($name, DataDefinitionInterface $definition);

  /**
   * Write back a value to the source.
   *
   * @param $name
   * @param \Drupal\Core\TypedData\DataDefinitionInterface $definition
   * @param $value
   *
   * @return string
   *   Updated key.
   */
  public function writeBackInfoValues(DataDefinitionInterface $definition, $values);

  /**
   * Mark a contact info as incomplete.
   *
   * @return static
   */
  public function setIncomplete();

  /**
   * Mark a contact info as complete.
   *
   * @return static
   */
  public function setComplete();

  /**
   * Is this contact info complete.
   *
   * @return boolean
   */
  public function isComplete();

  /**
   * Get a form for editing metadata
   *
   * @return array
   */
  public function metadataForm(array $form, FormStateInterface $form_state);
}
