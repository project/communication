<?php

namespace Drupal\communication\Plugin\Communication\ParticipantType;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\entity\BundleFieldDefinition;

abstract class ParticipantTypeBase extends PluginBase implements ParticipantTypeInterface {

  /**
   * {@inheritdoc}
   */
  public function communicationParticipantFieldDefinitions(array $base_field_definitions) {
    $fields = [];
    $fields['name'] = BundleFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Name'))
      ->setProvider('communication')
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);
    return $fields;
  }
}
