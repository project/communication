<?php

namespace Drupal\communication\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Class SetFormValueCommand
 *
 * @package Drupal\communication\Ajax
 */
class SetFormValueCommand implements CommandInterface {

  protected $form_selector = NULL;

  protected $form_build_id = NULL;

  protected $name;

  protected $value;

  /**
   * SetFormValueCommand constructor.
   *
   * @param $name
   * @param null $value
   * @param null $form_selector
   */
  public function __construct($name, $value = NULL, $form_selector = NULL, $form_build_id = NULL) {
    $this->form_selector = $form_selector;
    $this->form_build_id = $form_build_id;
    $this->name = $name;
    $this->value = $value;
  }

  /**
   * Return an array to be run through json_encode and sent to the client.
   */
  public function render() {
    return [
      'command' => 'set_form_value',
      'form_selector' => $this->form_selector,
      'form_build_id' => $this->form_build_id,
      'element_name' => $this->name,
      'data' => $this->value,
    ];
  }
}
