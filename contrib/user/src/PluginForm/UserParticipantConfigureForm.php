<?php

namespace Drupal\communication_user\PluginForm;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\entity_template_ui\Form\TemplateUIHelperTrait;
use Drupal\typed_data\DataFetcherInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserParticipantConfigureForm extends PluginFormBase implements ContainerInjectionInterface {
  use StringTranslationTrait;
  use TemplateUIHelperTrait;

  /**
   * The data fetcher service.
   *
   * @var \Drupal\typed_data\DataFetcherInterface
   */
  protected $dataFetcher;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('typed_data.data_fetcher')
    );
  }

  /**
   * UserParticipantConfigureForm constructor.
   *
   * @param \Drupal\typed_data\DataFetcherInterface $data_fetcher
   */
  public function __construct(DataFetcherInterface $data_fetcher) {
    $this->dataFetcher = $data_fetcher;
  }

  /**
   * The component plugin.
   *
   * @var \Drupal\communication_user\Plugin\EntityTemplate\Component\UserParticipant
   */
  protected $plugin;

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $configuration = $this->plugin->getConfiguration();

    $form['selector'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Select Contact'),
      '#default_value' => isset($configuration['selector']) ? $configuration['selector'] : '',
      '#attributes' => [
        'class' => [ 'entity-template-ui-autocomplete' ],
      ],
      '#attached' => [
        'library' => [
          'entity_template_ui/entity_template_ui.autocomplete',
        ],
      ],
    ] + $this->getTemplateUi($this->plugin->getTemplate())
      ->dataSelectAutocompleteElement(
        $configuration['uuid']
      );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $selector = $form_state->getValue('selector');
    [$context, $path] = explode('.', $selector.'.', 2);

    $template = $this->plugin->getTemplate();

    if (empty($path)) {
      $selected_data_def = $template
        ->getContextDefinition($context)
        ->getDataDefinition();
    }
    else {
      $path = substr($path, 0, -1);
      $selected_data_def = $this->dataFetcher->fetchDefinitionByPropertyPath(
        $template->getContextDefinition($context)->getDataDefinition(),
        $path
      );
    }

    if ($selected_data_def->getDataType() !== 'entity:user') {
      $form_state->setError($form['selector'], $this->t('The selected data is not a contact.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $configuration = $this->plugin->getConfiguration();
    $configuration['selector'] = $form_state->getValue('selector');
    $this->plugin->setConfiguration($configuration);
  }
}
