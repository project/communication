<?php

namespace Drupal\communication;

use Drupal\communication\Contact\ContactMiddlewareBase;
use Drupal\communication\Entity\Communication;
use Drupal\communication\Entity\CommunicationParticipant;
use Drupal\communication\Exception\UnsupportedCommunicationModeException;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Class CommunicationBuilder
 *
 * @package Drupal\communication
 */
class CommunicationBuilder implements CommunicationBuilderInterface {

  /**
   * @var \Drupal\communication\CommunicationModePluginManager
   */
  protected $modeManager;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\communication\Contact\ContactMiddlewareBase
   */
  protected $contactMiddleware;

  /**
   * CommunicationBuilder constructor.
   *
   * @param \Drupal\communication\CommunicationModePluginManager $mode_manager
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\communication\Contact\ContactMiddlewareBase $contact_middleware
   */
  public function __construct(
    CommunicationModePluginManager $mode_manager,
    EntityTypeManagerInterface $entity_type_manager,
    ContactMiddlewareBase $contact_middleware
  ) {
    $this->modeManager = $mode_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->contactMiddleware = $contact_middleware;
  }

  /**
   * {@inheritdoc}
   */
  public function buildParticipant(string $type, string $role, EntityInterface $contact): CommunicationParticipant {
    $participant_storage = $this->entityTypeManager->getStorage('communication_participant');

    /** @var \Drupal\communication\Entity\CommunicationParticipant $participant */
    $participant = $participant_storage->create([
      'type' => $type,
      'role' => $role,
    ]);
    /** @var \Drupal\communication\Contact\ContactInfoInterface[] $contact_infos */
    $contact_infos = $this->contactMiddleware->collectContactInfo($contact, $participant->requiredContactInfoDefinition());
    $selected_contact_info = NULL;
    foreach ($contact_infos as $contact_info) {
      if ($contact_info->isComplete()) {
        $selected_contact_info = $contact_info;
        break;
      }
    }
    $participant->contact = [
      'entity' => $contact,
      'key' => $selected_contact_info ? $selected_contact_info->key() : NULL,
    ];
    if ($selected_contact_info) {
      $this->contactMiddleware->populateParticipant(
        $participant,
        $selected_contact_info
      );
    }
    else {
      $participant->name = $contact->label();
    }

    return $participant;
  }

  /**
   * {@inheritdoc}
   */
  public function buildCommunication(string $mode_name, $target, EntityInterface $source = NULL, Communication $original = NULL) {
    if (!$this->modeManager->hasDefinition($mode_name)) {
      throw new UnsupportedCommunicationModeException("{$mode_name} is not a supported communication mode.");
    }

    // Validate that the targets are contacts.
    if (is_array($target)) {
      foreach ($target as $t) {
        if (!$t instanceof EntityInterface || $t->getEntityTypeId() != $this->contactMiddleware->contactEntityTypeId()) {
          throw new \InvalidArgumentException("Target must be a Contact or list of Contacts.");
        }
      }
    }
    else if (!$target instanceof EntityInterface || $target->getEntityTypeId() != $this->contactMiddleware->contactEntityTypeId()) {
      throw new \InvalidArgumentException("Target must be a Contact or list of Contacts.");
    }

    /** @var \Drupal\communication\Entity\Communication $communication */
    $communication = $this->entityTypeManager->getStorage('communication')->create([
      'mode' => $mode_name,
    ]);

    /** @var \Drupal\communication\Plugin\Communication\Mode\ModeInterface $mode */
    $mode = $this->modeManager->createInstance($mode_name);
    $participants = $mode->participantRoles();
    $first_target = is_array($target) ? array_shift($target) : $target;
    $target_participant_info = $participants[$mode->defaultTargetParticipant()];
    $communication->{$target_participant_info->getFieldName()} = $this->buildParticipant(
      $target_participant_info->getType(),
      $mode->defaultTargetParticipant(),
      $first_target
    );

    // If target was supplied as an array of contacts and there are still some
    // left in the array, then add them to the communication as well.
    if (is_array($target) && !empty($target)) {
      $cardinality = $target_participant_info->getCardinality();
      if ($cardinality === FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED || $cardinality > (count($target) + 1)) {
        foreach ($target as $t) {
          $communication->{$target_participant_info->getFieldName()}[] = $this->buildParticipant(
            $target_participant_info->getType(),
            $mode->defaultTargetParticipant(),
            $t
          );
        }
      }
    }

    if ($source) {
      $source_participant_info = $participants[$mode->defaultSourceParticipant()];
      $communication->{$source_participant_info->getFieldName()} = $this->buildParticipant(
        $source_participant_info->getType(),
        $mode->defaultSourceParticipant(),
        $source
      );
    }

    if ($original) {
      $communication->previous = $original;
    }

    return $communication;
  }
}
