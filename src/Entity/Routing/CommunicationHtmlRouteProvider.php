<?php

namespace Drupal\communication\Entity\Routing;

use Drupal\communication\Controller\CommunicationController;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider;
use Symfony\Component\Routing\Route;

class CommunicationHtmlRouteProvider extends DefaultHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $routes = parent::getRoutes($entity_type);

    $record_page_route = new Route($entity_type->getLinkTemplate('record-page'));
    $record_page_route->setDefault('_controller', CommunicationController::class . '::recordPage');
    $record_page_route->setDefault('_title_callback', CommunicationController::class . '::addTitle');
    $record_page_route->setDefault('entity_type_id', $entity_type->id());
    $record_page_route->setRequirement('_entity_create_any_access', $entity_type->id());
    $routes->add('entity.communication.record_page', $record_page_route);

    $record_form_route = $this->getAddFormRoute($entity_type);
    $record_form_route->setPath($entity_type->getLinkTemplate('record-form'));
    $record_form_route->setDefault('_communication_form_mode', 'record');
    $routes->add('entity.communication.record_form', $record_form_route);

    return $routes;
  }

  /**
   * {@inheritdoc}
   */
  protected function getAddFormRoute(EntityTypeInterface $entity_type) {
    $route = parent::getAddFormRoute($entity_type);
    $defaults = $route->getDefaults();
    $defaults['_controller'] = '\Drupal\communication\Controller\CommunicationController::newCommunicationWithParticipantsForm';
    $defaults['_communication_form_mode'] = 'add';
    unset($defaults['_entity_form']);
    $route->setDefaults($defaults);
    return $route;
  }
}
