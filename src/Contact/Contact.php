<?php
/**
 * Created by PhpStorm.
 * User: Rob
 * Date: 05/04/2019
 * Time: 18:48
 */

namespace Drupal\communication\Contact;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\DataDefinitionInterface;

abstract class Contact implements ContactInterface {

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param string $key
   *
   * @return mixed
   */
  public static function create(EntityInterface $entity, $key = '') {
    return new static($entity, $key);
  }

  /**
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * @var string
   */
  protected $key = '';

  /**
   * Contact constructor.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param string $key
   */
  public function __construct(EntityInterface $entity, $key = '') {
    $this->entity = $entity;
    $this->key = $key;
  }

  /**
   * Get the entity this contact interface wraps.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getParticipantData($name, DataDefinitionInterface $definition) {
    if (!$this->key) {
      return $this->getDefaultParticipantData($name, $definition);
    }

    return $this->getKeyedParticipantData($name, $definition);
  }

  /**
   * @param $name
   * @param \Drupal\Core\TypedData\DataDefinitionInterface $definition
   */
  abstract protected function getDefaultParticipantData($name, DataDefinitionInterface $definition);

  /**
   * @param $name
   * @param \Drupal\Core\TypedData\DataDefinitionInterface $definition
   *
   * @return mixed
   */
  abstract protected function getKeyedParticipantData($name, DataDefinitionInterface $definition);

}
