<?php

namespace Drupal\communication_service\Controller;

use Drupal\communication\Entity\Communication;
use Drupal\communication\OperationPluginManager;
use Drupal\communication\Plugin\Communication\Operation\OperationWithVariantsBase;
use Drupal\communication_service\OperationRequest;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\RouteMatch;
use Drupal\rest\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\SerializerInterface;

class OperationHandler extends ControllerBase {

  /**
   * @var \Symfony\Component\Serializer\SerializerInterface
   */
  protected $serializer;

  /**
   * @var \Drupal\communication\OperationPluginManager
   */
  protected $operationPluginManager;

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('serializer'),
      $container->get('plugin.manager.communication.operation')
    );
  }

  public function __construct(SerializerInterface $serializer, OperationPluginManager $operation_plugin_manager) {
    $this->serializer = $serializer;
    $this->operationPluginManager = $operation_plugin_manager;
  }

  /**
   * Handle operations.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param \Drupal\Core\Routing\RouteMatch $route_match
   * @param \Drupal\communication\Entity\Communication|NULL $communication
   */
  public function handle(Request $request, RouteMatch $route_match) {
    $operation_id = $route_match->getParameter('operation');
    $operation_variant_id = $route_match->getParameter('operation_variant') ?: NULL;

    /** @var \Drupal\communication_service\OperationRequest $operation_request */
    $operation_request = $this->serializer->deserialize($request->getContent(), OperationRequest::class, $request->query->get('_format'));
    $communication = $operation_request->getCommunication();

    try {
      /** @var \Drupal\communication\Plugin\Communication\Operation\OperationInterface $plugin */
      $plugin = $this->operationPluginManager->createInstance($operation_id);
    }
    catch (PluginException $exception) {
      throw new NotFoundHttpException('', $exception);
    }

    $options = $operation_request->getOptions();
    if ($operation_variant_id) {
      $options += ['variant' => $operation_variant_id];
    }

    // Check whether the operation or variant is applicable.
    if (!$plugin->applicable($communication, $options)) {
      throw new NotFoundHttpException();
    }
    else if ($plugin instanceof OperationWithVariantsBase) {
      try {
        $variant = $plugin->getVariant($communication, $options);

        if (!$variant->applicable($communication, $options)) {
          throw new NotFoundHttpException('Inapplicable variant');
        }
      }
      catch (PluginException $exception) {
        throw new NotFoundHttpException('Invalid variant selected', $exception);
      }
    }

    // Validate the operation.
    if (!$plugin->validate($communication, $options, $reasons)) {
      return new ResourceResponse([
        'status' => 'invalid',
        'reasons' => $reasons,
      ], 422);
    }

    try {
      $plugin->run($communication, $options);

      return new ResourceResponse([
        'status' => $communication->status->value,
        'communication' => $communication,
      ]);
    }
    catch (\Exception $exception) {
      return new ResourceResponse([
        'error' => $exception->getMessage()
      ], 500);
    }
  }
}
