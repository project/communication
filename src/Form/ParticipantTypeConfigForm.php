<?php

namespace Drupal\communication\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ParticipantTypeConfigForm allow participant types to have settings. We
 * will also allow configuration of ContactMiddleware behaviours here.
 *
 * @package Drupal\communication\Form
 */
class ParticipantTypeConfigForm extends ConfigFormBase {

  /**
   * @var string
   */
  protected $participantTypePluginId = '';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    if (!empty($this->participantTypePluginId)) {
      return ['communication.participant_type.'.$this->participantTypePluginId];
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mode_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $bundle = '') {
    $this->participantTypePluginId = $bundle;

    return parent::buildForm($form, $form_state);
  }
}
