<?php

namespace Drupal\communication;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

class CommunicationModePluginManager extends DefaultPluginManager {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a CommunicationModePluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, ConfigFactoryInterface $config_factory) {
    parent::__construct(
      'Plugin/Communication/Mode',
      $namespaces,
      $module_handler, 'Drupal\communication\Plugin\Communication\Mode\ModeInterface',
      'Drupal\communication\Annotation\CommunicationMode'
    );
    $this->alterInfo('communication_mode');
    $this->setCacheBackend($cache_backend, 'communication_mode');

    $this->configFactory = $config_factory;
  }

  /**
   * Create an instance of a mode plugin.
   *
   * @param $plugin_id
   * @param array $configuration
   *
   * @return \Drupal\communication\Plugin\Communication\Mode\ModeInterface
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function createInstance($plugin_id, array $configuration = []) {
    if (empty($configuration)) {
      $configuration = $this->configFactory
        ->get('communication.mode.'.$plugin_id)
        ->get();
    }

    return parent::createInstance($plugin_id, $configuration);
  }
}
