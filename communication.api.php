<?php

use Drupal\communication\Entity\CommunicationInterface;

/**
 * Act before an operation.
 *
 * To alter the options, use an event instead.
 *
 * @param \Drupal\communication\Entity\CommunicationInterface $communication
 * @param array $options
 * @param $operation
 */
function hook_pre_communication_operation(CommunicationInterface $communication, array $options, $operation) {

}

/**
 * Act before a specific operation.
 *
 * @param \Drupal\communication\Entity\CommunicationInterface $communication
 * @param array $options
 */
function hook_pre_communication_OPERATION_ID(CommunicationInterface $communication, array $options) {

}
