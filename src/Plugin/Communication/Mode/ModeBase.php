<?php

namespace Drupal\communication\Plugin\Communication\Mode;

use Drupal\communication\Entity\Communication;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Plugin\PluginWithFormsInterface;
use Drupal\Core\Plugin\PluginWithFormsTrait;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Utility\Token;
use Drupal\entity\BundleFieldDefinition;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ModeBase
 *
 * @package Drupal\communication\Plugin\Communication\Mode
 */
abstract class ModeBase extends PluginBase implements ModeInterface, ContainerFactoryPluginInterface, PluginWithFormsInterface {
  use PluginWithFormsTrait;

  /**
   * @var \Drupal\token\TokenInterface
   */
  protected $token;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('token'));
  }

  /**
   * CommunicationModeBase constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\Core\Utility\Token $token
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Token $token) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->token = $token;
  }

  /**
   * {@inheritdoc}
   */
  public static function statusOptions(FieldDefinitionInterface $definition, EntityInterface $entity = NULL, &$cacheable = TRUE) {
    return [
      'draft' => new TranslatableMarkup('Draft'),
      'ready' => new TranslatableMarkup('Ready'),
      'sent' => new TranslatableMarkup('Sent'),
    ];
  }

  /**
   * Get communication fields required for this mode.
   *
   * @param \Drupal\Core\Field\BaseFieldDefinition[] $base_field_definitions
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   */
  public function communicationFieldDefinitions(array $base_field_definitions) {
    $fields = [];

    if ($this->hasAttachments()) {
      $fields['attachments'] = BundleFieldDefinition::create('file')
        ->setSettings([
          'uri_scheme' => 'private',
          'file_directory' => 'attachments',
          'description_field' => 1,
        ])
        ->setProvider('communication')
        ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
        ->setLabel(new TranslatableMarkup('Attachments'))
        ->setDisplayOptions('view', [
          'type' => 'file_default',
          'label' => 'above',
        ])
        ->setDisplayConfigurable('view', TRUE)
        ->setDisplayOptions('form', [
          'type' => 'file_generic',
        ])
        ->setDisplayConfigurable('form', TRUE);
    }

    return $fields;
  }

  /**
   * Get the participant roles associated with this communication mode.
   *
   * @return array
   */
  public function participantRoles() {
    return [];
  }

  /**
   * Replace tokens in content.
   *
   * @param $content
   * @param \Drupal\communication\Entity\Communication $communication
   * @param \Drupal\Core\Render\BubbleableMetadata|NULL $bubbleable_metadata
   *
   * @return string
   */
  protected function replaceTokens($content, Communication $communication, BubbleableMetadata $bubbleable_metadata = NULL) {
    return $this->token->replace($content, [
      'communication' => $communication,
    ], [], $bubbleable_metadata);
  }

  /**
   * Return whether or not this plugin has attachments.
   */
  protected function hasAttachments() {
    return TRUE;
  }

  /**
   * Get a list of events supported by this mode.
   *
   * @return array
   */
  public function supportedEvents() {
    return [
      'operation_failed' => [
        'label' => new TranslatableMarkup('Operation Failed'),
      ],
      'sent' => [
        'label' => new TranslatableMarkup('Sent'),
      ],
      'received' => [
        'label' => new TranslatableMarkup('Received'),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'enabled_op_variants' => [],
    ];
  }
}
