<?php

namespace Drupal\communication\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a ContactInfoSource plugin annotation object.
 *
 * Plugin Namespace: Plugin\ContactInfoSource.
 *
 * @see \Drupal\communication\Plugin\ContactInfoSource\ContactInfoSourceInterface
 * @see \Drupal\communication\Plugin\ContactInfoSource\ContactInfoSourceBase
 *
 * @ingroup plugin_api
 *
 * @Annotation
 */
class ContactInfoSource extends Plugin {

  /**
   * The id of this mode.
   *
   * @var string
   */
  public $id;

  /**
   * The label of this mode.
   *
   * @var string
   */
  public $label;

  /**
   * The entity type id this contact info source works with.
   *
   * @var string
   */
  public $entity_type_id;
}
