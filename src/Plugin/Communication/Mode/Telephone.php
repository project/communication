<?php

namespace Drupal\communication\Plugin\Communication\Mode;

use Drupal\communication\ParticipantRole;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\entity\BundleFieldDefinition;

/**
 * Class Email
 *
 * @CommunicationMode(
 *   id = "telephone",
 *   label = "Telephone",
 *   forms = {
 *     "configure" = "Drupal\communication\PluginForm\ModeConfigureForm"
 *   }
 * )
 *
 * @package Drupal\communication\Plugin\Communication\Mode
 */
class Telephone extends ModeBase {

  /**
   * @param \Drupal\Core\Field\FieldDefinitionInterface $definition
   * @param \Drupal\Core\Entity\EntityInterface|NULL $entity
   * @param bool $cacheable
   *
   * @return array
   */
  public static function statusOptions(FieldDefinitionInterface $definition, EntityInterface $entity = NULL, &$cacheable = TRUE) {
    $options = parent::statusOptions($definition, $entity, $cacheable);
    unset($options['sent']);
    $options['in_progress'] = new TranslatableMarkup('In Progress');
    $options['complete'] = new TranslatableMarkup('Complete');

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function communicationFieldDefinitions(array $base_field_definitions) {
    $fields = parent::communicationFieldDefinitions($base_field_definitions);

    $fields['call_plan'] = BundleFieldDefinition::create('text_long')
      ->setLabel(new TranslatableMarkup('Plan'))
      ->setDescription(new TranslatableMarkup('What is the plan or rough agenda for this call.'))
      ->setCardinality(1)
      ->setRevisionable(TRUE)
      ->setProvider('communication')
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['call_notes'] = BundleFieldDefinition::create('text_long')
      ->setLabel(new TranslatableMarkup('Notes'))
      ->setDescription(new TranslatableMarkup('Any notes from the call'))
      ->setCardinality(1)
      ->setRevisionable(TRUE)
      ->setProvider('communication')
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
      ])
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function participantRoles() {
    return [
      'caller' => ParticipantRole::create('caller', 'phone', new TranslatableMarkup('Caller')),
      'recipient' => ParticipantRole::create('recipient', 'phone', new TranslatableMarkup('Recipient')),
      'other' => ParticipantRole::create('other', 'phone', new TranslatableMarkup('Other'), BaseFieldDefinition::CARDINALITY_UNLIMITED),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function hasAttachments() {
    return FALSE;
  }

  /**
   * Get the default target participant.
   *
   * @return string
   */
  public function defaultTargetParticipant() {
    return 'recipient';
  }

  /**
   * Get the default source participant.
   *
   * @return string
   */
  public function defaultSourceParticipant() {
    return 'caller';
  }
}
