<?php

namespace Drupal\communication_subscription\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Class NotificationMethod
 *
 * @Annotation
 *
 * @package Drupal\communication_subscription\Annotation
 */
class NotificationMethod extends Plugin {
}
