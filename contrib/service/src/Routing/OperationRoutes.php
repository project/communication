<?php
/**
 * Created by PhpStorm.
 * User: Rob
 * Date: 06/02/2020
 * Time: 14:33
 */

namespace Drupal\communication_service\Routing;

use Drupal\communication\OperationPluginManager;
use Drupal\communication\OperationVariantPluginManager;
use Drupal\communication\Plugin\Communication\Operation\OperationWithVariantsBase;
use Drupal\Core\Routing\RouteBuildEvent;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\Route;

/**
 * Class OperationRoutes
 *
 * @package Drupal\communication_service\Routing
 */
class OperationRoutes implements EventSubscriberInterface {

  /**
   * @var \Drupal\communication\OperationPluginManager
   */
  protected $operationManager;

  /**
   * @var \Drupal\communication\OperationVariantPluginManager
   */
  protected $operationVariantManager;

  public function __construct(OperationPluginManager $operation_manager, OperationVariantPluginManager $operation_variant_manager) {
    $this->operationManager = $operation_manager;
    $this->operationVariantManager = $operation_variant_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[RoutingEvents::DYNAMIC] = 'onDynamicRouteEvent';
    return $events;
  }

  /**
   * Alters existing routes for a specific collection.
   *
   * @param \Drupal\Core\Routing\RouteBuildEvent $event
   *   The route build event.
   */
  public function onDynamicRouteEvent(RouteBuildEvent $event) {
    $collection = $event->getRouteCollection();

    foreach ($this->operationManager->getDefinitions() as $operation => $definition) {
      $route_name = 'communication.operation.'.$operation;

      $route = new Route(
        '/api/communication/operation/'.$operation,
        [
          '_controller' => 'Drupal\communication_service\Controller\OperationHandler::handle',
          'operation' => $operation,
        ],
        [
          '_communication_operation_access' => $operation,
        ],
        [
          '_auth' => ['basic_auth'],
          '_csrf_request_header_token' => 'TRUE',
        ],
        '',
        [],
        ['POST']
      );
      $collection->add($route_name, $route);

      if (is_subclass_of($definition['class'], OperationWithVariantsBase::class)) {
        foreach ($this->operationVariantManager->getDefinitionsForOperation($operation) as $variant => $v_definition) {
          $variant_route_name = $route_name.'.'.$variant;
          $variant_route = new Route(
            '/api/communication/operation/'.$operation.'/'.$variant,
            [
              '_controller' => 'Drupal\communication_service\Controller\OperationHandler::handle',
              'operation' => $operation,
              'operation_variant' => $variant,
            ],
            [
              '_communication_operation_access' => $operation,
            ],
            [
              '_auth' => ['basic_auth'],
              '_csrf_request_header_token' => 'TRUE',
            ],
            '',
            [],
            ['POST']
          );
          $collection->add($variant_route_name, $variant_route);
        }
      }
    }
  }
}
