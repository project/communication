<?php

namespace Drupal\communication\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a CommunicationOperation plugin annotation object.
 *
 * Plugin Namespace: Plugin\Communication\Operation.
 *
 * @see \Drupal\communication\Plugin\Communication\Operation\OperationInterface
 * @see \Drupal\communication\Plugin\Communication\Operation\OperationBase
 *
 * @ingroup plugin_api
 *
 * @Annotation
 */
class CommunicationOperation extends Plugin {

  /**
   * The id of this mode.
   *
   * @var string
   */
  public $id;

  /**
   * The label of this mode.
   *
   * @var string
   */
  public $label;

  /**
   * The modes this operation applies to.
   *
   * @var array
   */
  public $modes = [];

  /**
   * The modes this operation cannot apply to.
   *
   * @var array
   */
  public $exclude_modes = [];

  /**
   * Whether or not this operation has variants.
   *
   * @var boolean
   */
  public $has_variants = TRUE;
}
