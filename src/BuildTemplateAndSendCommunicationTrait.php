<?php

namespace Drupal\communication;

use Drupal\communication\Entity\Communication;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\entity_template\Plugin\EntityTemplate\Builder\BuilderInterface;
use Drupal\entity_template\TemplateBuilderManager;

trait BuildTemplateAndSendCommunicationTrait {
  use LoggerChannelTrait;

  /**
   * The operation manager.
   *
   * @var \Drupal\communication\OperationPluginManager
   */
  protected $operationManager;

  /**
   * The template builder manager.
   *
   * @var \Drupal\entity_template\TemplateBuilderManager
   */
  protected $builderManager;

  /**
   * Get the operation manager service.
   *
   * @return \Drupal\communication\OperationPluginManager
   *   The communication operation manager.
   */
  protected function operationManager() : OperationPluginManager {
    if (empty($this->operationManager)) {
      $this->operationManager = \Drupal::service('plugin.manager.communication.operation');
    }

    return $this->operationManager;
  }

  /**
   * Set the operation manager.
   *
   * @param \Drupal\communication\OperationPluginManager $operation_manager
   *   The operation manager.
   */
  public function setOperationManager(OperationPluginManager $operation_manager) {
    $this->operationManager = $operation_manager;
  }

  /**
   * Get the builder manager.
   *
   * @return \Drupal\entity_template\TemplateBuilderManager
   *   The template builder manager.
   */
  protected function builderManager() : TemplateBuilderManager {
    if (empty($this->builderManager)) {
      $this->builderManager = \Drupal::service('plugin.manager.entity_template.builder');
    }

    return $this->builderManager;
  }

  /**
   * Set the template builder manager.
   *
   * @param \Drupal\entity_template\TemplateBuilderManager $builder_manager
   *   The builder manager service.
   */
  public function setBuilderManager(TemplateBuilderManager $builder_manager) {
    $this->builderManager = $builder_manager;
  }

  /**
   * Get the entity template builder to use.
   *
   * @param string|null $builder_name
   *   The name of the builder to use. If empty a default will be used.
   *
   * @return \Drupal\entity_template\Plugin\EntityTemplate\Builder\BuilderInterface|null
   *   An entity template builder or NULL if no builder is requested.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   Throws an exception when the builder is not available.
   */
  protected function getBuilder($builder_name = NULL) : ?BuilderInterface {
    $builder_name = $builder_name ?: $this->defaultBuilderName();

    return $builder_name ? $this->builderManager()->createInstance($builder_name) : NULL;
  }

  /**
   * Get the default builder name.
   *
   * @return string|null
   *   The default builder name.
   */
  protected function defaultBuilderName() : ?string {
    return NULL;
  }

  /**
   * Build and sent the communication.
   *
   * @param array $params
   *   An array of parameters to pass to the builder if accepted.
   * @param string|null $builder_name
   *   The name of the builder to execute.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\entity_template\Exception\MultipleAvailableBlueprintException
   */
  protected function buildAndSendCommunication(array $params = [], string $builder_name = NULL) {
    $builder = $this->getBuilder($builder_name);
    if (!$builder) {
      return;
    }

    $real_params = [];
    foreach ($builder->getContextDefinitions() as $name => $definition) {
      if (isset($params[$name])) {
        $real_params[$name] = $params[$name];
      }
    }

    $result = $builder->execute($real_params);
    foreach ($result->getItems() as $item) {
      // If the builder has build something that isn't a communication, just
      // save it an move on.
      if (!$item instanceof Communication) {
        $item->save();
        continue;
      }

      // Attempt to send the communication.
      $available_operations = $this->operationManager()->availableOperations($item);
      if (!isset($available_operations['send'])) {
        $item->save();
        continue;
      }

      $op = $available_operations['send'];
      if ($op->validate($item)) {
        try {
          $op->run($item);
        }
        catch (PluginException $exception) {
          $item->save();
          $this->getLogger('contacts_communication')->warning('Failed to send Communication ' . $item->id() . ': ' . $exception->getMessage());
        }
      }
      else {
        $item->save();
        $this->getLogger('contacts_communication')->warning('Failed to validate Communication ' . $item->id());
      }
    }
  }

}
