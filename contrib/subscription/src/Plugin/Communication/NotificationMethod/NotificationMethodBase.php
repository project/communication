<?php

namespace Drupal\communication_subscription\Plugin\Communication\NotificationMethod;

use Drupal\Core\Plugin\PluginBase;

/**
 * Class NotificationMethodBase
 *
 * @package Drupal\communication_subscription\Plugin\Communication\NotificationMethod
 */
abstract class NotificationMethodBase extends PluginBase implements NotificationMethodInterface {

  /**
   * Builds the field definitions for entities of this bundle.
   *
   * Important:
   * Field names must be unique across all bundles.
   * It is recommended to prefix them with the bundle name (plugin ID).
   *
   * @return \Drupal\entity\BundleFieldDefinition[]
   *   An array of bundle field definitions, keyed by field name.
   */
  public function buildFieldDefinitions() {
    return [];
  }
}
