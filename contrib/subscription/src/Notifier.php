<?php

namespace Drupal\communication_subscription;

use Drupal\communication\Entity\CommunicationEvent;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Class Notifier
 *
 * @package Drupal\communication_subscription
 */
class Notifier implements NotifierInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Notifier constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LoggerChannelFactoryInterface $logger_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger_factory->get('communication_subscription');
  }

  /**
   * {@inheritdoc}
   */
  public function notify(CommunicationEvent $event) {
    $subscriptions = $this->getSubscriptions($event);
    foreach ($subscriptions as $subscription) {
      try {
        $subscription->getNotificationMethod()->notify($event, $subscription);
      }
      catch (\Exception $exception) {
        // @todo: Consider queueing for later.
        $this->logger->error($exception->getMessage());
      }
    }
  }

  /**
   * Get the subscriptions handling a particular event.
   *
   * @param \Drupal\communication\Entity\CommunicationEvent $event
   *
   * @return \Drupal\communication_subscription\Entity\Subscription[]
   */
  protected function getSubscriptions(CommunicationEvent $event) {
    $storage = $this->entityTypeManager->getStorage('communication_subscription');

    $query = $storage->getQuery();
    $query->condition(
      $query->orConditionGroup()
        ->condition('events', $event->bundle())
        ->notExists('events')
    );
    $query->condition(
      $query->orConditionGroup()
        ->condition('communication', $event->communication->target_id)
        ->notExists('communication')
    );

    $subscriptions = [];
    if ($ids = $query->execute()) {
      /** @var \Drupal\communication_subscription\Entity\Subscription $subscription */
      foreach ($storage->loadMultiple($ids) as $id => $subscription) {
        if ($subscription->communication->target_id == $event->communication->target_id) {
          $subscriptions[] = $subscription;
        }
        else if ($event->getCommunication()->access('view', $subscription->getOwner())){
          $subscriptions[] = $subscription;
        }
      }
    }

    return $subscriptions;
  }
}
