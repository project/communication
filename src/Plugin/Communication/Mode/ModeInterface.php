<?php

namespace Drupal\communication\Plugin\Communication\Mode;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

interface ModeInterface extends ConfigurableInterface {

  /**
   * Get the status options associated with this mode
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $definition
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param bool $cacheable
   *
   * @return array
   */
  public static function statusOptions(FieldDefinitionInterface $definition, EntityInterface $entity = NULL, &$cacheable = TRUE);

  /**
   * Get a list of events supported by this mode.
   *
   * @return array
   */
  public function supportedEvents();

  /**
   * Get communication fields required for this mode.
   *
   * @param \Drupal\Core\Field\BaseFieldDefinition[] $base_field_definitions
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   */
  public function communicationFieldDefinitions(array $base_field_definitions);

  /**
   * Get the participant roles associated with this communication mode.
   *
   * @return \Drupal\communication\ParticipantRole[]
   */
  public function participantRoles();

  /**
   * Get the default target participant.
   *
   * @return string
   */
  public function defaultTargetParticipant();

  /**
   * Get the default source participant.
   *
   * @return string
   */
  public function defaultSourceParticipant();

}

