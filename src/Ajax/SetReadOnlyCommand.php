<?php

namespace Drupal\communication\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Class SetFormValueCommand
 *
 * @package Drupal\communication\Ajax
 */
class SetReadOnlyCommand implements CommandInterface {

  protected $form_selector = NULL;

  protected $form_build_id = NULL;

  protected $name;

  protected $readonly;

  /**
   * SetReadOnlyCommand constructor.
   *
   * @param $name
   * @param bool $readonly
   * @param null $form_selector
   */
  public function __construct($name, $readonly = TRUE, $form_selector = NULL, $form_build_id = NULL) {
    $this->form_selector = $form_selector;
    $this->form_build_id = $form_build_id;
    $this->name = $name;
    $this->readonly = $readonly;
  }

  /**
   * Return an array to be run through json_encode and sent to the client.
   */
  public function render() {
    return [
      'command' => 'set_read_only',
      'form_selector' => $this->form_selector,
      'form_build_id' => $this->form_build_id,
      'element_name' => $this->name,
      'readonly' => $this->readonly,
    ];
  }
}
