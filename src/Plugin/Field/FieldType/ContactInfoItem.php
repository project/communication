<?php

namespace Drupal\communication\Plugin\Field\FieldType;

use Drupal\communication\Contact\ContactInfo;
use Drupal\communication\Contact\ContactInfoInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Class ContactInfoItem
 *
 * This FieldType must ALWAYS be computed
 *
 * @FieldType(
 *   id = "contact_info",
 *   label = @Translation("Contact Info"),
 *   description = @Translation("Contact info"),
 *   category = @Translation("Communication"),
 *   list_class = "\Drupal\communication\Plugin\Field\ContactInfoFieldItemList"
 * )
 *
 * @package Drupal\communication\Plugin\Field\FieldType
 */
class ContactInfoItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['key'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Key'))
      ->setRequired(TRUE);
    $properties['label'] = DataDefinition::create('contact_info_label')
      ->setLabel(new TranslatableMarkup('Label'))
      ->setComputed(TRUE);
    $properties['summary'] = DataDefinition::create('contact_info_summary')
      ->setLabel(new TranslatableMarkup('Summary'))
      ->setComputed(TRUE);
    $properties['info'] = DataDefinition::create('contact_info')
      ->setLabel(new TranslatableMarkup('Contact Info'))
      ->setComputed(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'info';
  }

  /**
   * [@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    if ($values instanceof ContactInfoInterface) {
      $this->set('info', $values);
      $this->set('key', $values->key());
    }
    else if (is_string($values)) {
      $this->set('key', $values);
    }
    else if (is_array($values)) {
      if (!empty($values['key'])) {
        $this->set('key', $values['key']);
      }
      if (!empty($values['info'])) {
        $this->set('info', $values['info']);
        if (empty($values['key'])) {
          $this->set('key', $values['info']->key());
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function onChange($property_name, $notify = TRUE) {
    if ($property_name == 'key') {
      if ($this->get('key')->getValue()) {
        if (!$this->get('info')->getValue() || ($this->get('info')->getValue()->key() != $this->get('key')->getValue())) {
          $this->writePropertyValue('info', ContactInfo::create($this->getEntity(), $this->get('key')->getValue()));
        }
      }
      else {
        $this->writePropertyValue('info', NULL);
      }
    }
    parent::onChange($property_name, $notify);
  }
}
