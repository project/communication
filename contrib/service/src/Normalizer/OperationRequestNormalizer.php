<?php

namespace Drupal\communication_service\Normalizer;

use Drupal\communication_service\OperationRequest;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\serialization\Normalizer\NormalizerBase;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class OperationRequestNormalizer extends NormalizerBase implements DenormalizerInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * [@inheritdoc}
   */
  protected $supportedInterfaceOrClass = OperationRequest::class;

  /**
   * OperationRequestNormalizer constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = []) {
    /** @var \Drupal\communication_service\OperationRequest $object */
    $value = [];

    $communications = $object->getCommunications();
    if (count($communications) === 1) {
      $value['communication'] = [
        'id' => $object->getCommunication()->id(),
        'uuid' => $object->getCommunication()->uuid(),
        'url' => $object->getCommunication()->toUrl()->setAbsolute()->toString(),
      ];
    }
    else {
      $value['communications'] = [];
      foreach ($object->getCommunications() as $index => $communication) {
        $value['communications'][$index] = [
          'id' => $object->getCommunication($index)->id(),
          'uuid' => $object->getCommunication($index)->uuid(),
          'url' => $object->getCommunication($index)->toUrl()->setAbsolute()->toString(),
        ];
      }
    }

    $value['options'] = $object->getOptions();

    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function denormalize($data, $type, $format = NULL, array $context = []) {
    $options = isset($data['options']) ? $data['options'] : [];

    $comm_storage = $this->entityTypeManager->getStorage('communication');
    if (isset($data['communication'])) {
      if (is_array($data['communication'])) {
        $communications = $comm_storage->load($data['communication']['id']);
      }
      else {
        $communications = $comm_storage->load($data['communication']);
      }
    }
    else if (isset($data['communications'])) {
      $communications = [];
      foreach ($data['communications'] as $index => $data_comm) {
        if (is_array($data_comm)) {
          $communications[$index] = $comm_storage->load($data_comm['id']);
        }
        else {
          $communications[$index] = $comm_storage->load($data_comm);
        }
      }
    }
    else {
      $communications = [];
    }

    return new OperationRequest($communications, $options);
  }
}
