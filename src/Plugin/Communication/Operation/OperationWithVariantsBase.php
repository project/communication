<?php

namespace Drupal\communication\Plugin\Communication\Operation;

use Drupal\communication\Entity\CommunicationInterface;
use Drupal\communication\OperationVariantPluginManager;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class OperationWithVariantsBase extends OperationBase  {

  /**
   * @var \Drupal\communication\OperationVariantPluginManager
   */
  protected $variantManager;

  /**
   * @var \Drupal\communication\Plugin\Communication\OperationVariant\OperationVariantInterface[]
   */
  protected $variants;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('event_dispatcher'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.communication.operation_variant')
    );
  }

  /**
   * OperationWithVariantsBase constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   * @param \Drupal\communication\OperationVariantPluginManager $variant_manager
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ModuleHandlerInterface $module_handler,
    EventDispatcherInterface $event_dispatcher,
    EntityTypeManagerInterface $entity_type_manager,
    OperationVariantPluginManager $variant_manager
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $module_handler,
      $event_dispatcher,
      $entity_type_manager
    );

    $this->variantManager = $variant_manager;
  }

  /**
   * Get an array of applicable variants keyed by plugin id.
   *
   * @param \Drupal\communication\Entity\CommunicationInterface $communication
   * @param array $options
   *
   * @return \Drupal\communication\Plugin\Communication\OperationVariant\OperationVariantInterface[]
   */
  protected function applicableVariants(CommunicationInterface $communication, array $options = [], $reset = FALSE) {
    if (!$reset && !is_null($this->variants)) {
      return $this->variants;
    }

    $mode_config = $communication->getMode()->getConfiguration();
    $enabled_variants = !empty($mode_config['enabled_op_variants'][$this->getPluginId()]) ?
      $mode_config['enabled_op_variants'][$this->getPluginId()] :
      [];

    $this->variants = [];
    foreach ($this->variantManager->getDefinitionsForOperation($this->pluginId) as $variant_id => $variant_definition) {
      if (!in_array($variant_id, $enabled_variants)) {
        continue;
      }

      try {
        /** @var \Drupal\communication\Plugin\Communication\OperationVariant\OperationVariantInterface $variant */
        $variant = $this->variantManager->createInstance($variant_id);
        if ($variant->applicable($communication, $options)) {
          $this->variants[$variant_id] = $variant;
        }
      }
      catch (PluginException $exception) {
        continue;
      }
    }
    return $this->variants;
  }

  /**
   * Get the variant for this operation.
   *
   * @param \Drupal\communication\Entity\CommunicationInterface $communication
   * @param array $options
   *
   * @return \Drupal\communication\Plugin\Communication\OperationVariant\OperationVariantInterface
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getVariant(CommunicationInterface $communication, array $options = []) {
    $applicable_variants = $this->applicableVariants($communication, $options);
    if (empty($options['variant'])) {
      if (count($applicable_variants) == 1) {
        $options['variant'] = key($applicable_variants);
      }
      else {
        // @todo: Some how select a default.
        throw new PluginException(new TranslatableMarkup('No variant selected.'));
      }
    }

    if (!isset($applicable_variants[$options['variant']])) {
      throw new PluginException(new TranslatableMarkup('Unapplicable variant selected.'));
    }

    return $applicable_variants[$options['variant']];
  }

  /**
   * {@inheritdoc}
   */
  public function validate(CommunicationInterface $communication, array $options = [], array &$reasons = []) {
    $variant = $this->getVariant($communication, $options);
    unset($options['variant']);
    return $variant->validate($communication, $options, $reasons);
  }

  /**
   * {@inheritdoc}
   */
  public function applicable(CommunicationInterface $communication, array $options = []) {
    if (!parent::applicable($communication, $options)) {
      return FALSE;
    }

    return parent::applicable($communication, $options) && $this->applicableVariants($communication, $options);
  }

  /**
   * @param \Drupal\communication\Entity\CommunicationInterface $communication
   * @param array $options
   *
   * @return \Drupal\communication\Entity\CommunicationInterface
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function run(CommunicationInterface $communication, array $options = []) {
    $variant = $this->getVariant($communication, $options);

    $this->preRun($communication, $options);
    unset($options['variant']);
    return $variant->run($communication, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state, CommunicationInterface $communication) {
    $options = $form_state->get('operation_options') ?: [];
    $variant_id = !empty($options['variant']) ? $options['variant'] : FALSE;
    $variants = $this->applicableVariants($communication, $options);

    if (!$variant_id) {
      if (count($variants) == 1) {
        $variant_id = key($variants);
      }
    }

    if (!$variant_id) {
      $form['variant'] = [
        '#type' => 'select',
        '#title' => new TranslatableMarkup('Variant'),
        //@todo: More grokkable name.
        '#options' => [],
        '#ajax' => [
          'callback' => [$this, 'formAjaxRefreshCallback'],
          'wrapper' => 'communication-operations-form',
          'trigger_as' => ['name' => 'variant_select_submit'],
        ],
      ];
      foreach ($variants as $name => $variant) {
        $form['variant']['#options'][$name] = $variant->getPluginDefinition()['label'];
      }

      $form['variant_select_submit'] = [
        '#type' => 'submit',
        '#name' => 'variant_select_submit',
        '#value' => $this->t('Select Variant'),
        '#limit_validation_errors' => [['variant']],
        '#validate' => [],
        '#submit' => [
          [$this, 'formVariantSelectSubmit'],
        ],
        '#ajax' => [
          'callback' => [$this, 'formAjaxRefreshCallback'],
          'wrapper' => 'communication-operations-form',
        ],
        '#attributes' => ['class' => ['js-hide']],
      ];

      return $form;
    }

    $variant = $variants[$variant_id];
    return $variant->form($form, $form_state, $communication);
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function formVariantSelectSubmit(array $form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $parents = $triggering_element['#parents'];
    array_pop($parents); array_push($parents, 'variant');

    $options = $form_state->get('operation_options') ?: [];
    $options['variant'] = $form_state->getValue($parents);
    $form_state->set('operation_options', $options);

    $form_state->setRebuild(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function formValidate(array $form, FormStateInterface $form_state, CommunicationInterface $communication) {
    $options = $form_state->get('operation_options') ?: [];
    $variant_id = !empty($options['variant']) ? $options['variant'] : NULL;
    $variants = $this->applicableVariants($communication, $options);
    $variants[$variant_id]->formValidate($form, $form_state, $communication);
  }

  /**
   * {@inheritdoc}
   */
  public function formSubmit(array $form, FormStateInterface $form_state, CommunicationInterface $communication) {
    $options = $form_state->get('operation_options') ?: [];
    $variant_id = !empty($options['variant']) ? $options['variant'] : NULL;
    $variants = $this->applicableVariants($communication, $options);
    $variants[$variant_id]->formSubmit($form, $form_state, $communication);
  }

  /**
   * @param \Drupal\communication\Entity\CommunicationInterface $communication
   * @param array $options
   *
   * @return bool
   */
  public function hasForm(CommunicationInterface $communication, array $options = []) {
    if (!empty($options['variant'])) {
      $variants = $this->applicableVariants($communication, $options);
      return empty($variants[$options['variant']]) || $variants[$options['variant']]->hasForm($communication, $options);
    }
    else {
      $variants = $this->applicableVariants($communication, $options);
      if (count($variants) == 1) {
        return reset($variants)->hasForm($communication, $options);
      }
    }

    return TRUE;
  }
}
