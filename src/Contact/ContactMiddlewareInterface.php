<?php

namespace Drupal\communication\Contact;

use Drupal\communication\Entity\CommunicationParticipant;
use Drupal\Core\Entity\EntityInterface;

/**
 * Interface ContactMiddlewareInterface
 *
 * This interface is designed to allow developers to sync communication
 * participants up to contacts stored somewhere else in drupal. Hopefully this
 * will allow interaction with a number of different CRM implementations without
 * having to rewrite vast swathes of communication code each time.
 *
 * @package Drupal\communication\Contact
 */
interface ContactMiddlewareInterface {

  /**
   * Get the contact entity type id.
   *
   * @return string
   */
  public function contactEntityTypeId();

  /**
   * @param array $values
   *
   * @return mixed
   */
  public function createContact(array $values = []);

  /**
   * Populate a participant with information.
   *
   * @param \Drupal\communication\Entity\CommunicationParticipant $participant
   * @param \Drupal\communication\Contact\ContactInfoInterface $contact
   *
   * @return bool
   *   True if the participant could be populated
   *   False otherwise
   */
  public function populateParticipant(CommunicationParticipant $participant, ContactInfoInterface $contact);

  /**
   * Find a contact for a given participant.
   *
   * @param \Drupal\communication\Entity\CommunicationParticipant $participant
   *
   * @return ContactInterface
   */
  public function findContactForParticipant(CommunicationParticipant $participant);

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param \Drupal\communication\Contact\ContactInfoDefinitionInterface $definition
   * @param array $options
   *
   * @return \Drupal\communication\Contact\ContactInfoInterface[]
   */
  public function collectContactInfo(EntityInterface $entity, ContactInfoDefinitionInterface $definition, array $options = []);

  /**
   * @param $id
   *
   * @return \Drupal\Core\Entity\EntityInterface
   */
  public function loadContact($id);
}
