<?php

namespace Drupal\communication\Plugin\Field;

use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeFieldItemList;

class CommunicationEventDateComputedFieldItemList extends DateTimeFieldItemList {
  use ComputedItemListTrait;

  /**
   * Computes the values for an item list.
   */
  protected function computeValue() {
    $communication = $this->getEntity();
    $event_type = $this->getFieldDefinition()->getSetting('communication_event_type');

    if ($communication->isNew()) {
      return;
    }

    $event_storage = \Drupal::entityTypeManager()->getStorage('communication_event');
    $query = $event_storage->getQuery();
    $query->condition('communication', $communication->id());
    $query->condition('type', $event_type);
    $query->sort('date', 'DESC');
    $query->accessCheck(FALSE);

    if ($ids = $query->execute()) {
      /** @var \Drupal\communication\Entity\CommunicationEvent $event */
      $event = $event_storage->load(reset($ids));
      if (!$event->get('date')->isEmpty()) {
        $this->list[0] = $this->createItem(0, $event->get('date')->get(0)->getValue());
      }
    }
  }

  /**
   * Store the event.
   *
   * @param $update
   *
   * @return bool
   */
  public function postSave($update) {
    if (!$this->isEmpty()) {
      $communication = $this->getEntity();
      $event_type = $this->getFieldDefinition()->getSetting('communication_event_type');

      $event_storage = \Drupal::entityTypeManager()->getStorage('communication_event');
      $query = $event_storage->getQuery();
      $query->condition('communication', $communication->id());
      $query->condition('type', $event_type);
      $query->sort('date', 'DESC');

      if ($ids = $query->execute()) {
        /** @var \Drupal\communication\Entity\CommunicationEvent $event */
        $event = $event_storage->load(reset($ids));
        $event->date = $this->getValue();
      }
      else {
        $event = $event_storage->create([
          'type' => $event_type,
          'communication' => $communication->id(),
          'date' => $this->getValue(),
        ]);
      }
      $event->save();
    }

    return parent::postSave($update);
  }
}
