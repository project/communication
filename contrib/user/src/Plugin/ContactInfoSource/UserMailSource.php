<?php

namespace Drupal\communication_user\Plugin\ContactInfoSource;

use Drupal\communication\Contact\ContactInfo;
use Drupal\communication\Contact\ContactInfoDefinition;
use Drupal\communication\Contact\ContactInfoDefinitionInterface;
use Drupal\communication\Plugin\ContactInfoSource\ContactInfoSourceBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinitionInterface;

/**
 * Class UserEmailSource
 *
 * @ContactInfoSource(
 *   id = "user_mail",
 *   label = @Translation("User Mail"),
 *   entity_type_id = "user",
 * )
 *
 * @package Drupal\communication_user\Plugin\ContactInfoSource
 */
class UserMailSource extends ContactInfoSourceBase {

  /**
   * {@inheritdoc}
   */
  public function collectInfo(EntityInterface $entity, ContactInfoDefinitionInterface $definition, array $options = []) {
    if ($definition->getDataType() == 'email') {
      /** @var \Drupal\user\UserInterface $entity */
      $contact_info = (new ContactInfo($definition, $entity, $this->getPluginId()));
      if (!$entity->getEmail()) {
        $contact_info->setIncomplete();
      }
      return $contact_info;
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getInfoValue(EntityInterface $entity, $key, $name, DataDefinitionInterface $definition) {
    /** @var \Drupal\user\UserInterface $entity */
    if ($definition->getDataType() == 'email' || $name == 'email') {
      return $entity->getEmail();
    }

    if ($name == 'name') {
      return $entity->getDisplayName();
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel(EntityInterface $entity, $key) {
    return new TranslatableMarkup('Account Email');
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary(EntityInterface $entity, $key) {
    return $entity->getEmail();
  }

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param $key
   *
   * @return \Drupal\communication\Contact\ContactInfoDefinitionInterface
   */
  public function getInfoDefinition(EntityInterface $entity, $key) {
    return ContactInfoDefinition::create('email');
  }
}
