<?php

namespace Drupal\communication\Entity;

use Drupal\communication\Plugin\Field\CommunicationEventDateComputedFieldItemList;
use Drupal\communication\Plugin\Field\ComputedParticipantsReferenceFieldItemList;
use Drupal\communication\Plugin\Field\FieldType\ParticipantItem;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;
use Drupal\entity\BundleFieldDefinition;
use Drupal\user\EntityOwnerTrait;

/**
 * Class Communication
 *
 * @ContentEntityType(
 *   id = "communication",
 *   label = @Translation("Communication"),
 *   base_table = "communication",
 *   revision_table = "communication_revision",
 *   handlers = {
 *     "storage" = "Drupal\communication\Entity\CommunicationStorage",
 *     "access" =
 *   "Drupal\communication\Entity\CommunicationAccessControlHandler",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\communication\Form\CommunicationForm",
 *       "add" = "Drupal\communication\Form\CommunicationAddForm",
 *       "record" = "Drupal\communication\Form\CommunicationRecordForm",
 *     },
 *     "route_provider" = {
 *       "html" =
 *   "Drupal\communication\Entity\Routing\CommunicationHtmlRouteProvider",
 *     }
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "uuid" = "uuid",
 *     "label" = "subject",
 *     "bundle" = "mode",
 *     "owner" = "creator",
 *   },
 *   admin_permission = "administer communications",
 *   field_ui_base_route = "communication.mode_configure",
 *   links = {
 *     "canonical" = "/admin/communication/communication/{communication}",
 *     "edit-form" = "/admin/communication/communication/{communication}/edit",
 *     "add-page" = "/admin/communication/communication/new",
 *     "add-form" = "/admin/communication/communication/new/{mode}",
 *     "record-page" = "/admin/communication/communication/record",
 *     "record-form" = "/admin/communication/communication/record/{mode}",
 *   }
 * )
 *
 * @package Drupal\communication\Entity
 */
class Communication extends ContentEntityBase implements CommunicationInterface {
  use EntityOwnerTrait;

  /**
   * @var array
   */
  protected $_tempParticipantsStore = [];

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    // If this is a new communication and participants have been set onto the
    // computed participants fields, bad things are going to happen as all the
    // entities try to save eachother in a horrible spiral of hell.
    //
    // To prevent this we remove the entities from those computed fields and put
    // them in a temporary variable to be saved on post save.
    if ($this->isNew()) {
      foreach ($this->getMode()->participantRoles() as $role_name => $role_info) {
        $cardinality = $role_info['cardinality'] ?: 1;
        $plural_name = $role_info['plural_name'] ?: $role_name.'s';
        $field_name = $cardinality == 1 ? $role_name : $plural_name;

        $to_remove_from_computed_field = [];
        /** @var \Drupal\communication\Entity\CommunicationParticipant $participant */
        foreach ($this->get($field_name)->referencedEntities() as $delta => $participant) {
          if ($participant->id() && $participant->communication->entity) {
            // Participant is already saved. DON'T PANIC.
            continue;
          }

          $this->_tempParticipantsStore[$field_name][$delta] = $participant;
          $to_remove_from_computed_field[] = $delta;
        }

        // As items are removed from $this->get($field_name), the delta value
        // for each item could change. If we have two items (A & B) with
        // deltas 0 and 1 respectively, and we delete delta 0, item B will now
        // be at delta 0. We need to delete deltas in reverse
        // order (highest to lowest) to ensure we are deleting the correct
        // items.
        foreach (array_reverse($to_remove_from_computed_field) as $delta) {
          $this->get($field_name)->removeItem($delta);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    if (!empty($this->_tempParticipantsStore)) {
      foreach ($this->_tempParticipantsStore as $field_name => $items) {
        foreach ($items as $delta => $participant) {
          $participant->communication = $this;
          $participant->save();
          $this->{$field_name}->set($delta, $participant);
          unset($this->_tempParticipantsStore[$field_name][$delta]);
        }

        unset($this->_tempParticipantsStore[$field_name]);
      }
    }
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\Exception\UnsupportedEntityTypeDefinitionException
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields['subject'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Subject'))
      ->setCardinality(1)
      ->setRevisionable(TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['status'] = BaseFieldDefinition::create('list_string')
      ->setLabel(new TranslatableMarkup('Status'))
      ->setSetting('allowed_values_function', [static::class, 'allStatusOptions'])
      ->setCardinality(1)
      ->setRevisionable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'list_default',
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['previous'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Previous Communication'))
      ->setDescription(new TranslatableMarkup('The previous communication in as chain of communications, could be chained through forwarding or replying.'))
      ->setSetting('target_type', 'communication')
      ->setCardinality(1)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(new TranslatableMarkup('Created Date'))
      ->setCardinality(1)
      ->setDisplayConfigurable('view', TRUE);

    $fields['participants'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Particpants'))
      ->setComputed(TRUE)
      ->setSetting('target_type', 'communication_participant')
      ->setClass(ComputedParticipantsReferenceFieldItemList::class)
      ->setDisplayConfigurable('view', TRUE);
    $fields['participants']->setItemDefinition(
      $fields['participants']->getItemDefinition()
        ->setClass(ParticipantItem::class)
    );

    // @todo: Find a way of avoiding this work around.
    // @todo: The work around is probably hook_entity_field_storage_info
    // Views field plugin only supports computed fields if the are base fields
    // because computed fields don't get counted as a storage definition. We
    // would like to define bundle computed fields but this will not work with
    // views so we define them as base fields
    /** @var \Drupal\communication\CommunicationModePluginManager $mode_manager */
    $mode_manager = \Drupal::service('plugin.manager.communication.mode');
    foreach ($mode_manager->getDefinitions() as $mode_name => $definition) {
      /** @var \Drupal\communication\Plugin\Communication\Mode\ModeInterface $mode */
      $mode = $mode_manager->createInstance($mode_name);
      foreach ($mode->participantRoles() as $role_name => $role_def) {
        $cardinality = isset($role_def['cardinality']) ? $role_def['cardinality'] : 1;
        $plural_name = isset($role_def['plural_name']) ? $role_def['plural_name'] : $role_name.'s';

        $field_name = $cardinality == 1 ? $role_name : $plural_name;
        if (!empty($fields[$field_name])) {
          continue;
        }

        $plural_label = isset($role_info['plural_label']) ? $role_def['plural_label'] : $role_def['label'].'s';
        $fields[$field_name] = BaseFieldDefinition::create('entity_reference')
          ->setLabel($cardinality === 1 ? $role_def['label'] : $plural_label)
          ->setSetting('target_type', 'communication_participant')
          ->setSetting('handler', 'default:communication_participant')
          ->setSetting('participant_role', $role_name)
          ->setComputed(TRUE)
          ->setCardinality($cardinality)
          ->setClass(ComputedParticipantsReferenceFieldItemList::class);
        if (isset($role_def['description'])) {
          $fields[$field_name]->setDescription($role_def['description']);
        }

        $fields[$field_name]->setItemDefinition(
          $fields[$field_name]->getItemDefinition()
            ->setClass(ParticipantItem::class)
        );
      }
    }

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions) {
    $fields = parent::bundleFieldDefinitions($entity_type, $bundle, $base_field_definitions);

    $mode_manager = \Drupal::service('plugin.manager.communication.mode');

    /** @var \Drupal\communication\Plugin\Communication\Mode\ModeInterface $plugin */
    $plugin = $mode_manager->createInstance($bundle);
    $fields += $plugin->communicationFieldDefinitions($base_field_definitions);

    $fields['status'] = $base_field_definitions['status'];
    $fields['status']->setSetting('allowed_values_function', [get_class($plugin), 'statusOptions']);

    foreach ($base_field_definitions as $field_name => $base_field_definition) {
      $field_definition = clone $base_field_definition;

      /** @var \Drupal\Core\Field\BaseFieldDefinition $field_definition */
      if (!$field_definition->getSetting('participant_role')) {
        continue;
      }

      $role_name = $field_definition->getSetting('participant_role');
      $roles = $plugin->participantRoles();
      if (empty($roles[$role_name])) {
        $fields[$field_name] = $field_definition;
      }
      else {
        $role_info = $roles[$role_name];
        $cardinality = isset($role_info['cardinality']) ? $role_info['cardinality'] : 1;
        $plural_label = isset($role_info['plural_label']) ? $role_info['plural_label'] : $role_info['label'].'s';

        $field_definition->setLabel($cardinality === 1 ? $role_info['label'] : $plural_label);
        $field_definition->setSetting('handler_settings', ['target_bundles' => [$role_info['type'] => $role_info['type']]]);
        if (isset($role_info['description'])) {
          $field_definition->setDescription($role_info['description']);
        }
        else {
          $field_definition->setDescription('');
        }
        // Set Field definition.
        $field_definition->setDisplayConfigurable('view', TRUE)
          ->setDisplayOptions('form', [
            'type' => 'inline_entity_form_complex',
          ])
          ->setDisplayConfigurable('form', TRUE)
          ->setDisplayOptions('view', [
            'type' => 'entity_reference_label',
            'label' => $role_info->getCardinality() == 1 ? 'inline' : 'above',
            'settings' => [
              'link' => FALSE,
            ]
          ]);
        $fields[$field_name] = $field_definition;
      }
    }

    $supported_events = $plugin->supportedEvents();
    if (isset($supported_events['sent'])) {
      $fields['sent_date'] = BundleFieldDefinition::create('datetime')
        ->setSetting('datetime_type', DateTimeItem::DATETIME_TYPE_DATETIME)
        ->setLabel(new TranslatableMarkup('@label Date', ['@label' => $supported_events['sent']['label']]))
        ->setClass(CommunicationEventDateComputedFieldItemList::class)
        ->setComputed(TRUE)
        ->setDisplayOptions('view', [
          'type' => 'datetime_default',
        ])
        ->setDisplayOptions('form', [
          'type' => 'datetime_default',
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE)
        ->setSetting('communication_event_type', 'sent');
    }
    if (isset($supported_events['received'])) {
      $fields['received_date'] = BundleFieldDefinition::create('datetime')
        ->setSetting('datetime_type', DateTimeItem::DATETIME_TYPE_DATETIME)
        ->setLabel(new TranslatableMarkup('@label Date', ['@label' => $supported_events['received']['label']]))
        ->setClass(CommunicationEventDateComputedFieldItemList::class)
        ->setComputed(TRUE)
        ->setSetting('communication_event_type', 'received')
        ->setDisplayOptions('view', [
          'type' => 'datetime_default',
        ])
        ->setDisplayOptions('form', [
          'type' => 'datetime_default',
        ])
        ->setDisplayConfigurable('view', TRUE)
        ->setDisplayConfigurable('form', TRUE);
    }

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function allStatusOptions(FieldDefinitionInterface $definition, EntityInterface $entity = NULL, &$cacheable = TRUE) {
    $status_options = [];

    $mode_manager = \Drupal::service('plugin.manager.communication.mode');
    foreach ($mode_manager->getDefinitions() as $id => $mode_definition) {
      $status_options += $mode_definition['class']::statusOptions($definition, $entity, $cacheable);
    }

    return $status_options;
  }

  /**
   * {@inheritdoc}
   */
  public function getParticipants($role_name = NULL) {
    $field_name = $role_name;
    if (!$field_name) {
      $field_name = 'participants';
    }
    else {
      $roles_info = $this->getMode()->participantRoles();
      if (!$roles_info[$role_name]) {
        return [];
      }

      if ($roles_info[$role_name]['cardinality'] !== 1) {
        $field_name = $roles_info[$role_name]['plural_name'] ?: $role_name.'s';
      }
    }

    if ($this->get($field_name) instanceof ComputedParticipantsReferenceFieldItemList) {
      return $this->get($field_name)->referencedEntities();
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getParticipant($role_name) {
    $participants = $this->getParticipants($role_name);

    return !empty($participants) ? reset($participants) : NULL;
  }

  /**
   * Get the mode plugin.
   *
   * @return \Drupal\communication\Plugin\Communication\Mode\ModeInterface;
   */
  public function getMode() {
    return \Drupal::service('plugin.manager.communication.mode')->createInstance($this->mode->value);
  }

  /**
   * Log an event for this communication.
   *
   * @param $type
   * @param null $date
   * @param null $notes
   * @param bool $save
   *
   * @return \Drupal\communication\Entity\CommunicationEvent
   */
  public function logEvent($type, $date = NULL, $notes = NULL, $save = TRUE) {
    $event_storage = \Drupal::entityTypeManager()->getStorage('communication_event');

    /** @var \Drupal\communication\Entity\CommunicationEvent $event */
    $event = $event_storage->create(array_filter([
      'type' => $type,
      'communication' => $this,
      'date' => $date,
      'notes' => $notes,
    ]));
    if ($save) {
      $event->save();
    }

    return $event;
  }

  /**
   * Check the operation access.
   *
   * @param $operation
   * @param array $options
   * @param \Drupal\Core\Session\AccountInterface|NULL $account
   * @param bool $return_as_object
   *
   * @return \Drupal\Core\Access\AccessResultInterface|bool
   */
  public function operationAccess($operation, array $options = [], AccountInterface $account = NULL, $return_as_object = FALSE) {
    return $this->entityTypeManager()
      ->getAccessControlHandler($this->entityTypeId)
      ->operationAccess($this, $operation, $options, $account, $return_as_object);
  }
}
