<?php

namespace Drupal\communication\Event;

use Symfony\Contracts\EventDispatcher\Event;
use Drupal\communication\Entity\CommunicationInterface;

/**
 * Class CommunicationOperationEvent
 *
 * @package Drupal\communication\Event
 */
class CommunicationOperationEvent extends Event {

  /**
   * @var \Drupal\communication\Entity\CommunicationInterface
   */
  protected $communication;

  /**
   * @var array
   */
  protected $options;

  /**
   * @var string
   */
  protected $operation;

  /**
   * CommunicationOperationEvent constructor.
   *
   * @param \Drupal\communication\Entity\CommunicationInterface $communication
   * @param array $options
   */
  public function __construct(CommunicationInterface $communication, $operation, array $options =[]) {
    $this->communication = $communication;
    $this->options = $options;
    $this->operation = $operation;
  }

  /**
   * @return string
   */
  public function getOperation() {
    return $this->operation;
  }

  /**
   * Get the communication.
   *
   * @return \Drupal\communication\Entity\CommunicationInterface
   */
  public function getCommunication() {
    return $this->communication;
  }

  /**
   * Set the options.
   *
   * @param array $options
   *
   * @return static
   */
  public function setOptions(array $options = []) {
    $this->options = $options;

    return $this;
  }

  /**
   * Set an options
   *
   * @param $key
   * @param $value
   *
   * @return static
   */
  public function setOption($key, $value) {
    $this->options[$key] = $value;

    return $this;
  }

  /**
   * Set a get options.
   *
   * @param $key
   * @param null $default
   */
  public function getOption($key, $default = NULL) {
    return isset($this->options[$key]) ? $this->options[$key] : $default;
  }

  /**
   * Get the options
   *
   * @return array
   */
  public function getOptions() {
    return $this->options;
  }

}
