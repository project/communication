<?php

namespace Drupal\communication\Exception;

/**
 * Class UnsupportedCommunicationModeException
 *
 * @package Drupal\communication\Exception
 */
class UnsupportedCommunicationModeException extends \Exception {

}
