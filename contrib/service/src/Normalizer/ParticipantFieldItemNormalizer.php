<?php

namespace Drupal\communication_service\Normalizer;

use Drupal\communication\Entity\CommunicationParticipant;
use Drupal\communication\Plugin\Field\FieldType\ParticipantItem;
use Drupal\serialization\Normalizer\EntityReferenceFieldItemNormalizer;
use Drupal\serialization\Normalizer\FieldItemNormalizer;

/**
 * Class ParticipantFieldItemNormalizer
 *
 * @package Drupal\communication_service\Normalizer
 */
class ParticipantFieldItemNormalizer extends EntityReferenceFieldItemNormalizer {

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = ParticipantItem::class;

  /**
   * {@inheritdoc}
   */
  protected function constructValue($data, $context) {
    // If this has been sent as a normal field item then pass it through to
    // the parent class.
    if (isset($data['target_id']) || isset($data['target_type']) || isset($data['target_uuid'])) {
      return parent::constructValue($data, $context);
    }

    // Allow people to submit the the participant info under the entity key.
    if (isset($data['entity'])) {
      $data = $data['entity'];
    }

    /** @var \Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem $field_item */
    $field_item = $context['target_instance'];
    if (!isset($data['role']) && ($role = $field_item->getFieldDefinition()->getSetting('participant_role'))) {
      $data['role'] = $role;
    }

    // Denormalize a complete participant entity into this field.
    $participant = $this->serializer->denormalize($data, CommunicationParticipant::class, NULL, $context);
    return ['entity' => $participant];
  }

  /**
   * @param $field_item
   * @param null $format
   * @param array $context
   *
   * @return array|bool|float|int|null|string
   */
  public function normalize($field_item, $format = NULL, array $context = []) {
    $values = FieldItemNormalizer::normalize($field_item, $format, $context);

    if ($participant = $field_item->get('entity')->getValue()) {
      $values += $this->serializer->normalize($participant, $format, $context);
    }

    return $values;
  }
}
