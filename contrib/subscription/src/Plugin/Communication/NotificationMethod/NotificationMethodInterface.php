<?php

namespace Drupal\communication_subscription\Plugin\Communication\NotificationMethod;

use Drupal\communication\Entity\CommunicationEvent;
use Drupal\communication_subscription\Entity\Subscription;
use Drupal\entity\BundlePlugin\BundlePluginInterface;

/**
 * Interface NotificationMethodInterface
 *
 * @package Drupal\communication_subscription\Plugin\Communication\NotificationMethod
 */
interface NotificationMethodInterface extends BundlePluginInterface {

  /**
   * Notify the subscriber of the event.
   *
   * @param \Drupal\communication\Entity\CommunicationEvent $event
   * @param \Drupal\communication_subscription\Entity\Subscription $subscription
   *
   * @return mixed
   */
  public function notify(CommunicationEvent $event, Subscription $subscription);
}
