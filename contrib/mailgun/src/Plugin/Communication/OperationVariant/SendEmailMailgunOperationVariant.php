<?php

namespace Drupal\communication_mailgun\Plugin\Communication\OperationVariant;

use Drupal\communication\EmailSendOperationHelperTrait;
use Drupal\communication\Entity\CommunicationInterface;
use Drupal\communication\Plugin\Communication\OperationVariant\OperationVariantBase;
use Drupal\Component\Utility\Mail;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Theme\ThemeInitializationInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\Core\Url;
use Egulias\EmailValidator\EmailLexer;
use Egulias\EmailValidator\EmailParser;
use Egulias\EmailValidator\EmailValidator;
use Egulias\EmailValidator\Validation\RFCValidation;
use Html2Text\Html2Text;
use Mailgun\Mailgun;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SendEmailMailgunOperationVariant
 *
 * @CommunicationOperationVariant(
 *   id = "send_email_mailgun",
 *   label = @Translation("Mailgun"),
 *   modes = {"email"},
 *   operation = "send",
 *   exclude_modes = {},
 * )
 *
 * @package Drupal\communication_mailgun\Plugin\Communication\OperationVariant
 */
class SendEmailMailgunOperationVariant extends OperationVariantBase implements ContainerFactoryPluginInterface {
  use EmailSendOperationHelperTrait;

  /**
   * @var \Mailgun\Mailgun
   */
  protected $mailgunClient;

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected $themeManager;

  /**
   * @var \Drupal\Core\Theme\ThemeInitializationInterface
   */
  protected $themeInitialization;

  /**
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Configuration object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $mailgunConfig;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('mailgun.mailgun_client'),
      $container->get('renderer'),
      $container->get('entity_type.manager'),
      $container->get('file_system'),
      $container->get('config.factory'),
      $container->get('theme.manager'),
      $container->get('theme.initialization'),
      $container->get('module_handler'),
      $container->get('logger.factory')
    );
  }

  /**
   * SendEmailMailgunOperationVariant constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Mailgun\Mailgun $mailgun_client
   * @param \Drupal\Core\Render\RendererInterface $renderer
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\Core\Theme\ThemeManagerInterface $theme_manager
   * @param \Drupal\Core\Theme\ThemeInitializationInterface $theme_initialization
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    Mailgun $mailgun_client,
    RendererInterface $renderer,
    EntityTypeManagerInterface $entity_type_manager,
    FileSystemInterface $file_system,
    ConfigFactoryInterface $config_factory,
    ThemeManagerInterface $theme_manager,
    ThemeInitializationInterface $theme_initialization,
    ModuleHandlerInterface $module_handler,
    LoggerChannelFactoryInterface $logger_channel_factory
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager);
    $this->mailgunClient = $mailgun_client;
    $this->renderer = $renderer;
    $this->fileSystem = $file_system;
    $this->mailgunConfig = $config_factory->get('mailgun.settings');
    $this->themeManager = $theme_manager;
    $this->themeInitialization = $theme_initialization;
    $this->configFactory = $config_factory;
    $this->moduleHandler = $module_handler;
    $this->loggerFactory = $logger_channel_factory;
  }

  /**
   * Run this operation.
   *
   * @param \Drupal\communication\Entity\CommunicationInterface $communication
   * @param array $options
   *
   * @return CommunicationInterface
   *   The communication with the operation performed on it.
   */
  public function run(CommunicationInterface $communication, array $options = []) {
    $params = [];
    $params['from'] = $this->reduceRoleParticipants($communication, 'from');
    $params['to'] = $this->reduceRoleParticipants($communication, 'to');
    $params['cc'] = $this->reduceRoleParticipants($communication, 'cc');
    $params['bcc'] = $this->reduceRoleParticipants($communication, 'bcc');
    if ($reply_to = current($communication->getParticipants('reply_to'))) {
      $params['h:Reply-To'] = $this->reduceParticipant($reply_to);
    }

    if (empty($params['from'])) {
      $site_config = $this->configFactory->get('system.site');
      $site_mail = $site_config->get('mail');
      if (empty($site_mail)) {
        $site_mail = ini_get('sendmail_from');
      }

      $params['from'] = Mail::formatDisplayName($site_config->get('name')) . ' <' . $site_mail . '>';
    }

    $params['subject'] = $communication->subject->value;

    // Sort out the html.
    $plain_text = $communication->body_plain->value;
    if (!$communication->body_html->isEmpty()) {
      if (stripos($communication->body_html->value, '<html>') !== FALSE) {
        $content = (string) $communication->body_html->value;
      }
      else {
        $build = $this->entityTypeManager->getViewBuilder('communication')
          ->view($communication, 'send');
        $this->themeManager->setActiveTheme(
          $this->getEmailTheme($communication)
        );
        $content = $this->renderer->renderRoot($build);
        $this->themeManager->resetActiveTheme();
      }
      if (empty($plain_text)) {
        $plain_text = (new Html2Text($content))->getText();
      }
      $params['html'] = $content;

      // Expand all local links.
      $pattern = '/(<a[^>]+href=")([^"]*)/mi';
      $params['html'] = preg_replace_callback($pattern, function($matches) {
        $url = urldecode($matches[2]);

        $is_absolute = \Drupal::service('file_system')->uriScheme($url) != FALSE || preg_match('!(mailto|callto|tel)\:!', $url);

        // Strip the base path as Uri adds it again at the end.
        $base_path = rtrim(base_path(), '/');
        $url = preg_replace('!^' . $base_path . '!', '', $url, 1);

        if ($is_absolute) {
          return $matches[1].str_replace(' ', '%20', $url);
        }

        $url = str_replace('?q=', '', $url);
        @list($url, $fragment) = explode('#', $url, 2);
        @list($path, $query) = explode('?', $url, 2);

        // If we're dealing with an intra-document reference, return it.
        if (empty($path)) {
          return $matches[1].'#' . $fragment;
        }

        // Get a list of enabled languages.
        $languages = \Drupal::languageManager()->getLanguages(LanguageInterface::STATE_ALL);

        // Default language settings.
        $prefix = '';
        $language =  \Drupal::languageManager()->getDefaultLanguage();

        // Check for language prefix.
        $args = explode('/', $path);
        foreach ($languages as $lang) {
          if ($args[1] == $lang->getId()) {
            $prefix = array_shift($args);
            $language = $lang;
            $path = implode('/', $args);
            break;
          }
        }

        parse_str($query, $arr);
        $options = [
          'query' => !empty($arr) ? $arr : [],
          'fragment' => $fragment,
          'absolute' => TRUE,
          'language' => $language,
          'prefix' => $prefix,
        ];

        $url = Url::fromUserInput($path, $options)->toString();

        // If url() added a ?q= where there should not be one, remove it.
        if (preg_match('!^\?q=*!', $url)) {
          $url = preg_replace('!\?q=!', '', $url);
        }

        $url = str_replace('+', '%2B', $url);
        return $matches[1].$url;
      }, $params['html']);
    }
    $params['text'] = $plain_text;

    // Thread emails.
    if (!$communication->previous->isEmpty() && $communication->previous->entity->mailgun_id && !$communication->previous->entity->mailgun_id->isEmpty()) {
      $params['h:In-Reply-To'] = $communication->previous->entity->mailgun_id->value;
    }

    // Attachments
    $params['attachment'] = [];
    foreach ($communication->attachments as $attachment_item) {
      $uri = $attachment_item->entity->uri;
      $path = $this->fileSystem->realpath($uri);

      if (is_readable($path)) {
        $params['attachment'][] = $path;
      }
    }

    // If there is no id then save the communication.
    if (!$communication->id()) {
      $communication->save();
    }
    $params['v:communication-id'] = $communication->id();

    // Mailgun will accept the message but will not send it.
    if ($this->mailgunConfig->get('test_mode')) {
      $params['o:testmode'] = 'yes';
    }
    $params = array_filter($params);


    try {
      $domain = $this->getDomain($communication);
      $response = $this->mailgunClient->messages()->send($domain, $params);
      $communication->setNewRevision();
      $communication->mailgun_id = $response->getId();
      $communication->status = 'sent';
      $communication->save();
      $communication->logEvent('sent');
    }
    catch (\Exception $e) {
      $this->logger()->error("Communication {$communication->id()} Failed: {$e->getMessage()}");

      $communication->setNewRevision();
      $communication->status = 'failed';
      $communication->save();
    }

    return $communication;
  }

  /**
   * @param \Drupal\communication\Entity\CommunicationInterface $communication
   *
   * @return string
   */
  protected function getDomain(CommunicationInterface $communication) {
    $domain = $this->mailgunConfig->get('working_domain');
    if ($domain !== '_sender') {
      return $domain;
    }

    if ($from = current($communication->getParticipants('from'))) {
      $from_email = $from->email->value;
    }
    else if ($mail = $this->configFactory->get('system.site')->get('mail')) {
      $from_email = $mail;
    }
    else {
      $from_email = ini_get('sendmail_from');
    }

    if (!$from_email) {
      throw new \Exception('Unable to ascertain domain: no from address');
    }

    $parser = new EmailParser(new EmailLexer());
    $validator = new EmailValidator();
    if ($validator->isValid($from_email, new RFCValidation()) === TRUE) {
      return $parser->parse($from_email)['domain'];
    }

    // Extract the domain from the sender's email address.
    // Use regular expression to check since it could be either a plain email
    // address or in the form "Name <example@example.com>".
    $tokens = (preg_match('/^\s*(.+?)\s*<\s*([^>]+)\s*>$/', $from_email, $matches) === 1) ? explode('@', $matches[2]) : explode('@', $from_email);
    return array_pop($tokens);
  }

  /**
   * Get the ActiveTheme object for emails.
   */
  protected function getEmailTheme(CommunicationInterface $communication) {
    $theme = $this->configFactory->get('system.theme')->get('default');

    $this->moduleHandler->alter('communication_email_mailgun_theme', $theme,$communication);

    return $this->themeInitialization->initTheme($theme);
  }

  /**
   * Validate whether this operation can be run.
   *
   * @param \Drupal\communication\Entity\CommunicationInterface $communication
   * @param array $options
   * @param array $reasons
   *
   * @return boolean
   *   True if the operation can be run. False if it cannot.
   */
  public function validate(CommunicationInterface $communication, array $options = [], array &$reasons = []) {
    // @todo: Implement validation.
    return TRUE;
  }

  /**
   * Get the logger
   *
   * @return \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected function logger() {
    return $this->loggerFactory->get('communication');
  }
}
