<?php

namespace Drupal\communication\Contact;

use Drupal\Core\TypedData\DataDefinitionInterface;

interface ContactInfoDefinitionInterface extends DataDefinitionInterface {

  /**
   * @param $name
   * @param array $options
   *
   * @return static
   */
  public function addUsageConstraint($name, array $options = []);

  /**
   * @param $name
   *
   * @return array
   */
  public function getUsageConstraint($name);

  /**
   * @return array
   */
  public function getUsageConstraints();

  /**
   * @param array $constraints
   *
   * @return static
   */
  public function setUsageConstraints(array $constraints = []);

}
