<?php

namespace Drupal\communication\Plugin\ContactInfoSource;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

interface ContactInfoSourceMetadataInterface extends ContactInfoSourceInterface {

  /**
   * The form for setting metadata.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function metadataForm(array $form, FormStateInterface $form_state, EntityInterface $entity, $sub_key);
}
