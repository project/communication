<?php

namespace Drupal\communication\Plugin\Communication\Mode;

/**
 * Class CommunicationLongTextModeBase
 *
 * @package Drupal\communication\Plugin\CommunicationMode
 */
abstract class TextContentModeBase extends ModeBase {

  /**
   * Get the long text fields.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   */
  public abstract function textContentFieldDefinitions();

  /**
   * {@inheritdoc}
   */
  public function communicationFieldDefinitions(array $base_field_definitions) {
    $fields = parent::communicationFieldDefinitions($base_field_definitions);
    $fields += static::textContentFieldDefinitions();
    return $fields;
  }
}
