<?php

namespace Drupal\communication\Plugin\Communication\Operation;

use Drupal\communication\Entity\CommunicationInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;

interface OperationInterface extends PluginInspectionInterface {

  /**
   * Run this operation.
   *
   * @param \Drupal\communication\Entity\CommunicationInterface $communication
   * @param array $options
   *
   * @return CommunicationInterface
   *   The communication with the operation performed on it.
   */
  public function run(CommunicationInterface $communication, array $options = []);

  /**
   * Validate whether this operation can be run.
   *
   * @param \Drupal\communication\Entity\CommunicationInterface $communication
   * @param array $options
   * @param array $reasons
   *
   * @return boolean
   *   True if the operation can be run. False if it cannot.
   */
  public function validate(CommunicationInterface $communication, array $options = [], array &$reasons = []);

  /**
   * Determine whether this operation is applicable to the entity provided with
   * the options. Options may become deprecated in favour of just the variant
   * requested.
   *
   * @param \Drupal\communication\Entity\CommunicationInterface $communication
   * @param array $options
   *
   * @return boolean
   *   True if applicable, False otherwise.
   */
  public function applicable(CommunicationInterface $communication, array $options = []);

  /**
   * Determine whether or not this operation has a form.
   *
   * @param \Drupal\communication\Entity\CommunicationInterface $communication
   * @param array $options
   *
   * @return boolean
   *   True if applicable, False otherwise.
   */
  public function hasForm(CommunicationInterface $communication, array $options = []);

  /**
   * Build the form shown to users when performing the operation.
   *
   * @return array
   */
  public function form(array $form, FormStateInterface $form_state, CommunicationInterface $communication);

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\communication\Entity\CommunicationInterface $communication
   *
   * @return void
   */
  public function formValidate(array $form, FormStateInterface $form_state, CommunicationInterface $communication);

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\communication\Entity\CommunicationInterface $communication
   *
   * @return void
   */
  public function formSubmit(array $form, FormStateInterface $form_state, CommunicationInterface $communication);

  /**
   * Perform UI/form operation post operation. Can be used for initiating downloads
   * etc.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\communication\Entity\CommunicationInterface $communication
   *
   * @return mixed
   */
  public function formPostOperation(array $form, FormStateInterface $form_state, CommunicationInterface $communication);


}
