<?php

namespace Drupal\communication_service\Plugin\rest\resource;

use Drupal\rest\Plugin\rest\resource\EntityResource;

/**
 * Class CommunicationResource
 *
 * @package Drupal\communication_service\Plugin\rest\resource
 *
 * @todo: Support file uploads better. https://www.drupal.org/project/drupal/issues/1927648?page=1
 */
class CommunicationResource extends EntityResource {

}
