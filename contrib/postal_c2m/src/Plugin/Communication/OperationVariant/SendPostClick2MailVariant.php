<?php

namespace Drupal\communication_postal_c2m\Plugin\Communication\OperationVariant;

use Drupal\communication\Entity\CommunicationInterface;
use Drupal\communication\Plugin\Communication\OperationVariant\OperationVariantBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\pdf_tools\PDFAnalyserInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\GuzzleException;
use function GuzzleHttp\Psr7\build_query;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\Serializer;

/**
 * Class SendEmailMailgunOperationVariant
 *
 * @CommunicationOperationVariant(
 *   id = "send_post_click2mail",
 *   label = @Translation("Send Post Click2Mail"),
 *   modes = {"post"},
 *   operation = "send",
 *   exclude_modes = {},
 * )
 *
 * @package Drupal\communication_mailgun\Plugin\Communication\OperationVariant
 */
class SendPostClick2MailVariant extends OperationVariantBase implements ContainerFactoryPluginInterface {

  const HOST = 'https://rest.click2mail.com/molpro';

  /**
   * Production time constants.
   */
  const PROD__NEXT_DAY = 'Next Day';

  /**
   * Billing contants.
   */
  const BILLING__ACH = 'ACH';
  const BILLING__CREDIT = 'Credit Card';

  /**
   * Layout classes
   */
  const LAYOUT__ADDRESS_SEPARATE_PAGE = 'Address on Separate Page';
  const LAYOUT__PIC_ADDRESS_FIRST_PAGE = 'Picture and Address First Page';

  /**
   * Doc classes
   */
  const DOC__LETTER85x11 = 'Letter 8.5 x 11';
  const DOC__CERT_LETTER85x11 = 'Certified Letter 8.5 x 11';

  /**
   * Mail Classes
   */
  const CLASS__FIRST_CLASS = 'First Class';
  const CLASS__CERTIFIED = 'Certified Mail';
  const CLASS__CERT_ERR = 'Cert. w/electronic return receipt';
  const CLASS__CERT_RR_RD = 'Cert. w/rtn. and restricted delivery';
  const CLASS__CERT_RR = 'Certified Mail w/return receipt';

  /**
   * Evnelope constants
   */
  const ENVELOPE__10_DOUBLE_WINDOW = '#10 Double Window';
  const ENVELOPE__10_OPEN_WINDOW = '#10 Open Window Envelope';
  const ENVELOPE__FLAT = 'Flat Envelope';
  const ENVELOPE__CERTIFIED_LETTER = 'Certified Mail Letter Envelope';
  const ENVELOPE__CERTIFIED_FLAT = 'Certified Mail Flat Envelope';

  /**
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var \Symfony\Component\Serializer\Serializer
   */
  protected $serializer;

  /**
   * @var \Drupal\pdf_tools\PDFAnalyser
   */
  protected $pdfAnalyser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('http_client'),
      $container->get('config.factory'),
      $container->get('serializer'),
      $container->get('pdf_tools.analyser')
    );
  }

  /**
   * SendPostClick2MailVariant constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \GuzzleHttp\ClientInterface $client
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Symfony\Component\Serializer\Serializer $serializer
   * @param \Drupal\pdf_tools\PDFAnalyserInterface $pdf_analyser
   */
  public function __construct(
    array $configuration, $plugin_id, $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    ClientInterface $client,
    ConfigFactoryInterface $config_factory,
    Serializer $serializer,
    PDFAnalyserInterface $pdf_analyser
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager);

    $this->httpClient = $client;
    $this->configFactory = $config_factory;
    $this->serializer = $serializer;
    $this->pdfAnalyser = $pdf_analyser;
  }

  public function applicable(CommunicationInterface $communication, array $options = []) {
    if (!parent::applicable($communication, $options)) {
      return FALSE;
    }

    return (bool) $this->configFactory->get('communication_postal_c2m.settings')->get('enabled');
  }

  /**
   * Run this operation.
   *
   * @param \Drupal\communication\Entity\CommunicationInterface $communication
   * @param array $options
   *
   * @return CommunicationInterface
   *   The communication with the operation performed on it.
   */
  public function run(CommunicationInterface $communication, array $options = []) {
    $config = $this->configFactory->get('communication_postal_c2m.credentials');
    $username = isset($options['c2m_username']) ? $options['c2m_username'] : $config->get('username');
    $password = isset($options['c2m_password']) ? $options['c2m_password'] : $config->get('password');

    // Default request options.
    $get_options = $post_options = [
      'auth' => [$username, $password],
      'headers' => [
        'Accept' => 'application/json',
      ],
    ];
    $post_options['headers']['Content-Type'] = 'application/xml';

    try {
      $master_document_id = NULL;
      $page_count_estimate = $this->pdfAnalyser->countPages($communication->pdf->entity->getFileUri());

      $mail_class = isset($options['mail_class']) ? $options['mail_class'] : static::CLASS__FIRST_CLASS;
      $document_class = ($mail_class === static::CLASS__FIRST_CLASS) ? static::DOC__LETTER85x11 : static::DOC__CERT_LETTER85x11;

      /** @var \GuzzleHttp\Psr7\Response $response */
      $response = $this->httpClient->post(static::HOST . '/documents', [
          'multipart' => [
            [
              'name' => 'documentClass',
              'contents' => $document_class,
            ],
            [
              'name' => 'documentFormat',
              'contents' => 'PDF',
            ],
            [
              'name' => 'file',
              'contents' => fopen($communication->pdf->entity->getFileUri(), 'r'),
              'headers' => [
                'Content-Type' => 'application/pdf',
              ],
            ],
          ],
        ] + $get_options);
      $document_info = $this->serializer->decode($response->getBody(), 'json');
      $master_document_id = $document_info['id'];

      // Attachments.
      if ($communication->attachments && !$communication->attachments->isEmpty()) {
        $document_ids = [];

        /** @var \Drupal\file\Plugin\Field\FieldType\FileItem $file_item */
        foreach ($communication->attachments as $file_item) {
          /** @var \Drupal\file\Entity\File $file */
          $file = $file_item->entity;
          $extension = strtolower(pathinfo($file->getFilename(), PATHINFO_EXTENSION));
          if ($extension === 'pdf') {
            $page_count_estimate += $this->pdfAnalyser->countPages($file->getFileUri());
          }
          else {
            // For now assume that any other attachment is one page.
            // @todo: This is terrible!
            $page_count_estimate += 1;
          }

          // @todo: Do we need to convert to pdf first, we might benefit by
          // able to compute pages.
          $response = $this->httpClient->post(static::HOST.'/documents', [
            'multipart' => [
              [
                'name' => 'documentClass',
                'contents' => $document_class,
              ],
              [
                'name' => 'documentFormat',
                'contents' => strtoupper($extension),
              ],
              [
                'name' => 'file',
                'contents' => fopen($file->getFileUri(), 'r'),
                'headers' => [
                  'Content-Type' => $file->getMimeType(),
                ],
              ],
            ],
          ] + $get_options);
          $attachment_info = $this->serializer->decode($response->getBody(), 'json');

          $document_ids[] = $attachment_info['id'];
        }

        $data = '<documentList><documentId>'.$master_document_id.'</documentId>';
        foreach ($document_ids as $id) {
          $data .= '<documentId>'.$id.'</documentId>';
        }
        $data .= '</documentList>';
        $this->httpClient->post(
          static::HOST.'/documents/merge',
          [
            'body' => $data,
            'headers' => [
              'Accept' => 'application/json',
              'Content-Type' => 'application/xml',
            ],
          ] + $get_options
        );
      }

      // Make addresslist.
      $recipient_address = $communication->getParticipant('recipient')->address->get(0)->getValue();

      $xml = new \SimpleXMLElement('<addressList></addressList>');
      $xml->addChild('addressListName', 'Communication '.$communication->id().' Recipients');
      $xml->addChild('addressMappingId', 2);
      $list = $xml->addChild('addresses');
      $addressXML = $list->addChild('address');
      $addressXML->addChild('First_name', $recipient_address['given_name']);
      $addressXML->addChild('Last_name', $recipient_address['family_name']);
      $addressXML->addChild('Organization', $recipient_address['organization']);
      $addressXML->addChild('Address1', $recipient_address['address_line1']);
      $addressXML->addChild('Address2', $recipient_address['address_line2']);
      $addressXML->addChild('City', $recipient_address['locality']);
      $addressXML->addChild('State', $recipient_address['administrative_area']);
      $addressXML->addChild('Zip', $recipient_address['postal_code']);
      $addressXML->addChild('Country_non-US', $recipient_address['country_code']);

      $response = $this->httpClient->post(static::HOST . '/addressLists', [
          'body' => $xml->asXML(),
        ] + $post_options);
      $list_info = $this->serializer->decode($response->getBody(), 'json');

      // Build Job Options.
      $job_options = [
        'documentClass' => $document_class,
        'layout' => static::LAYOUT__ADDRESS_SEPARATE_PAGE,
        'productionTime' => static::PROD__NEXT_DAY,
        'color' => 'Black and White', // @todo: Constantize
        'paperType' => 'White 24#', // @todo: Constantize
        'printOption' => 'Printing both sides', // @todo: Constantize
        'documentId' => $master_document_id,
        'addressId' => $list_info['id'],
        'mailClass' => $mail_class,
      ];

      if ($job_options['layout'] === static::LAYOUT__ADDRESS_SEPARATE_PAGE) {
        $page_count_estimate++;
      }

      if ($mail_class === static::CLASS__FIRST_CLASS) {
        $job_options['envelope'] = static::ENVELOPE__10_DOUBLE_WINDOW;
        if ($job_options['layout'] === static::LAYOUT__PIC_ADDRESS_FIRST_PAGE) {
          if ($page_count_estimate > 12) {
            throw new \Exception(new TranslatableMarkup('Click2Mail Too Many Pages: Documents sent using Picture and Address First Page can have a maximum of 12 pages.'));
          }

          $job_options['envelope'] = static::ENVELOPE__10_OPEN_WINDOW;
        }
        if ($page_count_estimate > 6) {
          $job_options['envelope'] = static::ENVELOPE__FLAT;
        }
      }
      else {
        // For certified mail we use the letter envelope for 10 or fewer pages
        // and the flat envelope otherwise.
        $job_options['envelope'] = static::ENVELOPE__CERTIFIED_LETTER;
        if ($page_count_estimate > 10) {
          $job_options['envelope'] = static::ENVELOPE__CERTIFIED_FLAT;
        }
      }

      if ($return = $communication->getParticipant('return')) {
        /** @var \Drupal\address\Plugin\Field\FieldType\AddressItem $return_address */
        $return_address = $return->get('address')->get(0);
        if (!$return_address->isEmpty()) {
          $name_bits = array_filter([
            $return_address->getGivenName(),
            $return_address->getFamilyName(),
          ]);

          if (!empty($name_bits)) {
            $job_options['rtnName'] = implode(' ', $name_bits);
          }
          if ($return_address->getOrganization()) {
            $job_options['rtnOrganization'] = $return_address->getOrganization();
          }

          $job_options += array_filter([
            'rtnaddress1' => $return_address->getAddressLine1(),
            'rtnaddress2' => $return_address->getAddressLine2(),
            'rtnCity' => $return_address->getLocality(),
            'rtnState' => $return_address->getAdministrativeArea(),
            'rtnZip' => $return_address->getPostalCode(),
          ]);
        }
      }

      $response = $this->httpClient->post(static::HOST . '/jobs', [
          'body' => build_query($job_options),
        ] + $get_options);
      $job = $this->serializer->decode($response->getBody(), 'json');

      $billing_params = [
        'billingType' => static::BILLING__ACH,
      ];
      $response = $this->httpClient->post(static::HOST . '/jobs/' . $job['id'] . '/submit', [
          'body' => build_query($billing_params),
        ] + $get_options);
      $submission = $this->serializer->decode($response->getBody(), 'json');

      $communication->isNewRevision();
      $communication->status = 'sent';

      $this->eventStorage()->create([
        'type' => 'sent',
        'communication' => $communication,
        'operation_variant' => $this->getPluginId(),
        'notes' => 'Sent by Click2Mail',
      ])->save();

      // @todo: Consider storing these on a communication event.
      // @todo: Consider making cost and response generic to postal.
      $communication->c2m_response = $submission['description'];
      $communication->c2m_job = $submission['id'];
      $communication->c2m_job_cost = $submission['cost'];

      $communication->save();
    }
    catch (GuzzleException $exception) {
      $this->eventStorage()->create([
        'type' => 'operation_failed',
        'communication' => $communication,
        'operation' => 'send',
        'operation_vaiant' => $this->getPluginId(),
        'notes' => $exception->getMessage(),
      ])->save();

      $communication->isNewRevision();
      $communication->status = 'failed';
      $communication->save();
    }
  }

  /**
   * Validate whether this operation can be run.
   *
   * @param \Drupal\communication\Entity\CommunicationInterface $communication
   * @param array $options
   * @param array $reasons
   *
   * @return boolean
   *   True if the operation can be run. False if it cannot.
   */
  public function validate(CommunicationInterface $communication, array $options = [], array &$reasons = []) {
    return TRUE;
  }
}
