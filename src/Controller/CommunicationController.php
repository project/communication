<?php

namespace Drupal\communication\Controller;

use Drupal\communication\CommunicationModePluginManager;
use Drupal\communication\Contact\ContactMiddlewareInterface;
use Drupal\communication\ParticipantRole;
use Drupal\Core\Entity\Controller\EntityController;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Link;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface;

class CommunicationController extends EntityController {

  /**
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * @var \Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface
   */
  protected $argumentResolver;

  /**
   * @var \Drupal\communication\CommunicationModePluginManager
   */
  protected $modeManager;

  /**
   * @var \Drupal\communication\Contact\ContactMiddlewareInterface
   */
  protected $contactMiddleware;

  /**
   * CommunicationController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   * @param \Drupal\Core\Render\RendererInterface $renderer
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   * @param \Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface $argument_resolver
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    EntityRepositoryInterface $entity_repository,
    RendererInterface $renderer,
    TranslationInterface $string_translation,
    UrlGeneratorInterface $url_generator,
    FormBuilderInterface $form_builder,
    ArgumentResolverInterface $argument_resolver,
    CommunicationModePluginManager $mode_manager,
    ContactMiddlewareInterface $contact_middleware = NULL
  ) {
    parent::__construct($entity_type_manager, $entity_type_bundle_info, $entity_repository, $renderer, $string_translation, $url_generator);

    $this->formBuilder = $form_builder;
    $this->argumentResolver = $argument_resolver;
    $this->modeManager = $mode_manager;
    $this->contactMiddleware = $contact_middleware;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity.repository'),
      $container->get('renderer'),
      $container->get('string_translation'),
      $container->get('url_generator'),
      $container->get('form_builder'),
      $container->get('http_kernel.controller.argument_resolver'),
      $container->get('plugin.manager.communication.mode'),
      $container->has('communication.contact_middleware') ? $container->get('communication.contact_middleware') : NULL
    );
  }

  /**
   * This differs from the add page in that it is designed to record communications
   * that have already happened.
   *
   * @param $entity_type_id
   */
  public function recordPage($entity_type_id) {
    $build = $this->addPage($entity_type_id);

    if (is_array($build)) {
      foreach ($build['#bundles'] as $bundle_name => &$bundle_link) {
        $bundle_link['add_link'] = Link::createFromRoute($bundle_link['label'], 'entity.communication.record_form', [
          'mode' => $bundle_name,
        ]);
      }
    }
    else {
      $bundles = $this->entityTypeBundleInfo->getBundleInfo($entity_type_id);
      return $this->redirect('entity.communication.record_form', [
        'mode' => key($bundles),
      ]);
    }

    return $build;
  }

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Form\EnforcedResponseException
   * @throws \Drupal\Core\Form\FormAjaxException
   */
  public function newCommunicationWithParticipantsForm(Request $request, RouteMatchInterface $route_match) {
    $mode_name = $route_match->getParameter('mode');
    /** @var \Drupal\communication\Plugin\Communication\Mode\ModeInterface $mode */
    $mode = $this->modeManager->createInstance($mode_name);

    // Allow the entity form to determine the entity object from a given route
    // match.
    $values = [
      'mode' => $mode_name,
    ];

    if (($participants = $request->get('participants', FALSE)) && is_array($participants)) {
      $participant_storage = $this->entityTypeManager->getStorage('communication_participant');

      foreach ($mode->participantRoles() as $role_name => $role_def) {
        if (!isset($participants[$role_name])) {
          continue;
        }

        if ($role_def->getCardinality() != 1) {
          $values[$role_def->getFieldName()] = [];
          $contacts = is_array($participants[$role_name]) ? $participants[$role_name] : [$participants[$role_name]];
          foreach ($contacts as $id) {
            $participant = $participant_storage->create([
              'type' => $role_def->getType(),
              'role' => $role_name,
            ]);
            $participant->contact = $this->defaultContactValueFromContactId($role_def , $id);
            $values[$role_def->getFieldName()][] = $participant;
          }
        }
        else {
          $id = is_array($participants[$role_name]) ? reset($participants[$role_name]) : $participants[$role_name];

          $participant = $participant_storage->create([
            'type' => $role_def->getType(),
            'role' => $role_name,
          ]);
          $participant->contact = $this->defaultContactValueFromContactId($role_def , $id);

          $values[$role_def->getFieldName()][] = $participant;
        }
      }
    }

    $entity = $this->entityTypeManager->getStorage('communication')
      ->create($values);

    $form_object = $this->entityTypeManager->getFormObject(
      'communication',
      $request->get('form_mode') ?: 'default'
    );

    $form_object->setEntity($entity);

    // Add the form and form_state to trick the getArguments method of the
    // controller resolver.
    $form_state = new FormState();
    $request->attributes->set('form', []);
    $request->attributes->set('form_state', $form_state);
    $args = $this->argumentResolver->getArguments($request, [$form_object, 'buildForm']);
    $request->attributes->remove('form');
    $request->attributes->remove('form_state');

    // Remove $form and $form_state from the arguments, and re-index them.
    unset($args[0], $args[1]);
    $form_state->addBuildInfo('args', array_values($args));

    return $this->formBuilder->buildForm($form_object, $form_state);
  }

  /**
   * @param \Drupal\communication\ParticipantRole $role
   * @param $id
   *
   * @return array|null
   */
  protected function defaultContactValueFromContactId(ParticipantRole $role, $id) {
    $contact = $this->contactMiddleware->loadContact($id);

    $type = $role->getTypePlugin();
    $contact_infos = $this->contactMiddleware->collectContactInfo($contact, $type->requiredContactInfoDefinition());
    foreach ($contact_infos as $contact_info) {
      if ($contact_info->isComplete()) {
        return [
          'target_id' => $contact->id(),
          'key' => $contact_info->key(),
        ];
      }
    }

    \Drupal::messenger()->addWarning(
      new TranslatableMarkup('@contact has no complete contact information for @role, please review.', [
        '@contact' => $contact->label(),
        '@role' => $role->getLabel(),
      ])
    );
    return $id;
  }

}
