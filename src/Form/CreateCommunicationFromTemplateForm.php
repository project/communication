<?php

namespace Drupal\communication\Form;

use Drupal\communication\Entity\Communication;
use Drupal\communication\OperationPluginManager;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_template_ui\Form\CreateEntityFromTemplateForm;
use Drupal\inline_entity_form\ElementSubmit;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CreateCommunicationFromTemplateForm
 *
 * @package Drupal\communication\Form
 */
class CreateCommunicationFromTemplateForm extends CreateEntityFromTemplateForm {
  use CommunicationOperationsFormTrait;

  /**
   * The operation manager.
   *
   * @var \Drupal\communication\OperationPluginManager
   */
  protected $operationManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return parent::create($container)->setOperationManager($container->get('plugin.manager.communication.operation'));
  }

  /**
   * Set the communication operation manager.
   *
   * @param \Drupal\communication\OperationPluginManager $operation_manager
   *
   * @return \Drupal\communication\Form\CreateCommunicationFromTemplateForm
   */
  public function setOperationManager(OperationPluginManager $operation_manager): CreateCommunicationFromTemplateForm {
    $this->operationManager = $operation_manager;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCommunication(FormStateInterface $form_state): Communication {
    return $form_state->get('current_entity');
  }

  /**
   * {@inheritdoc}
   */
  protected function defaultOptions(FormStateInterface $form_state) {
    return $form_state->get('current_default_options') ?: [];
  }

  protected function buildEntitiesForm(array $form, FormStateInterface $form_state) {
    $this->applyAjaxFormWrapper($form);
    if (!$form_state->get('operation')) {
      return parent::buildEntitiesForm($form, $form_state);
    }
    else {
      $form = $this->buildOperationForm($form, $form_state);
      $form['actions'] = [
        '#type' => 'actions',
      ] + $this->operationFormActions($form, $form_state);
      return $form;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function buildEntitiesFormActions(EntityInterface $entity, array $form, FormStateInterface $form_state) {
    /** @var \Drupal\communication\Entity\Communication $entity */
    $actions = parent::buildEntitiesFormActions($entity, $form, $form_state);

    $ajax_defaults = $this->ajaxDefaults();
    foreach ($this->operationManager->availableOperations($entity) as $id => $plugin) {
      $actions['op_' . $id] = [
        '#type' => 'submit',
        '#name' => 'op_' . $id,
        '#value' => $plugin->getPluginDefinition()['label'],
        '#submit' => [
          '::submitEntitiesForm',
          '::formSubmitOperationSelect',
        ],
        '#validate' => [
          '::validateEntitiesForm',
          '::formValidateOperationSelect',
        ],
        '#ajax' => $ajax_defaults,
        '#operation' => $id,
      ];
      ElementSubmit::addCallback($actions['op_'.$id], $form);
    }

    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function formSubmitPostOperationDo(array $form, FormStateInterface $form_state) {
    $form_state->set('operation', NULL);
    $form_state->set('operation_options', $this->defaultOptions($form_state));

    $this->submitEntitiesFormNextEntity($form, $form_state);
  }

}
