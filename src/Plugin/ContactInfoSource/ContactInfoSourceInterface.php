<?php

namespace Drupal\communication\Plugin\ContactInfoSource;

use Drupal\communication\Contact\ContactInfoDefinitionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\TypedData\DataDefinitionInterface;

interface ContactInfoSourceInterface {

  /**
   * Collect contact info for the entity that matches.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param \Drupal\communication\Contact\ContactInfoDefinitionInterface $definition
   * @param array $options
   *
   * @return \Drupal\communication\Contact\ContactInfoInterface[]
   */
  public function collectInfo(EntityInterface $entity, ContactInfoDefinitionInterface $definition, array $options = []);

  /**
   * @return mixed
   */
  public function getInfoValue(EntityInterface $entity, $key, $name, DataDefinitionInterface $definition);

  /**
   * Find out whether changes to this piece of contact info can be written back
   * to the source.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param $key
   * @param $name
   * @param \Drupal\Core\TypedData\DataDefinitionInterface $definition
   *
   * @return boolean
   */
  public function supportsWriteBackInfoValue(EntityInterface $entity, $key, $name, DataDefinitionInterface $definition);

  /**
   * Write back a value to the source.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param $key
   * @param \Drupal\Core\TypedData\DataDefinitionInterface $definition
   * @param $values
   *
   * @return string
   *   The key of the contact info. Write back can change this key.
   */
  public function writeBackInfoValues(EntityInterface $entity, $key, DataDefinitionInterface $definition, $values);

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param $key
   *
   * @return string
   */
  public function getLabel(EntityInterface $entity, $key);

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param $key
   *
   * @return mixed
   */
  public function getSummary(EntityInterface $entity, $key);

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param $key
   *
   * @return \Drupal\communication\Contact\ContactInfoDefinitionInterface
   */
  public function getInfoDefinition(EntityInterface $entity, $key);
}
