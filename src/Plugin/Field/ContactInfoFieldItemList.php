<?php

namespace Drupal\communication\Plugin\Field;

use Drupal\communication\Contact\ContactInfoDefinition;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

class ContactInfoFieldItemList extends FieldItemList {
  use ComputedItemListTrait;

  /**
   * Computes the values for an item list.
   */
  protected function computeValue() {
    $contact = $this->getEntity();
    if (!$contact) {
      return;
    }

    $contact_info_data_definition = $this->getFieldDefinition()
      ->getSetting('contact_info_data_definition');
    if (is_string($contact_info_data_definition)) {
      $contact_info_data_definition = ContactInfoDefinition::create($contact_info_data_definition);
    }

    /** @var \Drupal\communication\Contact\ContactMiddlewareInterface $middleware */
    $middleware = \Drupal::service('communication.contact_middleware');
    $contact_infos = $middleware->collectContactInfo($contact, $contact_info_data_definition);

    $delta = 0;
    foreach ($contact_infos as $key => $contact_info) {
      if ($contact_info->isComplete()) {
        $this->list[$delta] = $this->createItem($delta, [
          'key' => $key,
          'info' => $contact_info,
        ]);
        $delta++;
      }
    }
  }
}
