<?php

namespace Drupal\communication_subscription\Entity;

use Drupal\communication\Entity\CommunicationEvent;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\EntityOwnerInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Class Subscription
 *
 * @ContentEntityType(
 *   id = "communication_subscription",
 *   label = @Translation("Communication Subscription"),
 *   bundle_label = @Translation("Notification Method"),
 *   handlers = {
 *     "storage" = "Drupal\Core\Entity\Sql\SqlContentEntityStorage",
 *     "access" = "Drupal\communication_subscription\Entity\SubscriptionAccessControlHandler",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *   },
 *   base_table = "communication_subscription",
 *   admin_permission = "administer communication subscription",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "method",
 *     "uuid" = "uuid",
 *     "owner" = "user",
 *   },
 *   bundle_plugin_type = "communication_subscription_notification_method",
 *   links = {
 *     "canonical" = "/api/communication_subscription/{communication_subscription}",
 *   }
 * )
 * @package Drupal\communication_subscription
 */
class Subscription extends ContentEntityBase implements EntityOwnerInterface {
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    // If left blank all communications that the user has access to view
    // will be subscribed to.
    $fields['communication'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Communication'))
      ->setDescription(new TranslatableMarkup('The communication subscribed to.'))
      ->setSetting('target_type', 'communication')
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_label',
      ])
      ->setDisplayConfigurable('form', TRUE);

    // If left blank all events
    $fields['events'] = BaseFieldDefinition::create('list_string')
      ->setLabel(new TranslatableMarkup('Events'))
      ->setDescription(new TranslatableMarkup('The events subscribed to.'))
      ->setSetting('allowed_values_function', [CommunicationEvent::class, 'eventTypeOptions'])
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
      ])
      ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }

  /**
   * Get the notification method.
   *
   * @return \Drupal\communication_subscription\Plugin\Communication\NotificationMethod\NotificationMethodInterface
   */
  public function getNotificationMethod() {
    /** @var \Drupal\communication_subscription\Plugin\Communication\NotificationMethod\NotificationMethodInterface $plugin */
    $plugin = \Drupal::service('plugin.manager.communication_subscription_notification_method')
      ->createInstance($this->bundle());

    return $plugin;
  }

}
