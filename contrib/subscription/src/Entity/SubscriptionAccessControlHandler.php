<?php

namespace Drupal\communication_subscription\Entity;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Class SubscriptionAccessControlHandler
 *
 * @package Drupal\communication_subscription\Entity
 */
class SubscriptionAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\communication_subscription\Entity\Subscription $entity */
    $access =  parent::checkAccess($entity, $operation, $account);

    if ($operation === 'edit') {
      $operation = 'update';
    }

    $access = $access->orIf(
      AccessResult::allowedIfHasPermission($account, $operation.' any communication subscription')
    );

    $access = $access->orIf(
      AccessResult::allowedIfHasPermission($account, $operation.' own communication subscription')
        ->andIf(
          AccessResult::allowedIf($account->id() === $entity->getOwnerId())
            ->cachePerUser()
            ->addCacheableDependency($entity)
        )
    );

    return $access;
  }

}
