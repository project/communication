<?php

namespace Drupal\communication\Entity;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

class CommunicationEventAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  public function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    // Defer to the communication.
    /** @var \Drupal\communication\Entity\Communication $communication */
    if ($communication = $entity->communication->entity) {
      return $communication->access($operation, $account, TRUE);
    }

    return parent::checkAccess($entity, $operation, $account);
  }
}
