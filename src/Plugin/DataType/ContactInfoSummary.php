<?php

namespace Drupal\communication\Plugin\DataType;

use Drupal\Core\TypedData\Plugin\DataType\StringData;

/**
 * Class ContactInfoLabel
 *
 * @DataType(
 *   id = "contact_info_summary",
 *   label = @Translation("Contact Info Summary"),
 * )
 *
 * @package Drupal\communication\Plugin\DataType
 */
class ContactInfoSummary extends StringData {

  /**
   * Compute the summary.
   *
   * @return string
   */
  public function getValue() {
    /** @var \Drupal\communication\Contact\ContactInfoInterface $contact_info */
    $contact_info = ($this->parent instanceof ContactInfoData) ? $this->parent->getValue() : $this->parent->get('info')->getValue();

    return $contact_info->summary();
  }

}
