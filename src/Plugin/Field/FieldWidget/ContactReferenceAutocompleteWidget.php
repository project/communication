<?php

namespace Drupal\communication\Plugin\Field\FieldWidget;

use Drupal\communication\Ajax\SetFormValueCommand;
use Drupal\communication\Ajax\SetReadOnlyCommand;
use Drupal\communication\Contact\ContactInfo;
use Drupal\communication\Contact\ContactMiddlewareInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InsertCommand;
use Drupal\Core\Ajax\PrependCommand;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;
use Drupal\Core\Field\WidgetPluginManager;
use Drupal\Core\Form\FormAjaxResponseBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ContactReferenceAutocompleteWidget
 *
 * @FieldWidget(
 *   id = "contact_reference_autocomplete",
 *   label = @Translation("Autocomplete"),
 *   description = @Translation("An autocomplete text field."),
 *   field_types = {
 *     "contact_reference"
 *   }
 * )
 *
 * @package Drupal\communication\Plugin\Field\FieldWidget
 */
class ContactReferenceAutocompleteWidget extends EntityReferenceAutocompleteWidget implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\communication\Contact\ContactMiddlewareInterface
   */
  protected $contactMiddleware;

  /**
   * @var \Drupal\Core\Field\WidgetPluginManager
   */
  protected $widgetManager;

  /**
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('communication.contact_middleware'),
      $container->get('plugin.manager.field.widget'),
      $container->get('entity_display.repository'),
      $container->get('renderer')
    );
  }

  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    ContactMiddlewareInterface $contact_middleware,
    WidgetPluginManager $widget_manager,
    EntityDisplayRepositoryInterface $entity_display_repository,
    RendererInterface $renderer
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings, $renderer);
    $this->contactMiddleware = $contact_middleware;
    $this->widgetManager = $widget_manager;
    $this->entityDisplayRepository = $entity_display_repository;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'new_contact' => 1,
        'new_contact_form_modes' => [
          'default',
        ],
      ] + parent::defaultSettings();
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    $element['new_contact'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow Creation of New Contacts'),
      '#default_value' => $this->getSetting('new_contact'),
    ];

    $element['new_contact_form_modes'] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#options' => $this->entityDisplayRepository->getFormModeOptions($this->contactMiddleware->contactEntityTypeId()),
      '#title' => $this->t('New Contact Form Mode'),
      '#default_value' => $this->getSetting('new_contact_form_mode'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $field_parents = !empty($element['#field_parents']) ? $element['#field_parents'] : [];

    $elements = parent::formElement($items, $delta, $element, $form, $form_state);

    $widget_state = static::getWidgetState($field_parents, $items->getFieldDefinition()->getName(), $form_state);
    if (!isset($widget_state[$delta]['contact_entity']) && $items->get($delta)->entity) {
      $widget_state[$delta]['contact_entity'] = $items->get($delta)->entity;
    }
    if (!isset($widget_state[$delta]['contact_key']) && $items->get($delta)->key) {
      $widget_state[$delta]['contact_key'] = $items->get($delta)->key;
    }
    static::setWidgetState($field_parents, $items->getFieldDefinition()->getName(), $form_state, $widget_state);

    $parents = $field_parents;
    $parents[] = $items->getFieldDefinition()->getName();
    $parents[] = $delta;
    $wrapper_id = implode('--', $parents).'--key-wrapper';

    $button_parents = $parents;
    $button_parents[] = 'target_id_submit';
    $button_name = array_shift($button_parents).'['.implode('][', $button_parents).']';

    $key_button_parents = $parents;
    $key_button_parents[] = 'key_submit';
    $key_button_name = array_shift($key_button_parents).'['.implode('][', $key_button_parents).']';

    $parents[] = 'target_id';
    $elements['#attached']['library'][] = 'communication/contactreference.widget';
    $elements['target_id_submit'] = [
      '#type' => 'submit',
      '#value' => new TranslatableMarkup('Select Contact'),
      '#name' => $button_name,
      '#entity' => $items->getEntity(),
      '#delta' => $delta,
      '#field_parents' => $field_parents,
      '#field_name' => $this->fieldDefinition->getName(),
      '#validate' => [],
      '#submit' => [
        [$this, 'formElementSubmitSelectContact'],
      ],
      '#limit_validation_errors' => [ $parents ],
      '#ajax' => [
        'callback' => [$this, 'formElementAjaxSelectContact'],
        'wrapper' => $wrapper_id,
      ],
      '#attributes' => [
        'class' => ['js-hide'],
      ]
    ];
    $elements['target_id']['#ajax'] = [
      'trigger_as' => ['name' => $button_name],
      'callback' => [$this, 'formElementAjaxSelectContact'],
      'wrapper' => $wrapper_id,
      'method' => 'replace',
      'effect' => 'fade',
      'event' => 'autocompleteclose',
    ];

    $name_parents = $parents;
    $modes = array_filter($this->getSetting('new_contact_form_modes') ?: ['default']);
    if (count($modes) == 1) {
      $elements['target_id_new'] = [
        '#type' => 'link',
        '#title' => $this->t('New Contact'),
        '#url' => Url::fromRoute('communication.contact.new', [], [
          'query' => [
            'eren' => array_shift($name_parents) . '[' . implode('][', $name_parents) . ']',
            'erfi' => '.' . implode('.', $form_state->getCompleteForm()['#attributes']['class']),
            'form_mode' => reset($modes),
          ],
        ]),
        '#attributes' => [
          'class' => ['use-ajax', 'button'],
          'data-dialog-type' => 'modal',
          'data-dialog-options' => Json::encode([
            'width' => 800,
            'height' => 500
          ]),
        ],
        '#attached' => [
          'library' => [
            'core/drupal.dialog.ajax',
            'communication/ajax.commands'
          ],
        ],
        '#access' => $this->getSetting('new_contact'),
      ];
    }
    else {
      $elements['target_id_new'] = [
        '#type' => 'dropbutton',
        '#links' => [
        ],
        '#attached' => [
          'library' => [
            'core/drupal.dialog.ajax',
            'communication/ajax.commands'
          ],
        ],
        '#access' => $this->getSetting('new_contact'),
      ];

      $mode_labels = $this->entityDisplayRepository->getFormModeOptions($this->contactMiddleware->contactEntityTypeId());
      foreach ($modes as $mode) {
        $complete_form = $form_state->getCompleteForm();
        $elements['target_id_new']['#links'][$mode] = [
          'title' => $mode_labels[$mode],
          'url' => Url::fromRoute('communication.contact.new', [], [
            'query' => [
              'eren' => array_shift($name_parents) . '[' . implode('][', $name_parents) . ']',
              'erfi' => !empty($complete_form['#attributes']['class']) ? '.' . implode('.', $complete_form['#attributes']['class']) : '',
              'form_mode' => $mode,
            ],
          ]),
          'attributes' => [
            'class' => ['use-ajax', 'button'],
            'data-dialog-type' => 'modal',
            'data-dialog-options' => Json::encode([
              'width' => 800,
              'height' => 500
            ]),
          ]
        ];
      }
    }

    $elements['key_container'] = [
      '#type' => 'container',
      '#id' => $wrapper_id,
      '#attached' => [
        'library' => [
          'communication/ajax.commands',
        ]
      ],
      'key_submit' => [
        '#type' => 'markup',
        '#markup' => '',
      ]
    ];

    if (!empty($widget_state[$delta]['contact_entity'])) {
      $contact = $widget_state[$delta]['contact_entity'];

      $key_options = [];

      /** @var \Drupal\communication\Contact\ContactInfoEntitySyncableInterface $participant */
      $participant = $items->getEntity();
      try {
        $collected_info = $this->contactMiddleware->collectContactInfo($contact, $participant->requiredContactInfoDefinition());
      }
      catch(\Exception $e) {
        $collected_info = [];
      }
      foreach ($collected_info as $key => $contact_info) {
        $key_options[$key] = $contact_info->label();

        if (!$contact_info->isComplete()) {
          $key_options[$key] .= " *";
        }
      }

      array_pop($parents); array_push($parents, 'key');
      $key_submit_parents = $parents;
      array_pop($key_submit_parents); array_push($key_submit_parents, 'submit');
      $elements['key_container']['key_submit'] = [
        '#type' => 'submit',
        '#value' => new TranslatableMarkup('Select'),
        '#name' => $key_button_name,
        '#delta' => $delta,
        '#parents' => $key_submit_parents,
        '#field_parents' => $field_parents,
        '#field_name' => $this->fieldDefinition->getName(),
        '#entity' => $participant,
        '#collected_info' => $collected_info,
        '#attached' => [
          'library' => [
            'communication/ajax.commands',
          ]
        ],
        '#submit' => [
          [$this, 'formElementSubmitSelectKey'],
        ],
        '#validate' => [],
        '#limit_validation_errors' => [ $parents ],
        '#ajax' => [
          'wrapper' => $wrapper_id,
          'callback' => [$this, 'formElementAjaxSelectKey'],
        ],
        '#attributes' => [
          'class' => ['js-hide'],
        ]
      ];

      $elements['key_container']['key'] = [
        '#type' => 'select',
        '#options' => $key_options,
        '#parents' => $parents,
        '#empty_option' => new TranslatableMarkup('Custom'),
        '#default_value' => isset($key_options[$items->get($delta)->key]) ? $items->get($delta)->key : NULL,
        '#ajax' => [
          'callback' => [$this, 'formElementAjaxSelectKey'],
          'trigger_as' => ['name' => $key_button_name],
          'wrapper' => $wrapper_id,
        ],
      ];

      array_pop($parents); array_push($parents, 'metadata');
      $elements['key_container']['metadata'] = [
        '#type' => 'container',
        '#parents' => $parents,
      ];
      if (!empty($widget_state[$delta]['contact_key'])) {
        $selected_key = $widget_state[$delta]['contact_key'];
        $contact_info = !empty($widget_state[$delta]['contact_info']) ? $widget_state[$delta]['contact_info'] : ContactInfo::create($contact, $selected_key);
        $elements['key_container']['metadata'] += $contact_info->metadataForm($elements['key_container']['metadata'], $form_state);
      }
    }

    static::setWidgetState($field_parents, $items->getFieldDefinition()->getName(), $form_state, $widget_state);
    return $elements;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function formElementSubmitSelectContact(array $form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $parents = $triggering_element['#parents'];
    array_pop($parents); array_push($parents, 'target_id');
    $selected_contact_id = $form_state->getValue($parents);

    $field_parents = $triggering_element['#field_parents'];
    $widget_state = static::getWidgetState($field_parents, $this->fieldDefinition->getName(), $form_state);
    $widget_state[$triggering_element['#delta']]['contact_entity'] = $this->contactMiddleware->loadContact($selected_contact_id);
    static::setWidgetState($field_parents, $this->fieldDefinition->getName(), $form_state, $widget_state);

    $input = &$form_state->getUserInput();
    $key_parents = $field_parents;
    $key_parents[] = $triggering_element['#field_name'];
    $key_parents[] = $triggering_element['#delta'];
    $key_parents[] = 'key';
    NestedArray::unsetValue($input, $key_parents);

    $form_state->setRebuild(TRUE);
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return mixed
   */
  public function formElementAjaxSelectContact(array $form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $array_parents = $triggering_element['#array_parents'];
    array_pop($array_parents); array_push($array_parents, 'key_container');

    $response = new AjaxResponse();

    $section = NestedArray::getValue($form, $array_parents);
    $section_html = $this->renderer->renderRoot($section);
    $response->setAttachments($section['#attached']);

    // The selector for the insert command is NULL as the new content will
    // replace the element making the Ajax call. The default 'replaceWith'
    // behavior can be changed with #ajax['method'].
    $response->addCommand(new InsertCommand(NULL, $section_html));

    // Make sure that participant values get cleared.
    /** @var \Drupal\Core\Entity\ContentEntityInterface $participant */
    $participant = clone $triggering_element['#entity'];
    $field_definitions = $participant->getFieldDefinitions();

    $array_parents = $triggering_element['#array_parents'];
    $field_array_parents = array_slice($array_parents, 0, -4);
    $form_section = NestedArray::getValue($form, $field_array_parents);
    // This is a custom piece of information so make sure the elements are not read only.
    foreach (Element::children($form_section) as $child) {
      if (!($field_definitions[$child] && !in_array($child, ['contact', 'role', 'communication']))) {
        continue;
      }

      foreach (Element::children($form_section[$child]['widget']) as $delta) {
        foreach (Element::children($form_section[$child]['widget'][$delta]) as $property) {
          $response->addCommand(new SetReadOnlyCommand(
            $form_section[$child]['widget'][$delta][$property]['#name'],
            FALSE
          ));
          $response->addCommand(new SetFormValueCommand(
            $form_section[$child]['widget'][$delta][$property]['#name'],
            NULL
          ));
        }
      }
    }

    $status_messages = ['#type' => 'status_messages'];
    $output = $this->renderer->renderRoot($status_messages);
    if (!empty($output)) {
      $response->addCommand(new PrependCommand(NULL, $output));
    }

    return $response;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function formElementSubmitSelectKey(array $form, FormStateInterface $form_state) {
    $element = $form_state->getTriggeringElement();

    $parents = $element['#parents'];
    array_pop($parents); array_push($parents, 'key');
    $selected_key = $form_state->getValue($parents);

    $widget_state = static::getWidgetState($element['#field_parents'], $element['#field_name'], $form_state);
    $widget_state[$element['#delta']]['contact_key'] = $selected_key;
    static::setWidgetState($element['#field_parents'], $element['#field_name'], $form_state, $widget_state);

    $form_state->setRebuild(TRUE);
  }

  /**
   * Form ajax select key.
   *
   * When a contact info is selected we want to populate the values on the participant
   * automatically. We do this by generating a new widget and looping through the elements,
   * setting the values accordingly with javascript.
   *
   * @todo: We want to make sure that values are written back where possible.
   * @todo: Where it is not possible to write back values, we want to disable the
   *        field.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function formElementAjaxSelectKey(array $form, FormStateInterface $form_state) {
    $element = $form_state->getTriggeringElement();
    $response = new AjaxResponse();

    $parents = $element['#parents'];
    array_pop($parents); array_push($parents, 'key');
    $selected_key = $form_state->getValue($parents);

    // Get the metadata section.
    $array_parents = $element['#array_parents'];
    array_pop($array_parents);
    $metadata_section = NestedArray::getValue($form, $array_parents);
    $metadata_section_html = $this->renderer->renderRoot($metadata_section);
    $response->setAttachments($metadata_section['#attached']);

    // The selector for the insert command is NULL as the new content will
    // replace the element making the Ajax call. The default 'replaceWith'
    // behavior can be changed with #ajax['method'].
    $response->addCommand(new InsertCommand(NULL, $metadata_section_html));

    $array_parents = $element['#array_parents'];
    $field_array_parents = array_slice($array_parents, 0, -5);
    $form_section = NestedArray::getValue($form, $field_array_parents);

    /** @var \Drupal\Core\Entity\ContentEntityInterface $participant */
    $participant = clone $element['#entity'];
    $field_definitions = $participant->getFieldDefinitions();

    /** @var \Drupal\communication\Contact\ContactInfoInterface $contact_info */
    if ($contact_info = $element['#collected_info'][$selected_key]) {
      $this->contactMiddleware->populateParticipant($participant, $contact_info);

      foreach (Element::children($form_section) as $child) {
        if (!($field_definitions[$child] && !in_array($child, ['contact', 'role', 'communication']))) {
          continue;
        }

        $widget = $this->widgetManager->getInstance([
          'field_definition' => $field_definitions[$child],
          'form_mode' => $form_section['#form_mode'],
        ]);
        $field_element = $widget->form($participant->get($child), $form_section, $form_state);

        // @todo: This may not work with address fields
        foreach (Element::children($form_section[$child]['widget']) as $delta) {
          foreach (Element::children($form_section[$child]['widget'][$delta]) as $property) {
            $response->addCommand(new SetFormValueCommand($form_section[$child]['widget'][$delta][$property]['#name'], $field_element['widget'][$delta][$property]['#default_value']));
            $response->addCommand(new SetReadOnlyCommand(
              $form_section[$child]['widget'][$delta][$property]['#name'],
              !$contact_info->supportsWriteBackInfoValue($child, $field_definitions[$child]->getItemDefinition())
            ));
          }
        }
      }
    }
    else {
      // This is a custom piece of information so make sure the elements are not read only.
      foreach (Element::children($form_section) as $child) {
        if (!($field_definitions[$child] && !in_array($child, ['contact', 'role', 'communication']))) {
          continue;
        }

        foreach (Element::children($form_section[$child]['widget']) as $delta) {
          foreach (Element::children($form_section[$child]['widget'][$delta]) as $property) {
            $response->addCommand(new SetReadOnlyCommand(
              $form_section[$child]['widget'][$delta][$property]['#name'],
              FALSE
            ));
          }
        }
      }
    }

    return $response;
  }

}
