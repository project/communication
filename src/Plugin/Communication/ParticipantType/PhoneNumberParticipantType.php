<?php

namespace Drupal\communication\Plugin\Communication\ParticipantType;

use Drupal\communication\Contact\ContactInfoDefinition;
use Drupal\communication\Entity\CommunicationParticipant;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\entity\BundleFieldDefinition;

/**
 * Class EmailParticipantType
 *
 * @CommunicationParticipantType(
 *   id = "phone",
 *   label = "Phone Number",
 * )
 *
 * @package Drupal\communication\Plugin\CommunicationParticipantType
 */
class PhoneNumberParticipantType extends ParticipantTypeBase {

  /**
   * Get communication participant fields required for this type.
   *
   * @param \Drupal\Core\Field\BaseFieldDefinition[] $base_field_definitions
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   */
  public function communicationParticipantFieldDefinitions(array $base_field_definitions) {
    $fields = parent::communicationParticipantFieldDefinitions($base_field_definitions);

    $fields['number'] = BundleFieldDefinition::create('telephone')
      ->setLabel(new TranslatableMarkup('Tel. Number'))
      ->setDescription(new TranslatableMarkup('The phone number of this participant.'))
      ->setRequired(TRUE)
      ->setProvider('communication')
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'basic_string',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'telephone_default',
      ]);

    return $fields;
  }

  /**
   * Get the participant label.
   *
   * @param \Drupal\communication\Entity\CommunicationParticipant $participant
   *
   * @return string|\Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public function participantLabel(CommunicationParticipant $participant) {
    if ($participant->name->isEmpty()) {
      return $participant->number->value ?: new TranslatableMarkup('INCOMPLETE');
    }
    else {
      return new TranslatableMarkup("@name (@number)", [
        '@name' => $participant->name->value,
        '@number' => $participant->number->value ?: new TranslatableMarkup('INCOMPLETE'),
      ]);
    }
  }

  /**
   * The required contact info definition.
   *
   * @return \Drupal\communication\Contact\ContactInfoDefinitionInterface
   */
  public function requiredContactInfoDefinition() {
    return ContactInfoDefinition::create('telephone');
  }
}
