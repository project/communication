<?php
/**
 * Created by PhpStorm.
 * User: Rob
 * Date: 23/05/2019
 * Time: 14:43
 */

namespace Drupal\communication\Plugin\views\relationship;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Plugin\views\relationship\RelationshipPluginBase;
use Drupal\views\Views;

/**
 * Class CommunicationLatestEventRelationship
 *
 * @ViewsRelationship("communication_latest_event")
 *
 * @package Drupal\communication\Plugin\views\relationship
 */
class CommunicationLatestEventRelationship extends RelationshipPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['event_type'] = ['default' => 'sent'];
    $options['reduce_duplication'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $type_options = [];
    foreach (\Drupal::service('entity_type.bundle.info')->getBundleInfo('communication_event') as $bundle => $bundle_info) {
      $type_options[$bundle] = $bundle_info['label'];
    }
    $form['event_type'] = [
      '#type' => 'select',
      '#title' => new TranslatableMarkup('Event Type'),
      '#options' => $type_options,
      '#required' => TRUE,
      '#default_value' => $this->options['event_type'],
    ];

    $form['reduce_duplication'] = [
      '#type' => 'checkbox',
      '#title' => new TranslatableMarkup('Reduce Duplication'),
      '#description' => new TranslatableMarkup('Some events can occur more than once on a communication, to reduce duplication in this case tick the box.'),
      '#default_value' => $this->options['reduce_duplication'],
    ];

    parent::buildOptionsForm($form, $form_state);
  }

  public function query() {
    if ($this->options['reduce_duplication']) {
      $this->ensureMyTable();

      $query = \Drupal::database()->select('communication_event', 'ce');
      $query->addField('ce', 'id', 'event_id');
      $query->where("{$this->tableAlias}.id = ce.communication");
      $query->where("ce.type = '{$this->definition['event type']}'");
      $query->orderBy('ce.date', 'DESC');
      $query->range(0, 1);

      $def = [];
      $def['table'] = $this->definition['base'];
      $def['field'] = 'id';
      $def['left_table'] = $this->tableAlias;
      $def['left_field'] = $this->realField;
      $def['adjusted'] = TRUE;
      if (!empty($this->options['required'])) {
        $def['type'] = 'INNER';
      }
      $def['left_query'] = $query->__toString();

      $join = Views::pluginManager('join')->createInstance('subquery', $def);

      // use a short alias for this:
      $alias = $def['table'] . '_' . $this->table;
      $this->alias = $this->query->addRelationship($alias, $join, $this->definition['base'], $this->relationship);
    }
    else {
      parent::query();
    }
  }
}
