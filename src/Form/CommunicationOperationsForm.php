<?php

namespace Drupal\communication\Form;

use Drupal\communication\Entity\Communication;
use Drupal\communication\OperationPluginManager;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CommunicationOperationsForm
 *
 * @package Drupal\communication\Form
 */
class CommunicationOperationsForm extends FormBase {
  use CommunicationOperationsFormTrait;

  /**
   * @var \Drupal\communication\OperationPluginManager
   */
  protected $operationManager;

  /**
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * @var \Drupal\communication\Entity\Communication
   */
  protected $communication;

  /**
   * @var array
   */
  protected $options = [
    'exclude' => [],
    'configuration' => [],
  ];

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.communication.operation'),
      $container->get('renderer')
    );
  }

  /**
   * CommunicationOperationsForm constructor.
   *
   * @param \Drupal\communication\OperationPluginManager $operation_plugin_manager
   */
  public function __construct(OperationPluginManager $operation_plugin_manager, RendererInterface $renderer) {
    $this->operationManager = $operation_plugin_manager;
    $this->renderer = $renderer;
  }

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'communication_operations_form';
  }

  /**
   * {@inheritdoc}}
   */
  protected function getCommunication(): Communication {
    return $this->communication;
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\communication\Entity\Communication $communication
   *   The communication entity to perform the operation on.
   * @param array $options
   *   Options governing the behaviour of this form. Has the following keys:
   *   - exclude: A list of operation ids. If an operation id appears in this list
   *              it will be excluded from the operation options.
   *   - configuration: Default configuration to provide to the operation.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state, Communication $communication = NULL, array $options = []) {
    $this->communication = $communication;
    $this->options = $options + $this->options;
    $this->applyAjaxFormWrapper($form);

    $ajax_defaults = [
      'callback' => [$this, 'formAjax'],
      'wrapper' => 'communication-operations-form',
      'method' => 'replace',
    ];

    if (!$form_state->get('operation')) {
      $form['operations'] = [
        '#type' => 'container',
      ];

      /** @var \Drupal\communication\Plugin\Communication\Operation\OperationInterface $plugin */
      foreach ($this->operationManager->availableOperations($communication, $this->options) as $id => $plugin) {
        $form['operations']['do_'.$id] = [
          '#type' => 'submit',
          '#name' => 'do_'.$id,
          '#value' => $plugin->getPluginDefinition()['label'],
          '#submit' => [
            '::formSubmitOperationSelect',
          ],
          '#validate' => [
            '::formValidateOperationSelect',
          ],
          '#ajax' => $ajax_defaults,
          '#operation' => $id,
        ];
      }
    }
    else {
      $form = $this->buildOperationForm($form, $form_state);

      $operation_id = $form_state->get('operation');
      /** @var \Drupal\communication\Plugin\Communication\Operation\OperationInterface $operation */
      $operation = $this->operationManager->createInstance($operation_id);
      $form = $operation->form($form, $form_state, $this->getCommunication($form_state));

      $form['actions'] = [
        '#type' => 'actions',
      ];
      $form['actions'] += $this->operationFormActions($form, $form_state);
    }

    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // TODO: Implement submitForm() method.
  }




}
