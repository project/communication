<?php

namespace Drupal\communication\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CommunicationModeNewActions extends DeriverBase implements ContainerDeriverInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * CommunicationModeNewActions constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   */
  public function __construct(EntityTypeBundleInfoInterface $entity_type_bundle_info) {
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    foreach ($this->entityTypeBundleInfo->getBundleInfo('communication') as $name => $info) {
      $definition = $base_plugin_definition;
      $definition['title'] = new TranslatableMarkup('New @mode', ['@mode' => $info['label']]);
      $definition['route_name'] = 'entity.communication.add_form';
      $definition['appears_on'] = ['view.communication_list.page'];
      $definition['route_parameters'] = [
        'mode' => $name,
      ];

      $this->derivatives[$name] = $definition;
    }

    return $this->derivatives;
  }

}
