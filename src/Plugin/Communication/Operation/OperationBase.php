<?php

namespace Drupal\communication\Plugin\Communication\Operation;

use Drupal\communication\Entity\CommunicationInterface;
use Drupal\communication\Event\CommunicationEvents;
use Drupal\communication\Event\CommunicationOperationEvent;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class CommunicationOperationBase
 *
 * @package Drupal\communication\Plugin\Communication\Operation
 */
abstract class OperationBase extends PluginBase implements OperationInterface, ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('event_dispatcher'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * OperationBase constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ModuleHandlerInterface $module_handler,
    EventDispatcherInterface $event_dispatcher,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->moduleHandler = $module_handler;
    $this->eventDispatcher = $event_dispatcher;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Get communication event storage.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function eventStorage() {
    return $this->entityTypeManager->getStorage('communication_event');
  }

  /**
   * Run just before the operation fires.
   *
   * @param \Drupal\communication\Entity\CommunicationInterface $communication
   * @param array $options
   */
  protected function preRun(CommunicationInterface $communication, array &$options) {
    $this->moduleHandler->invokeAll('pre_communication_operation', [$communication, $options, $this->getPluginId()]);
    $this->moduleHandler->invokeAll('pre_communication_'.$this->getPluginId(), [$communication, $options]);

    $event = new CommunicationOperationEvent($communication, $this->getPluginId(), $options);
    $this->eventDispatcher->dispatch($event, CommunicationEvents::PRE_OPERATION);

    $options = $event->getOptions();
  }

  /**
   * {@inheritdoc}
   */
  public function applicable(CommunicationInterface $communication, array $options = []) {
    if (!in_array($communication->mode->value, $this->pluginDefinition['modes']) && !empty($this->pluginDefinition['modes'])) {
      return FALSE;
    }

    if (in_array($communication->mode->value, $this->pluginDefinition['exclude_modes'])) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function formPostOperation(array $form, FormStateInterface $form_state, CommunicationInterface $communication) { }

  /**
   * Return the operations section of the form
   *
   * @param array $form
   *   The complete form.
   * @param FormStateInterface $form_state
   *   The form status object
   *
   * @return array
   *   A section of the form to return.
   */
  public function formAjaxRefreshCallback(array $form, FormStateInterface $form_state) {
    $triggerring_element = $form_state->getTriggeringElement();
    $array_parents = $triggerring_element['#array_parents'];
    $key = array_search('operations', $array_parents);
    $sub_parents = array_slice($array_parents, 0, $key);

    return NestedArray::getValue($form, $sub_parents);
  }
}
