<?php

namespace Drupal\communication\Controller;

use Drupal\communication\Contact\ContactMiddlewareInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormState;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface;

class NewContactFormController extends ControllerBase {

  /**
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * @var \Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface
   */
  protected $argumentResolver;

  /**
   * @var \Drupal\communication\Contact\ContactMiddlewareInterface
   */
  protected $contactMiddleware;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('communication.contact_middleware'),
      $container->get('form_builder'),
      $container->get('http_kernel.controller.argument_resolver')
    );
  }

  /**
   * NewContactFormController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\communication\Contact\ContactMiddlewareInterface $contact_middleware
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   * @param \Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface $argument_resolver
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ContactMiddlewareInterface $contact_middleware,
    FormBuilderInterface $form_builder,
    ArgumentResolverInterface $argument_resolver
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->contactMiddleware = $contact_middleware;
    $this->formBuilder = $form_builder;
    $this->argumentResolver = $argument_resolver;
  }

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return array
   * @throws \Drupal\Core\Form\EnforcedResponseException
   * @throws \Drupal\Core\Form\FormAjaxException
   */
  public function getContentResult(Request $request) {
    $form_object = $this->entityTypeManager->getFormObject(
      $this->contactMiddleware->contactEntityTypeId(),
      $request->get('form_mode') ?: 'default'
    );

    // Allow the entity form to determine the entity object from a given route
    // match.
    $values = [];
    if (\Drupal::request()->get('contact_bundle')) {
      $values['bundle'] = \Drupal::request()->get('contact_bundle');
    }
    $entity = $this->contactMiddleware->createContact($values);
    $form_object->setEntity($entity);

    // Add the form and form_state to trick the getArguments method of the
    // controller resolver.
    $form_state = new FormState();
    $request->attributes->set('form', []);
    $request->attributes->set('form_state', $form_state);
    $args = $this->argumentResolver->getArguments($request, [$form_object, 'buildForm']);
    $request->attributes->remove('form');
    $request->attributes->remove('form_state');

    $form_state->set('entity_reference_element_name', $request->get('eren'));
    $form_state->set('entity_reference_form_id', $request->get('erfi') ?: NULL);
    $form_state->set('entity_reference_form_build_id', $request->get('erfbi') ?: NULL);
    $form_state->set('entity_reference_fill_result', TRUE);

    // Remove $form and $form_state from the arguments, and re-index them.
    unset($args[0], $args[1]);
    $form_state->addBuildInfo('args', array_values($args));

    return $this->formBuilder->buildForm($form_object, $form_state);
  }

}
