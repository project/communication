<?php

namespace Drupal\communication\Plugin\Communication\Mode;
use Drupal\communication\ParticipantRole;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\entity\BundleFieldDefinition;

/**
 * Class SMS
 *
 * @CommunicationMode(
 *   id = "sms",
 *   label = "SMS",
 *   forms = {
 *     "configure" = "Drupal\communication\PluginForm\ModeConfigureForm"
 *   }
 * )
 *
 * @package Drupal\communication\Plugin\Communication\Mode
 */
class SMS extends TextContentModeBase {

  /**
   * Get the long text fields.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   */
  public function textContentFieldDefinitions() {
    $text_fields['body'] = BundleFieldDefinition::create('string_long')
      ->setLabel(new TranslatableMarkup('Plain Body'))
      ->setProvider('communication')
      ->setRevisionable(TRUE)
      ->setDisplayOptions('view', [
        'type' => 'string',
        'label' => 'hidden',
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
      ])
      ->setDisplayConfigurable('form', TRUE);

    return $text_fields;
  }

  /**
   * {@inheritdoc}
   */
  public function participantRoles() {
    return [
      'from' => ParticipantRole::create('from', 'phone', new TranslatableMarkup('Sender')),
      'recipient' => ParticipantRole::create('recipient', 'phone', new TranslatableMarkup('Recipient')),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultTargetParticipant() {
    return 'recipient';
  }

  /**
   * {@inheritdoc}
   */
  public function defaultSourceParticipant() {
    return 'from';
  }
}
