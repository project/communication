<?php

namespace Drupal\communication;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class CommunicationServiceProvider.
 *
 * @package Drupal\communication
 */
class CommunicationServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    // Some services depend on a communication.contact_middleware existing,
    // this isn't defined in the communication module directly so we only
    // register these services if the contact_middleware service exists.
    if ($container->has('communication.contact_middleware')) {
      $container->setDefinition('communication.builder', new Definition(
        CommunicationBuilder::class,
        [
          new Reference('plugin.manager.communication.mode'),
          new Reference('entity_type.manager'),
          new Reference('communication.contact_middleware'),
        ]
      ));
    }
  }

}
